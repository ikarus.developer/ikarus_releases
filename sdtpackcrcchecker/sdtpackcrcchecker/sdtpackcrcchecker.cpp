#include "pch.h"
#include "utils.h"
#include <iostream>

#define ENABLE_MULTITHREADING


#ifdef ENABLE_MULTITHREADING
#include <mutex>
#include <thread>

std::mutex g_mtx;
#define CRITICAL_SCOPE std::lock_guard<std::mutex> guard(g_mtx);
const int g_threadCount = 4; //you could change it but does't increase more than 20/30 threads (few cpu could using more than 30 threads efficiently)
#endif



long long GetMilliseconds()
{
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}





int main()
{
	long long startTimestamp = GetMilliseconds();

	DIRECTORIES dirs;
	FILES files;
	
	GetDirectoriesFromDir(L"*", dirs, true);

	for(auto& dir : dirs)
		GetFilesFromDir(dir, files, L"");


	std::map<DWORD, FILES> crcMap;
	DWORD			count		= 0;
	const DWORD		total		= files.size();

	DWORD			savedmemory	= 0; 

	printf("starting to exams files... \n");


	auto func = [&crcMap,&count,&total] (WSTRING filename)
	{
		DWORD dwCRC = GetFileCRC32(filename.c_str());
		{
#ifdef ENABLE_MULTITHREADING
			CRITICAL_SCOPE
#endif
			auto it = crcMap.find(dwCRC);
			if(it == crcMap.end())
				it = crcMap.insert({dwCRC , FILES()}).first;

			it->second.push_back(filename);
			printf("\rexaming... [%d / %d]",count++,total);
		}
	};

#ifdef ENABLE_MULTITHREADING
	std::vector<std::thread> threads;

	auto mtFunc = [&files,&func] ()
	{
		while (true)
		{
			WSTRING file;

			{
				CRITICAL_SCOPE
				DWORD listSize = files.size();
				if(listSize == 0)
					break;

				file = files.front();
				files.pop_front();
			}

			func(file);
		}
	};


	for (int i = 0; i < g_threadCount; i++)
		threads.push_back(std::thread(mtFunc));

	for (auto& th : threads)
		th.join();

	threads.clear();
#else
	for (auto& file : files)
		func(file);
#endif


	auto funcMap = [&crcMap,&savedmemory](FILES& list, std::wofstream& inFile)
	{
		bool inserted=list.size() != 1;
		if (inserted)
			for (auto& file : list)
			{
				inFile << file.c_str();
				inFile << L"\n";
			}

		if (inserted)
		{
			inFile << L"\n\n\n";
			savedmemory += GetFileSize(list.front().c_str()) * (list.size()-1);
		}

		//just for checking
		/*DWORD dwFileSize = GetFileSize(list.front().c_str());
		for(auto& file : list)
			if (dwFileSize != GetFileSize(file.c_str()))
				printf("WARNING : [%s] you should ignore it.\n");*/

	};



	std::wofstream resultFile("result.txt");

	for (auto& iterMap : crcMap)
		funcMap(iterMap.second, resultFile);

	std::wstring stSavedmemory = L"\nMEMORY YOU COULD RECOVER [";
	stSavedmemory += std::to_wstring(savedmemory);
	stSavedmemory += L" bytes ] \n\n";

	resultFile << stSavedmemory.c_str();


	wprintf(stSavedmemory.c_str());

	long long endTimestamp = GetMilliseconds();
	const long long totalms = endTimestamp - startTimestamp;

	printf("elaboration time : %lld m   %lld s   %lld ms", 
		(totalms / (60*1000)) , (totalms/1000)%60 , (totalms%1000) 
	);

	char a=getchar();
	return 0;
}
