//search 
void CActorInstance::ShowAllAttachingEffect()
{
	std::list<TAttachingEffect>::iterator it;
	for(it = m_AttachingEffectList.begin(); it!= m_AttachingEffectList.end();++it)
	{
		CEffectManager::Instance().SelectEffectInstance(it->dwEffectIndex);
		CEffectManager::Instance().ShowEffect();


//add under:
#ifdef __ENABLE_STEALTH_FIX__
		CEffectManager::Instance().ReleaseAlwaysHidden();
#endif





//search
void CActorInstance::HideAllAttachingEffect()
{
	std::list<TAttachingEffect>::iterator it;
	for(it = m_AttachingEffectList.begin(); it!= m_AttachingEffectList.end();++it)
	{
		CEffectManager::Instance().SelectEffectInstance(it->dwEffectIndex);
		CEffectManager::Instance().HideEffect();


//add under:
#ifdef __ENABLE_STEALTH_FIX__
		CEffectManager::Instance().ApplyAlwaysHidden();
#endif