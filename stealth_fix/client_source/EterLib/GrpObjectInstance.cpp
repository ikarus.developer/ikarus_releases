//search:

bool CGraphicObjectInstance::isShow()
{


//add under
#ifdef __ENABLE_STEALTH_FIX__
	return m_isVisible && !m_isAlwaysHidden;
#endif






//search
void CGraphicObjectInstance::Hide()
{
	m_isVisible = false;
}

//add under

#ifdef __ENABLE_STEALTH_FIX__
void CGraphicObjectInstance::ApplyAlwaysHidden() {
	m_isAlwaysHidden = true;
}

void CGraphicObjectInstance::ReleaseAlwaysHidden() {
	m_isAlwaysHidden = false;
}
#endif






//search

void CGraphicObjectInstance::Initialize()
{
	if (m_CullingHandle)
		CCullingManager::Instance().Unregister(m_CullingHandle);
	m_CullingHandle = 0;

	m_pHeightAttributeInstance = NULL;

	m_isVisible = TRUE;

	m_BlockCamera = false;


//add under
#ifdef __ENABLE_STEALTH_FIX__
	m_isAlwaysHidden = false;
#endif