//search:
		void					BlendRender();
		void					RenderToShadowMap();
		void					RenderShadow();
		void					RenderPCBlocker();
		void					Deform();
		void					Transform();

		void					Show();
		void					Hide();
		bool					isShow();

//add
#ifdef __ENABLE_STEALTH_FIX__
		void					ApplyAlwaysHidden();
		void					ReleaseAlwaysHidden();
#endif




//search:
float					m_fYaw;
		float					m_fPitch;
		float					m_fRoll;

		D3DXMATRIX				m_mRotation;

		bool					m_isVisible;
		
//add
#ifdef __ENABLE_STEALTH_FIX__
		bool					m_isAlwaysHidden;
#endif