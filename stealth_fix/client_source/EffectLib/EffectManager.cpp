//search
void CEffectManager::HideEffect()
{
	if (!m_pSelectedEffectInstance)
		return;

	m_pSelectedEffectInstance->Hide();
}



//add under
#ifdef __ENABLE_STEALTH_FIX__
void CEffectManager::ApplyAlwaysHidden()
{
	if (!m_pSelectedEffectInstance)
		return;

	m_pSelectedEffectInstance->ApplyAlwaysHidden();
}


void CEffectManager::ReleaseAlwaysHidden()
{
	if (!m_pSelectedEffectInstance)
		return;

	m_pSelectedEffectInstance->ReleaseAlwaysHidden();
}
#endif