// header.h: file di inclusione per file di inclusione di sistema standard
// o file di inclusione specifici del progetto
//

#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include "targetver.h"
#define WIN32_LEAN_AND_MEAN             // Escludere gli elementi usati raramente dalle intestazioni di Windows
// File di intestazione di Windows
#include <Windows.h>
#include <commdlg.h>
// File di intestazione Runtime C
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <fstream>
#include <string>
#include <vector>
#include <unordered_map>
#include <map>
#include <algorithm>
#include <ctime>

#include "exp-helper.h"
#define __IKGL_LINK_ONLY_DEP__
#include <ikarus_gl.h>
#include <ikarus_gl_link.h>

#include "exp-helper-types.h"

//ex-library
#include <format.h>
#include <bench_mark.h>
#include "regen.h"
#include "mob_proto.h"