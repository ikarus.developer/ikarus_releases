#pragma once

#include "resource.h"

inline std::vector<std::string> get_split(const std::string& s, const std::string& tok) {
	std::vector<std::string> v;
	size_t pos = 0, prev_pos = 0;
	while ((pos = s.find(tok, prev_pos)) != std::string::npos) {
		v.emplace_back(s.substr(prev_pos, pos - prev_pos));
		prev_pos = pos + 1;
	} if (prev_pos < s.size()) { v.emplace_back(s.substr(prev_pos)); }
	return v;
};

inline void erase_empty_elements(std::vector<std::string>& v) {
	v.erase(std::remove_if(v.begin(), v.end(), [](const std::string& s) ->bool { return s.empty(); }), v.end());
}

inline std::string to_lowercase(const std::string& s) {
	std::string r; r.reserve(s.size());
	for (const auto c : s) {
		r.push_back(std::tolower(c));
	} return r;
}

template<class ...T>
void debugme(T... args) {
	ikex::fprintf(stderr , ikex::format(args...)+'\n');
}

template <class T>
T random_in_range(const T init, const T end) {
	static bool init_random = false;
	if (!init_random) { std::srand(std::time(NULL)); init_random = true; }
	return init + T((float(std::rand())/float(RAND_MAX)) * (end-init));
}

inline long get_linear_distance(const long x, const long y, const long xdest, const long ydest) {
	const auto wx = x - xdest;
	const auto wy = y - ydest;
	return std::sqrt((wx*wx) + (wy*wy));
}

BYTE get_map_position_value(const long x, const long y);