#pragma once

namespace mobproto {
	enum {
		CHARACTER_NAME_MAX_LEN = 24,
		MOB_ATTRIBUTE_MAX_NUM = 12,
		MOB_SKILL_MAX_NUM = 5,
		MOB_ENCHANTS_MAX_NUM = 6,
		MOB_RESISTS_MAX_NUM = 11,
	};

#pragma push()
#pragma pack(1)

	typedef struct SMobSkillLevel{
		DWORD       dwVnum;
		BYTE        bLevel;
	} TMobSkillLevel;

	typedef struct SMobTable_r255{
		DWORD       dwVnum;
		char        szName[CHARACTER_NAME_MAX_LEN + 1];
		char        szLocaleName[CHARACTER_NAME_MAX_LEN + 1];
		BYTE        bType;
		BYTE        bRank;
		BYTE        bBattleType;
		BYTE        bLevel;
		BYTE        bSize;
		DWORD       dwGoldMin;
		DWORD       dwGoldMax;
		DWORD       dwExp;
		DWORD       dwMaxHP;
		BYTE        bRegenCycle;
		BYTE        bRegenPercent;
		WORD        wDef;
		DWORD       dwAIFlag;
		DWORD       dwRaceFlag;
		DWORD       dwImmuneFlag;
		BYTE        bStr, bDex, bCon, bInt;
		DWORD       dwDamageRange[2];
		short       sAttackSpeed;
		short       sMovingSpeed;
		BYTE        bAggresiveHPPct;
		WORD        wAggressiveSight;
		WORD        wAttackRange;
		char        cEnchants[MOB_ENCHANTS_MAX_NUM];
		char        cResists[MOB_RESISTS_MAX_NUM];
		DWORD       dwResurrectionVnum;
		DWORD       dwDropItemVnum;
		BYTE        bMountCapacity;
		BYTE        bOnClickType;
		BYTE        bEmpire;
		char        szFolder[64 + 1];
		float       fDamMultiply;
		DWORD       dwSummonVnum;
		DWORD       dwDrainSP;
		DWORD		dwMonsterColor;
		DWORD       dwPolymorphItemVnum;
		TMobSkillLevel	Skills[MOB_SKILL_MAX_NUM];
		BYTE		bBerserkPoint;
		BYTE		bStoneSkinPoint;
		BYTE		bGodSpeedPoint;
		BYTE		bDeathBlowPoint;
		BYTE		bRevivePoint;
	} TMobTable;
#pragma pop()
	extern std::unordered_map<DWORD, mobproto::TMobTable> g_mob_proto;
	bool load_mob_proto(const std::string& filename = "");
}