#pragma once
#include "framework.h"
#include "exp-helper.h"
#include "mob_proto.h"
#include "lzo.h"

#define MAKEFOURCC(ch0, ch1, ch2, ch3)                              \
                ((DWORD)(BYTE)(ch0) | ((DWORD)(BYTE)(ch1) << 8) |   \
                ((DWORD)(BYTE)(ch2) << 16) | ((DWORD)(BYTE)(ch3) << 24 ))



namespace mobproto {
	std::unordered_map<DWORD, mobproto::TMobTable> g_mob_proto;


	bool load_mob_proto(const std::string& filename) {
		g_mob_proto.clear();

		static DWORD s_adwMobProtoKey[4] ={
			4813894,	18955,		552631,		6822045
		};
		std::ifstream file(filename.empty()?"mob_proto": filename, std::ios::binary);
		if (!file.is_open())
			return false;

		auto read_stream = [&file](void* mem, const size_t len) {
			file.read((char*)mem, len);
		};

		DWORD dwFourCC=0, dwElements=0, dwDataSize=0;
		read_stream(&dwFourCC, sizeof(DWORD));

		if (dwFourCC != MAKEFOURCC('M', 'M', 'P', 'T'))
			return false;

		read_stream(&dwElements, sizeof(DWORD));
		read_stream(&dwDataSize, sizeof(DWORD));

		std::unique_ptr<BYTE[]> data (new BYTE[dwDataSize]);
		read_stream(data.get(), dwDataSize);

		CLZObject zObj;
		if (!g_clzo.Decompress(zObj, data.get(), s_adwMobProtoKey))
			return false;

		if ((zObj.GetSize() % sizeof(TMobTable)) != 0)
			return false;

		for (DWORD i = 0; i < dwElements; ++i){
			TMobTable& t = *((TMobTable*) zObj.GetBuffer() + i);
			g_mob_proto[t.dwVnum] = t;
		}return true;
	}

}