#pragma once


namespace regen {
	bool load_regen(const std::string& filename);
	bool load_group(const std::string& filename);
	bool load_group_group(const std::string& filename);
	void for_each_regen(std::function<void(tool_types::regen_mob_t&)> func);
	void reset_regen();
	void reload_mobproto_regen();
}