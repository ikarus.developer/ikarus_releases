// exp-helper.cpp : Definisce il punto di ingresso dell'applicazione.
//

#include "framework.h"


//for select folder dialog
#include <Shlobj.h>
#pragma comment(lib, "Shell32.lib")
#pragma comment(lib, "Comdlg32.lib")


//the graphic library is linked by referecing the project


//* globals vars
ikgl::gui g_gui;
ikgl::coord_t g_screen_width = 0;
ikgl::coord_t g_screen_height = 0;
std::deque<ikgl::text*> g_log_texts;

std::atomic<bool> g_load_attrmap = false;
std::atomic<bool> g_is_positioning = false;


//* map loding
std::string g_selected_map = "";
std::string g_selected_regen = "";
tool_types::map_info_t g_map_info = {};
std::vector<tool_types::player_info_t> g_players;


void set_screen_global_size();
bool create_gui(ikgl::hinstance_t hinstance);
bool create_ui_layer(ikgl::layer* ptr);
bool create_log_panel(ikgl::window* mainwindow);
bool create_titlebar_panel(ikgl::window* ptr);

bool create_load_map_panel(ikgl::window* mainwindow);
bool create_load_regen_panel(ikgl::window* mainwindow);
bool create_set_player_constants_panel(ikgl::window* mainwindow);
bool create_popup_error_panel(ikgl::window* layer);
bool create_scrollbar(ikgl::window* layer);

bool create_simulation_panel(ikgl::window* mainwindow);

void load_map(const std::string& folder);
void load_regen(const std::string& file);
void spawn_player();

//common functions
void popup_error(const std::string& s);
ikgl::widget_pos_t client_pos_to_map_pos(const ikgl::widget_pos_t& pos);

//external functions
std::string get_selected_folder();
std::string get_selected_file();
std::string get_working_directory();



template <class ...S>
void append_log(S... args) {
	auto adjust_log_texts = []() {
		const int lines_count = 12;
		for (auto ptr : g_log_texts) {
			ptr->hide();
		}
		auto y = g_log_texts.size() > lines_count ?
			     190 : 12 * g_log_texts.size();
		auto iter = --g_log_texts.end();
		auto idx  = lines_count;
		while (idx > 0) {
			auto text = *iter;
			text->set_position(5, y);
			std::printf("5 x %d : %s ",y,text->get_text());
			y -= 12; idx--;
			if (iter == g_log_texts.begin()) {
				break;
			}iter--;
		}
	};
	static ikgl::font_t font = {
		L"Calibri", 12,
		Gdiplus::FontStyle::FontStyleRegular,
		Gdiplus::Unit::UnitPixel
	};
	auto line = ikex::format(args...);
	auto logpanel = g_gui.get_window_by_name("log-panel");
	auto new_text = logpanel->create_element<ikgl::text>();
	new_text->set_font(font);
	new_text->set_size({ logpanel->get_width()-5, 12 });
	new_text->set_horizontal_alignment(ikgl::text_halign_t::TEXT_HALIGN_LEFT);
	new_text->set_text(line.c_str());
	new_text->set_color({RGB(180,180,180)});
	g_log_texts.push_back(new_text);
	adjust_log_texts();
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
#ifdef _DEBUG
	AllocConsole();
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);
#endif

	set_screen_global_size();
	if (!create_gui(hInstance)) {
		__ikgl_error("cannot create gui!");
		return 0;
	}

	if (!mobproto::load_mob_proto()) {
		popup_error("Mob proto not loaded! (You can load it later via the button in the panel.)");
	}

	g_gui.start_rendering_thread();
	g_gui.start_updating_thread();
	g_gui.message_loop(10);

	g_gui.clear();
	return 0;
}

void set_screen_global_size() {
	auto size = ikgl::utilities::get_desktop_size();
	g_screen_height = size.height;
	g_screen_width = size.width;
}


bool create_gui(ikgl::hinstance_t hinstance) {
	//*creating application window
	ikgl::create_gui_infow_t info;
	ikgl::make_default_create_info(hinstance, &info);

	static constexpr wchar_t window_name[] = L"exp-helper v2.0";

	info.window_name = window_name;
	info.wc.lpszClassName = window_name;
	
	const auto app_rect = ikgl::utilities::get_app_bar_rect();
	const auto app_size = ikgl::utilities::get_app_bar_size();
	const auto is_vertical = app_size.height > app_size.width;
	const auto is_left = app_rect.left < g_screen_width / 2;
	const auto is_top = app_rect.top < g_screen_height / 2;

	info.x = is_vertical ? (is_left ? app_size.width : 0) : 0;
	info.y = is_vertical ? 0 : (is_top ? app_size.height : 0);
	info.width = is_vertical ? (g_screen_width - app_size.width) : g_screen_width;
	info.height = is_vertical ? g_screen_height : (g_screen_height - app_size.height);

	if (!g_gui.create(&info))
		return false;
	//* creating a layers
	auto* ui_layer = g_gui.add_layer("UI");
	return create_ui_layer(ui_layer);
}


bool create_ui_layer(ikgl::layer* ptr) {
	ikgl::window* mainwindow = ptr->create_element<ikgl::window>();
	mainwindow->set_window_name("ui-main-window");
	mainwindow->set_position(0, 0);
	mainwindow->set_size({ g_gui.get_size().width, g_gui.get_size().height });
	mainwindow->set_background(ikgl::background_info_t(RGB(20,20,20)));

	auto* leftpanel = ptr->create_element<ikgl::window>();
	leftpanel->set_size({200, mainwindow->get_height() - 32});
	leftpanel->set_position(0, 32);
	leftpanel->set_background(ikgl::background_info_t(RGB(30, 30, 30), RGB(50, 50, 50)));
	
	create_scrollbar(leftpanel);
	create_load_map_panel(leftpanel);
	create_titlebar_panel(mainwindow);
	create_load_regen_panel(leftpanel);
	create_set_player_constants_panel(leftpanel);
	create_simulation_panel(mainwindow);
	create_log_panel(mainwindow);
	create_popup_error_panel(mainwindow);
	return true;
}


bool create_titlebar_panel(ikgl::window* ptr) {
	auto* mainwindow = ptr->create_element<ikgl::window>();
	mainwindow->set_window_name("titlebar-main-window");
	mainwindow->set_position(0, 0);
	mainwindow->set_size({ g_gui.get_size().width, 32 });
	mainwindow->set_background(ikgl::background_info_t(RGB(45, 45, 48) , RGB(60,60,60)));

	//* creating close button
	auto* closebutton = mainwindow->create_element<ikgl::button>();
	closebutton->set_default_cover("resources/close-button.png");
	closebutton->set_over_cover("resources/close-button-over.png");
	closebutton->set_down_cover("resources/close-button.png");

	closebutton->move_to_right();
	closebutton->move_to_top();
	closebutton->set_on_click_event(__ikgl_wevent_lambda_type{ ::PostQuitMessage(0); return ikgl::widget_event_return_t::KEEP_CALLBACK; });

	//* creating minimize button
	auto* minimize = mainwindow->create_element<ikgl::button>();
	minimize->set_default_cover("resources/minimize-button.png");
	minimize->set_over_cover("resources/minimize-button-over.png");
	minimize->set_down_cover("resources/minimize-button.png");

	minimize->move_to_right();
	minimize->move_to_top();
	minimize->move_left(closebutton->get_width());
	minimize->set_on_click_event(__ikgl_wevent_lambda_type{ c->get_gui()->minimize(); return ikgl::widget_event_return_t::KEEP_CALLBACK; });

	//* creating icon helper tool
	auto* icon = mainwindow->create_element<ikgl::image>();
	icon->load_image("resources/IKD-LOGO.png");
	icon->set_size(20,20);
	icon->move_to_left();
	icon->move_vertical_center();
	icon->move_right(5);

	//* creating helper tool title
	ikgl::font_t font;
	ikgl::make_default_font(font);
	font.size = 14;
	font.font_family = L"Calibri";

	auto* title = mainwindow->create_element<ikgl::text>();
	title->set_font(font);
	title->set_text(std::wstring(L"Exp Helper Tool v2.0  - Ikarus Developer ") + std::wstring({ 0x00A9, 0x0 }) + L" Copyrights 2020 ", true);
	title->set_color(ikgl::color_rgba_t(RGB(180,180,180)));
	title->move_to_left();
	title->move_vertical_center();
	title->move_right(icon->get_width() + 10);

	return true;
}

bool create_log_panel(ikgl::window* mainwindow) {
	auto panel = mainwindow->create_element<ikgl::window>();
	panel->set_size(mainwindow->get_width() - 201, 200);
	panel->set_position(201, mainwindow->get_height()-201);
	panel->set_background({ RGB(30, 30, 30), RGB(50, 50, 50) });
	panel->set_window_name("log-panel");
	return true;
}

bool create_load_map_panel(ikgl::window* mainwindow) {
	//* creating panel
	auto* panel = mainwindow->create_element<ikgl::window>();
	panel->set_size({160, 75});
	panel->set_background(ikgl::background_info_t(RGB(20,20,20), RGB(40,40,40)));
	panel->set_position({ 31, 25});

	//* creating title
	ikgl::font_t font;
	ikgl::make_default_font(font);
	font.size = 14;
	font.font_family = L"Calibri";

	auto* title = panel->create_element<ikgl::text>();
	title->set_font(font);
	title->set_text(std::wstring({ ' ',' ',' ', 0x2b29,' ', 0x0 }) + L"   Load Map   " + std::wstring({ ' ', 0x2b29, ' ',' ',' ',0x0 }), true);
	title->move_to_top();
	title->move_bottom(2);
	title->move_horizontal_center();
	title->set_color(ikgl::color_rgba_t(RGB(180, 180, 180)));
	title->set_background(ikgl::background_info_t({ RGB(65,65,65), RGB(75,75,75) }));

	//* creating load map button
	ikgl::button_color_info_t button_colors;
	button_colors.default_color = RGB(60,60,60);
	button_colors.down_color = RGB(60,60,60);
	button_colors.over_color = RGB(80,80,80);
	button_colors.border_color = RGB(80,80,80);
	button_colors.border_width = 1;
	
	auto* select_folder_button = panel->create_element<ikgl::button>();
	select_folder_button->set_size(75,20);
	select_folder_button->set_colors(&button_colors);
	select_folder_button->set_text("Select");
	select_folder_button->get_text_instance()->set_font(font);
	select_folder_button->get_text_instance()->set_color(ikgl::color_rgba_t(RGB(180, 180, 180)));
	select_folder_button->set_text("Select");
	select_folder_button->move_to_bottom();
	select_folder_button->move_top(5);
	select_folder_button->move_horizontal_center();
	select_folder_button->set_on_click_event(__ikgl_wevent_lambda_type{
		std::string s = get_selected_folder();
		g_selected_map = s;
		auto* ptr = ikgl::widget_cast<ikgl::text>(c->get_gui()->get_window_by_name("selected-folder-text"));
		load_map(s);
		if (ptr) {
			ptr->set_text(std::string("..") + s.substr(s.size() > 20 ? s.size() - 20 : 0) , true);
			ptr->move_horizontal_center();
		}return ikgl::widget_event_return_t::KEEP_CALLBACK;
	});

	auto* selected_folder_text = panel->create_element<ikgl::text>();
	font.size = 12;
	selected_folder_text->set_font(font);
	selected_folder_text->set_text("press select button..", true);
	selected_folder_text->set_color(ikgl::color_rgba_t(RGB(180, 180, 180)));
	selected_folder_text->set_position(0, title->get_height() +8);
	selected_folder_text->move_horizontal_center();
	selected_folder_text->set_window_name("selected-folder-text");

	return true;
}

bool create_load_regen_panel(ikgl::window* mainwindow) {
	//* creating panel
	auto* panel = mainwindow->create_element<ikgl::window>();
	panel->set_size({ 160, 75 });
	panel->set_background(ikgl::background_info_t(RGB(20, 20, 20), RGB(40, 40, 40)));
	panel->set_position({ 31,100+25 });

	//* creating title
	ikgl::font_t font;
	ikgl::make_default_font(font);
	font.size = 14;
	font.font_family = L"Calibri";

	auto* title = panel->create_element<ikgl::text>();
	title->set_font(font);
	title->set_text(std::wstring({ ' ',' ',' ', 0x2b29,' ', 0x0 }) + L"   Load Regen   " + std::wstring({ ' ', 0x2b29, ' ',' ',' ',0x0 }), true);
	title->move_to_top();
	title->move_bottom(2);
	title->move_horizontal_center();
	title->set_color(ikgl::color_rgba_t(RGB(180, 180, 180)));
	title->set_background(ikgl::background_info_t({ RGB(65,65,65), RGB(75,75,75) }));

	//* creating load map button
	ikgl::button_color_info_t button_colors;
	button_colors.default_color = RGB(60, 60, 60);
	button_colors.down_color = RGB(60, 60, 60);
	button_colors.over_color = RGB(80, 80, 80);
	button_colors.border_color = RGB(80, 80, 80);
	button_colors.border_width = 1;

	auto* select_file_button = panel->create_element<ikgl::button>();
	select_file_button->set_size(75, 20);
	select_file_button->set_colors(&button_colors);
	select_file_button->set_text("Select");
	select_file_button->get_text_instance()->set_font(font);
	select_file_button->get_text_instance()->set_color(ikgl::color_rgba_t(RGB(180, 180, 180)));
	select_file_button->set_text("Select");
	select_file_button->move_to_bottom();
	select_file_button->move_top(5);
	select_file_button->move_horizontal_center();
	select_file_button->set_on_click_event(__ikgl_wevent_lambda_type{
		load_regen(get_selected_file());
		return ikgl::widget_event_return_t::KEEP_CALLBACK;
	});

	auto* selected_file_text = panel->create_element<ikgl::text>();
	font.size = 12;
	selected_file_text->set_font(font);
	selected_file_text->set_text("press select button..", true);
	selected_file_text->set_color(ikgl::color_rgba_t(RGB(180, 180, 180)));
	selected_file_text->set_position(0, title->get_height() + 8);
	selected_file_text->move_horizontal_center();
	selected_file_text->set_window_name("selected-regen-text");
	return true;
}

bool create_set_player_constants_panel(ikgl::window* mainwindow) {
	//* creating panel
	auto* panel = mainwindow->create_element<ikgl::window>();
	panel->set_size({ 160, 175 });
	panel->set_background(ikgl::background_info_t(RGB(20, 20, 20), RGB(40, 40, 40)));
	panel->set_position({ 31,100+100 + 25 });

	//* creating title
	ikgl::font_t font;
	ikgl::make_default_font(font);
	font.size = 14;
	font.font_family = L"Calibri";

	auto* title = panel->create_element<ikgl::text>();
	title->set_font(font);
	title->set_text(std::wstring({ ' ',' ',' ', 0x2b29,' ', 0x0 }) + L"   Players Panel   " + std::wstring({ ' ', 0x2b29, ' ',' ',' ',0x0 }), true);
	title->move_to_top();
	title->move_bottom(2);
	title->move_horizontal_center();
	title->set_color(ikgl::color_rgba_t(RGB(180, 180, 180)));
	title->set_background(ikgl::background_info_t({RGB(65,65,65), RGB(75,75,75)}));

	//* creating load map button
	ikgl::button_color_info_t button_colors;
	button_colors.default_color = RGB(60, 60, 60);
	button_colors.down_color = RGB(60, 60, 60);
	button_colors.over_color = RGB(80, 80, 80);
	button_colors.border_color = RGB(80, 80, 80);
	button_colors.border_width = 1;

	auto* spawn_player_button = panel->create_element<ikgl::button>();
	spawn_player_button->set_size(75, 20);
	spawn_player_button->set_colors(&button_colors);
	spawn_player_button->set_text("Spawn");
	spawn_player_button->get_text_instance()->set_font(font);
	spawn_player_button->get_text_instance()->set_color(ikgl::color_rgba_t(RGB(180, 180, 180)));
	spawn_player_button->set_text("Spawn");
	spawn_player_button->move_to_bottom();
	spawn_player_button->move_top(5);
	spawn_player_button->move_horizontal_center();
	spawn_player_button->set_on_click_event(__ikgl_wevent_lambda_type{
		if (g_map_info.attr_zones.empty()) {
			popup_error("You must load a map before to spawn a player!");
		} else {
			g_is_positioning = true;
			auto* ptr = g_gui.get_window_by_name("simulation-panel");
			if (ptr) {
				ptr->set_on_mousemove(__ikgl_wevent_lambda_type{
					if (g_is_positioning) {
						auto* t = ikgl::widget_cast<ikgl::text>(g_gui.get_window_by_name("spawn-player-text"));
						const auto pos = client_pos_to_map_pos(g_gui.get_mouse_position());
						t->set_text(ikex::format("coords  : {}  x  {} ", pos.x, pos.y));
						return ikgl::widget_event_return_t::KEEP_CALLBACK;
					}return ikgl::widget_event_return_t::UNSET_CALLBACK;
				});
			}
		}return ikgl::widget_event_return_t::KEEP_CALLBACK;
	});

	auto* spawn_position_text = panel->create_element<ikgl::text>();
	font.size = 12;
	spawn_position_text->set_font(font);
	spawn_position_text->set_text("press spawn button..", true);
	spawn_position_text->set_color(ikgl::color_rgba_t(RGB(180, 180, 180)));
	spawn_position_text->set_position(0, title->get_height() + 8);
	spawn_position_text->move_horizontal_center();
	spawn_position_text->set_window_name("spawn-player-text");
	
	return true;
}


bool create_popup_error_panel(ikgl::window* mainwindow) {
	auto* window = mainwindow->create_element<ikgl::window>();
	window->set_size(100,65);
	window->hide();
	window->set_window_name("popup-error-window");
	window->set_background(ikgl::background_info_t({RGB(60,60,60)}));

	ikgl::font_t font;
	ikgl::make_default_font(font);
	font.size = 13;
	font.font_family = L"Calibri";

	auto* text = window->create_element<ikgl::text>();
	text->set_font(font);
	text->set_text("no text");
	text->set_color(ikgl::color_rgba_t(RGB(180, 180, 180)));
	text->set_position(0, 5);
	text->set_window_name("popup-error-text");

	ikgl::button_color_info_t button_colors;
	button_colors.default_color = RGB(45,45,45);
	button_colors.down_color = RGB(45, 45, 45);
	button_colors.over_color = RGB(70,70,70);
	button_colors.border_color = RGB(90,90,90);
	button_colors.border_width = 1;

	auto* closeme = window->create_element<ikgl::button>();
	closeme->set_size(50, 20);
	closeme->set_text("Ok");
	closeme->get_text_instance()->set_color(ikgl::color_rgba_t(RGB(180, 180, 180)));
	closeme->move_horizontal_center();
	closeme->move_to_bottom();
	closeme->move_top(5);
	closeme->set_colors(&button_colors);
	closeme->set_window_name("popup-error-button");
	closeme->set_on_click_event(__ikgl_wevent_lambda_type{
		c->add_timer(0.1, __ikgl_wevent_lambda_type{
			g_gui.get_window_by_name("popup-error-window")->hide();
			g_gui.call_render();
			return ikgl::widget_event_return_t::UNSET_CALLBACK;	
		});	return ikgl::widget_event_return_t::KEEP_CALLBACK; 
	});

	window->move_horizontal_center();
	window->move_vertical_center();
	return true;
}


__ikgl_custom_render_declare(simulation_render) {
	if (g_map_info.attr_zones.empty()) {
		return ikgl::widget_event_return_t::KEEP_CALLBACK;
	}

	static bool was_loaded = false;
	static HDC thdc;
	static HBITMAP tbmp; 

	if (g_load_attrmap) {
		g_load_attrmap = false;
		if (was_loaded) {
			::DeleteObject(tbmp);
			::DeleteDC(thdc);
		}

		was_loaded = true;
		const long totw = ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE) * g_map_info.width;
		const long toth = ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE) * g_map_info.height;

		//prepare the bitmap attributes
		BITMAPINFO bmInfo;
		memset(&bmInfo.bmiHeader, 0, sizeof(BITMAPINFOHEADER));
		bmInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
		bmInfo.bmiHeader.biWidth = totw;
		bmInfo.bmiHeader.biHeight = toth;
		bmInfo.bmiHeader.biPlanes = 1;
		bmInfo.bmiHeader.biBitCount = 24;

		thdc = CreateCompatibleDC(hdc);
		BYTE* pbase;
		tbmp = CreateDIBSection(hdc,&bmInfo, DIB_RGB_COLORS, (void**)&pbase, 0, 0);
		HGDIOBJ TmpObj = SelectObject(thdc, tbmp);
		
		
		auto render_zone = [](HDC& hdc, const int x, const int y, const tool_types::attr_zone_t& zone) {
			const auto bx = x * (int)tool_types::constval::ATTR_MAP_SIZE;
			const auto by = y * (int)tool_types::constval::ATTR_MAP_SIZE;
			const auto* ptr = (BYTE*) zone.attr;
			for (int oy = 0; oy < (int)tool_types::constval::ATTR_MAP_SIZE; oy++) {
				for (int ox = 0; ox < (int)tool_types::constval::ATTR_MAP_SIZE; ox++, ptr++) {
					const auto cr = *(ptr) * 12;
					::SetPixel(hdc, bx + ox, by + oy, RGB(cr +20,cr,cr +20));
				}
			}
		};


		int x = 0, y = 0;
		const auto& zones_matrix = g_map_info.attr_zones;
		for (const auto& zones : zones_matrix) {
			for (const auto& zone : zones) {
				render_zone(thdc, x, y++, zone);
			}x++; y = 0;
		}::SelectObject(thdc, TmpObj);
	}
	
	auto render_func = [](ikgl::hdc_t& hdc , ikgl::gdiplus_graphics_t* graphicsptr, ikgl::window* widgetptr) {
		HGDIOBJ TmpObj = SelectObject(thdc, tbmp);
		const long totw = ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE) * g_map_info.width;
		const long toth = ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE) * g_map_info.height;

		::StretchBlt(hdc, widgetptr->get_x() +1, widgetptr->get_y()+1, widgetptr->get_width()-1, widgetptr->get_height()-1, thdc, 0, 0, totw, toth, SRCCOPY);
		::SelectObject(thdc, TmpObj);
	};

	
	render_func(hdc, graphicsptr, widgetptr);
	//monsters rendering
	const long totw = ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE) * g_map_info.width;
	const long toth = ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE) * g_map_info.height;
	const auto wscale = float(widgetptr->get_width()) / float(totw);
	const auto hscale = float(widgetptr->get_height()) / float(toth);
	const long bx = widgetptr->get_x();
	const long by = widgetptr->get_y();
	const auto cr = RGB(255, 10, 10);

	regen::for_each_regen([&bx, &by, &hscale,&wscale,&hdc,&cr](tool_types::regen_mob_t& regen) {
		if (regen.is_alive) {
			const auto fx = bx + (regen.x * wscale);
			const auto fy = by + (regen.y * hscale);
			::SetPixelV(hdc, fx, fy, cr);
		}
	});

	//players rendering
	auto draw_circle = [](ikgl::hdc_t& hdc, const long x, const long y, const long radius, COLORREF cr) {
		HBRUSH old = SelectBrush(hdc, GetStockObject(HOLLOW_BRUSH));
		HPEN pen = ::CreatePen(PS_SOLID, 2, cr);
		HPEN old_pen = SelectPen(hdc, pen);
		::Ellipse(hdc, x-radius, y-radius, x+radius, y+radius);
		SelectBrush(hdc, old);
		::DeleteObject(SelectPen(hdc, old_pen));
	};
	for (const auto& player : g_players) {
		RECT rect;
		rect.left = bx + (player.x * wscale) -1;
		rect.top  = by + (player.y * hscale) -1;
		rect.right  = rect.left + 2;
		rect.bottom = rect.top  + 2;
		HBRUSH br = ::CreateSolidBrush(RGB(0,255,0));
		::FillRect(hdc, &rect, br);
		::DeleteObject(br);
		draw_circle(hdc, bx + (player.x * wscale), by + (player.y * hscale), 6, RGB(0, 255, 0));
	}


	auto render_mouse_cross = [](ikgl::hdc_t& hdc, const ikgl::widget_pos_t& pos) {
		const auto sx = pos.x - 6;
		const auto ex = pos.x + 6;
		const auto sy = pos.y - 6;
		const auto ey = pos.y + 6;
		const auto cr = RGB(255,255,0);
		for (int i = -1; i < 2; i++) {
			for (int x = sx; x < ex; x++ ) {
				::SetPixel(hdc, x, pos.y + i, cr);
			}
			for (int y = sy; y < ey; y++) {
				::SetPixel(hdc, pos.x + i, y, cr);
			}
		}
	};

	if (g_is_positioning) {
		const auto pos = g_gui.get_mouse_position();
		if (widgetptr->is_in(pos))
			render_mouse_cross(hdc, pos);
	}return ikgl::widget_event_return_t::KEEP_CALLBACK;
}

bool create_scrollbar(ikgl::window* panel) {
	auto* scrollbar = panel->create_element<ikgl::scrollbar>();
	scrollbar->set_height(panel->get_height() - 4);
	scrollbar->set_position(6, 2);
	auto* bt1 = scrollbar->get_button1_ptr();
	auto* bt2 = scrollbar->get_button2_ptr();
	auto* mbt = scrollbar->get_middle_button_ptr();
	scrollbar->set_background(ikgl::background_info_t({ RGB(62,62,62) }));

	bt1->set_default_cover("resources/scrollbar/up.png");
	bt1->set_down_cover("resources/scrollbar/up.png");
	bt1->set_over_cover("resources/scrollbar/up-over.png");
	bt2->set_default_cover("resources/scrollbar/down.png");
	bt2->set_down_cover("resources/scrollbar/down.png");
	bt2->set_over_cover("resources/scrollbar/down-over.png");
	mbt->set_default_cover("resources/scrollbar/middle.png");
	mbt->set_down_cover("resources/scrollbar/middle.png");
	mbt->set_over_cover("resources/scrollbar/middle-over.png");

	scrollbar->set_size(mbt->get_width(), scrollbar->get_height());
	scrollbar->set_middle_button_height(scrollbar->get_height()/10);

	bt1->move_horizontal_center();
	bt1->move_to_top();
	bt2->move_horizontal_center();
	bt2->move_to_bottom();
	mbt->move_horizontal_center();
	return true; 
}

bool create_simulation_panel(ikgl::window* mainwindow) {
	auto* simulation_window = mainwindow->create_element<ikgl::window>();
	simulation_window->set_position(201, 33);
	auto guisize = g_gui.get_size();
	simulation_window->set_size(guisize.width -201, guisize.height -(33+200));
	simulation_window->set_background(ikgl::background_info_t({ 0.0 , 0.0 ,0.0, 1.0 }));
	simulation_window->set_on_custom_render(simulation_render);
	simulation_window->set_window_name("simulation-panel");
	simulation_window->set_on_mouseleft_up(__ikgl_wevent_lambda_type{
		if (g_is_positioning) {
			spawn_player();
			g_is_positioning = false;
			g_gui.call_render();
		}return ikgl::widget_event_return_t::KEEP_CALLBACK;
	});
	return true;
}

void load_map(const std::string& folder) {
	if (folder.empty())
		return;

	if (g_map_info.attr_zones.empty() == false)
		regen::reset_regen();

	//* loading files
	std::string names[] = {
			folder + "\\setting.txt" ,
			folder + "\\Setting.txt" ,
			folder + "\\SETTING.txt" ,
	}; for (const auto& name : names) {
		std::ifstream stream(name.c_str());
		if (stream.is_open()) {
			std::string line;

			while (std::getline(stream, line)) {
				auto v = get_split(line, "\t");
				if (v.empty() ==  false &&
					v[0] == "MapSize" ){
					if (v.size() >= 3) {
						g_map_info.width = atoi(v[1].c_str());
						g_map_info.height = atoi(v[2].c_str());
					}
				}
			} break;
		}
	}

	//*checking the loading
	if (g_map_info.width == 0 || g_map_info.height == 0 ) {
		popup_error(ikex::format("loading setting.txt fails : width {}  height {}  ", g_map_info.width , g_map_info.height ));
	}
	
	auto* simulation = g_gui.get_window_by_name("simulation-panel");
	simulation->remove_children();
	const ikgl::coord_t iw = simulation->get_width()  / g_map_info.width;
	const ikgl::coord_t ih = simulation->get_height() / g_map_info.height;
	static char path[1024] = {};
	g_map_info.attr_zones.clear();
	g_map_info.attr_zones.resize(g_map_info.height);

	for (long i = 0; i < g_map_info.width; i++) {
		g_map_info.attr_zones[i].resize(g_map_info.height);

		for (long j = 0; j < g_map_info.height; j++) {
			tool_types::attr_zone_t& newzone = g_map_info.attr_zones[i][j];

			_snprintf(path, sizeof(path), "%s\\%03d%03d\\attr.atr", folder.c_str(),i,j);
			std::ifstream st(path, std::ios::ate);
			if (st.is_open()) {
				const auto total_size = st.tellg();
				st.seekg(0, std::ios::beg);

				if (total_size > sizeof(tool_types::SAttrMapHeader)) {
					tool_types::SAttrMapHeader attrheader;
					st.read((char*)&attrheader, sizeof(attrheader));

					if (attrheader.m_wMagic  != 2634 ||
						attrheader.m_wHeight != (int)tool_types::constval::ATTR_MAP_SIZE ||
						attrheader.m_wWidth  != (int)tool_types::constval::ATTR_MAP_SIZE) {
						popup_error(ikex::format("invalid attr map header {} ", path));
					} else st.read((char*) &newzone, sizeof(newzone));
				} else popup_error(ikex::format("invalid attr map {} ", path));
			}else popup_error(ikex::format("invalid attr map {} ", path));
		}
	} g_load_attrmap = true;
}

void load_regen(const std::string& filename) {

	debugme("is going to load regen {} ", filename);

	auto here = get_working_directory();
	auto groupfile = here + "\\group.txt";
	auto groupgroupfile = here + "\\group_group.txt";

	if (!regen::load_group(groupfile)){
		popup_error(ikex::format("cannot load file {} ", groupfile));
	}else append_log("group.txt loaded successfully.");

	if (!regen::load_group_group(groupgroupfile)){
		popup_error(ikex::format("cannot load file {} ", groupgroupfile));
	}else append_log("group_group.txt loaded successfully.");

	if (!regen::load_regen(filename)){
		popup_error(ikex::format("cannot load file {} ", filename));
	}else append_log("{} loaded successfully.", filename);
}


void spawn_player() {
	const auto pos = client_pos_to_map_pos(g_gui.get_mouse_position());
	tool_types::player_info_t player;
	player.x = pos.x;
	player.y = pos.y;
	player.level = 0;
	player.exp = 0;
	g_players.emplace_back(player);
}

//* common functions
void popup_error(const std::string& s) {
	auto tx = ikgl::widget_cast<ikgl::text>(g_gui.get_window_by_name("popup-error-text"));
	if (tx) {
		tx->set_text(s, true);
		auto size = tx->get_size();
		tx->get_parent()->set_size(size.width + 30, tx->get_parent()->get_height());
		tx->get_parent()->show();
		tx->get_parent()->move_horizontal_center();
		tx->move_horizontal_center();
		g_gui.get_window_by_name("popup-error-button")->move_horizontal_center();
	}
}


ikgl::widget_pos_t client_pos_to_map_pos(const ikgl::widget_pos_t& pos) {
	if (g_map_info.attr_zones.empty())
		return pos;

	auto result = pos;
	auto* ptr = g_gui.get_window_by_name("simulation-panel");
	const auto map_size = ptr->get_size();
	const auto map_pos = ptr->get_position();
	const long totw = ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE) * g_map_info.width;
	const long toth = ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE) * g_map_info.height;
	const auto wscale = float(map_size.width)  / float(totw);
	const auto hscale = float(map_size.height) / float(toth);

	result.x -= map_pos.x;
	result.y -= map_pos.y;
	result.x /= wscale;
	result.y /= hscale;

	return result;
}

BYTE get_map_position_value(const long x, const long y) {
	if (g_map_info.attr_zones.empty())
		return 0;

	const long totw = ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE) * g_map_info.width;
	const long toth = ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE) * g_map_info.height;
	if (x >= totw) return 0;
	if (y >= toth) return 0;

	const auto i = x / ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE);
	const auto j = y / ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE);
	return g_map_info.attr_zones[i][j].attr[x% ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE)][y% ikgl::coord_t(tool_types::constval::ATTR_MAP_SIZE)];
}



//* external functions
int CALLBACK browse_callback_func(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData)
{
	if (uMsg == BFFM_INITIALIZED){
		std::string tmp = (const char*)lpData;
		SendMessage(hwnd, BFFM_SETSELECTION, TRUE, lpData);
	} return 0;
}


inline std::wstring display_result(IShellItemArray* psiaResult) {
	PWSTR pszFilePath = NULL;
	DWORD dwNumItems = 0; // number of items in multiple selection
	std::wstring strSelected; // will hold file paths of selected items

	auto hr = psiaResult->GetCount(&dwNumItems);  // get number of selected items

	// Loop through IShellItemArray and construct string for display
	for (DWORD i = 0; i < dwNumItems; i++)
	{
		IShellItem* psi = NULL;

		hr = psiaResult->GetItemAt(i, &psi); // get a selected item from the IShellItemArray

		if (SUCCEEDED(hr))
		{
			hr = psi->GetDisplayName(SIGDN_FILESYSPATH, &pszFilePath);

			if (SUCCEEDED(hr))
			{
				strSelected += pszFilePath; // Append the filepath to the string
				if (i < (dwNumItems - 1))   //  if not the last item append 
					strSelected += L"\n";   //  new line character to string
				CoTaskMemFree(pszFilePath);
			}

			psi->Release();
		}
	}

	psiaResult->Release();
	return strSelected;
}

std::string get_selected_folder() {
	HWND hwnd = NULL;
	HRESULT hr;
	IFileOpenDialog* pOpenFolderDialog;
	std::wstring result;

	// CoCreate the dialog object.
	hr = CoCreateInstance(CLSID_FileOpenDialog, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pOpenFolderDialog));
	if (SUCCEEDED(hr)) {
		pOpenFolderDialog->SetOptions(FOS_PICKFOLDERS);
		//pOpenFolderDialog->SetFolder(psi);
		hr = pOpenFolderDialog->Show(hwnd);

		if (SUCCEEDED(hr)) {
			IShellItemArray* psiResult;
			hr = pOpenFolderDialog->GetResults(&psiResult);
			if (SUCCEEDED(hr)) {
				result = display_result(psiResult);
				psiResult->Release();
			}
		}		
		pOpenFolderDialog->Release();
	}return ikgl::utilities::to_string(result);
}

std::string get_working_directory() {
	char NPath[MAX_PATH];
	GetCurrentDirectoryA(MAX_PATH, NPath);
	return NPath;
}


std::string get_selected_file() {
	char filename[1024];

	OPENFILENAMEA ofn;
	ZeroMemory(&filename, sizeof(filename));
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = NULL;  // If you have a window to center over, put its HANDLE here
	ofn.lpstrFilter = "Text Files\0*.txt\0Any File\0*.*\0";
	ofn.lpstrFile = filename;
	ofn.nMaxFile = 1024;
	ofn.lpstrTitle = "Select a file!";
	ofn.Flags =  OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;

	if (GetOpenFileNameA(&ofn))
	{
		return filename;
	} return "";
}