#include "framework.h"

struct group_info_t{
	DWORD vnum;
	std::vector<DWORD> mob_vnums;
};

struct groupgroup_info_t{
	DWORD vnum;
	std::vector<DWORD> group_vnums;
};

std::map<DWORD, group_info_t> g_groups;
std::map<DWORD, groupgroup_info_t> g_group_groups;
std::vector<tool_types::regen_mob_t> g_regen;

namespace regen {
	void add_anywhere_regen(const long sx, const long sy, const long ex, const long ey, const long time, const long count, const DWORD vnum) {

	}

	void add_mob_regen(const long sx, const long sy, const long ex, const long ey, const long time, const long count, const DWORD vnum) {
		tool_types::regen_mob_t regen;
		for (long i = 0; i < max(1,count); i++) {
			regen.sx = sx;	regen.sy = sy;
			regen.ex = ex;	regen.ey = ey;
			regen.time = time;
			regen.is_alive = true;
			regen.vnum = vnum;
			regen.x = random_in_range(sx, sx+ex) + random_in_range(0, 30);
			regen.y = random_in_range(sy, sy+ey) + random_in_range(0, 30);
			auto it = mobproto::g_mob_proto.find(vnum);
			if (it != mobproto::g_mob_proto.end()){
				regen.table = &(it->second);
			}else regen.table = nullptr;
			g_regen.push_back(regen);
		}
	}

	void reload_mobproto_regen() {
		for (auto& regen : g_regen) {
			auto it = mobproto::g_mob_proto.find(regen.vnum);
			if (it != mobproto::g_mob_proto.end()) {
				regen.table = &(it->second);
			} else regen.table = nullptr;
		}
	}

	void add_group_regen(const long sx, const long sy, const long ex, const long ey, const long time, const long count, const DWORD vnum) {
		auto it = g_groups.find(vnum);
		if (it != g_groups.end()) {
			for (const auto& m : it->second.mob_vnums)
				add_mob_regen(sx - random_in_range(0,30) , sy - random_in_range(0, 30), ex + random_in_range(0, 30), ey + random_in_range(0, 30), time, count, m);
		} else debugme("cannot find group {} ", vnum);
	}

	void add_groupgroup_regen(const long sx, const long sy, const long ex, const long ey, const long time, const long count, const DWORD vnum ) {
		auto it = g_group_groups.find(vnum);
		if (it != g_group_groups.end()) {
			for (const auto grp : it->second.group_vnums)
				add_group_regen(sx, sy, ex, ey, time, count, grp);
		} else debugme("cannot find group group {} ", vnum);
	}

	

	void add_anywhere(const long sx, const long sy, const long ex, const long ey, const long time, const long count, const DWORD vnum) {
	}
	
	bool load_regen(const std::string& filename) {
		std::ifstream stream(filename.c_str());
		if (!stream.is_open()) return false;
		g_regen.clear();

		std::string line;
		while (std::getline(stream, line)) {
			//debugme("reading line {} ", line);

			auto v = get_split(line, "\t");
			erase_empty_elements(v);

			if (v.size() < 11) {
				debugme("jumping line {} ",line);
				continue;
			}

			static constexpr int sx_index = 1;
			static constexpr int sy_index = 2;
			static constexpr int ex_index = 3;
			static constexpr int ey_index = 4;
			static constexpr int time_index = 7;
			static constexpr int count_index = 9;
			static constexpr int vnum_index = 10;

			auto timenum = v[time_index].substr(0, v[time_index].length() - 1);
			long time = atoi(timenum.c_str());

			switch (v[time_index].back()) {
			case 's':break;
			case 'm': time *= 60; break;
			case 'h': time *= 3600; break;
			}


			switch (v[0][0]) {
			case 'r': {
				add_groupgroup_regen(
					atoi(v[sx_index].c_str()), atoi(v[sy_index].c_str()),
					atoi(v[ex_index].c_str()), atoi(v[ey_index].c_str()),
					time, atoi(v[count_index].c_str()),
					atoi(v[vnum_index].c_str())
				);
			}break;

			case 'm': {
				add_mob_regen(
					atoi(v[sx_index].c_str()), atoi(v[sy_index].c_str()),
					atoi(v[ex_index].c_str()), atoi(v[ey_index].c_str()),
					time, atoi(v[count_index].c_str()),
					atoi(v[vnum_index].c_str())
				);
			} break;

			case 'g': {
				add_group_regen(
					atoi(v[sx_index].c_str()), atoi(v[sy_index].c_str()),
					atoi(v[ex_index].c_str()), atoi(v[ey_index].c_str()),
					time, atoi(v[count_index].c_str()),
					atoi(v[vnum_index].c_str())
				);
			} break;

			case 's': {
				add_anywhere_regen(
					atoi(v[sx_index].c_str()), atoi(v[sy_index].c_str()),
					atoi(v[ex_index].c_str()), atoi(v[ey_index].c_str()),
					time, atoi(v[count_index].c_str()),
					atoi(v[vnum_index].c_str())
				);
			} break;

			default:
				debugme("cannot known the type of {} ", v[0][0]);
				break;
			}
		}return true;
	}

	bool load_group(const std::string& filename) {
		bool is_reading_group = false;
		g_groups.clear();

		std::ifstream stream(filename.c_str());
		if (!stream.is_open()) return false;

		std::string line;
		group_info_t group;

		while (std::getline(stream, line)) {
			if (line.empty() == false && line.back() == '\n')
				line.erase(line.size() - 1);

			auto v = get_split(line, "\t");
			erase_empty_elements(v);

			if (v.empty())continue;

			if (!is_reading_group && v[0] == "{") {
				is_reading_group = true;
				continue;
			}

			if (is_reading_group && v[0] == "}") {
				is_reading_group = false;
				g_groups.emplace(group.vnum, group);
				group.mob_vnums.clear();
				continue;
			}

			if (is_reading_group) {
				if (to_lowercase(v[0]) == "leader" && v.size() > 2) {
					group.mob_vnums.emplace_back(atoi(v[2].c_str()));
				}else if (to_lowercase(v[0]) == "vnum" && v.size() > 1) {
					group.vnum = atoi(v[1].c_str());
				}else if (v.size() > 2 && v[0].empty() == false && isdigit(v[0][0])) {
					group.mob_vnums.emplace_back(atoi(v[2].c_str()));
				}
			}
		} return true;
	}

	bool load_group_group(const std::string& filename) {
		bool is_reading_group = false;
		g_group_groups.clear();

		std::ifstream stream(filename.c_str());
		if (!stream.is_open())return false;

		std::string line;
		groupgroup_info_t group;

		while (std::getline(stream, line)) {
			if (line.empty() == false && line.back() == '\n')
				line.erase(line.size() - 1);

			auto v = get_split(line, "\t");
			erase_empty_elements(v);

			if (v.empty()) continue;

			if (!is_reading_group && v[0] == "{") {
				is_reading_group = true;
				continue;
			}

			if (is_reading_group && v[0] == "}") {
				is_reading_group = false;
				g_group_groups.emplace(group.vnum, group);
				group.group_vnums.clear();
				continue;
			}

			if (is_reading_group) {
				if (to_lowercase(v[0]) == "vnum" && v.size() > 1) {
					group.vnum = atoi(v[1].c_str());
				} else if (v.size() > 2 && v[0].empty() == false && isdigit(v[0][0])) {
					for (int i = 0; i < atoi(v[2].c_str()); i++)
						group.group_vnums.emplace_back(atoi(v[1].c_str()));
				}
			}
		} return true;
	}

	void for_each_regen(std::function<void(tool_types::regen_mob_t&)> func) {
		for (auto& regen : g_regen) {
			func(regen);
		}
	}

	void reset_regen() {
		g_regen.clear();
	}
}


