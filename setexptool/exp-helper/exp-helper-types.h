#pragma once
#include "mob_proto.h"

namespace tool_types {

	enum class constval {
		ATTR_MAP_SIZE = 256,
	};

	enum class attr_type
	{
		ATTRIBUTE_BLOCK = (1 << 0),
		ATTRIBUTE_WATER = (1 << 1),
		ATTRIBUTE_BANPK = (1 << 2),
	};

	enum class regen_type {
		REGEN_MOB,
		REGEN_GROUP,
		REGEN_GROUPGROUP,
		REGEN_ANYWHERE,
	};

	struct attr_zone_t {
		unsigned char attr[(const int)constval::ATTR_MAP_SIZE][(const int)constval::ATTR_MAP_SIZE];
	};

	struct map_info_t {
		long width;
		long height;

		std::vector<std::vector<attr_zone_t>>   attr_zones;
	};

	struct regen_mob_t {
		bool is_alive;
		DWORD vnum;
		double_t last_die_time;
		double_t time;
		long sx, sy, ex, ey;
		long x, y;
		mobproto::TMobTable* table;
	};


	struct player_info_t {
		long x, y;
		long level;
		long long exp;
	};


#pragma pack(1)
	struct SAttrMapHeader
	{
		WORD m_wMagic;
		WORD m_wWidth;
		WORD m_wHeight;
	};
#pragma pack()
}