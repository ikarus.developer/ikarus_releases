#pragma once
#ifndef __INCLUDE_ENUM_IKEX_HEADER__
#define __INCLUDE_ENUM_IKEX_HEADER__

namespace ikex {
	enum class iter_directive_t {
		stop = 0,
		next,
	};
}

#endif //__INCLUDE_ENUM_IKEX_HEADER__