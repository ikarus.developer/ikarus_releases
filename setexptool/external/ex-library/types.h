#pragma once

#ifndef __INCLUDE_TYPES_IKEX_HEADER__
#define __INCLUDE_TYPES_IKEX_HEADER__

namespace ikex {
	typedef size_t 				 hash_name_t;
	typedef unsigned long		 dword_t;
	typedef unsigned short		 word_t;
	typedef unsigned char		 byte_t;
	typedef int					 int_t;
	typedef bool				 bool8_t;
	typedef bool				 bool_t;
	typedef short				 short_t;
	typedef unsigned short		 ushort_t;
	typedef unsigned long		 ulong_t;
	typedef long				 long_t;
	typedef char				 char_t;
	typedef unsigned char		 uchar_t;
	typedef char*				 char_ptr_t;
	typedef unsigned char*		 uchar_ptr_t;
	typedef double				 double_t;
	typedef long double			 ldouble_t;
	typedef long long			 longlong_t;
	typedef unsigned long long	 ulonglong_t;
	typedef float				 float_t;
	typedef const char			 const_char_t;
	typedef void				 void_t;
}

#endif //__INCLUDE_TYPES_IKEX_HEADER__