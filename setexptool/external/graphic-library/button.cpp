/*
Copyright (c) 2019 IkarusDeveloper

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "stdafx.h"

#include "ikarus_gl.h"


namespace ikgl
{
	button::button(gui* guiptr) : 
	_on_click_event(nullptr) ,
	_cover({ nullptr, nullptr, nullptr }),
	window(guiptr)
	{
		this->__clear_button();
	}

	button::~button(){
		this->clear();
	}

	void_t button::clear(){
		this->window::clear();
		this->__clear_button();
	}

	void_t button::__clear_button() {
		this->_textline				= nullptr;
		this->_tooltip				= nullptr;
		this->_button_status		= button_status_t::BUTTON_STATUS_DEFAULT;
		this->_is_use_border		= false;
		this->_is_use_round			= false;
		this->_is_run_click_event	= false;


		utilities::destroy_obj_ptr(this->_cover.default_image);
		utilities::destroy_obj_ptr(this->_cover.down_image);
		utilities::destroy_obj_ptr(this->_cover.over_image);

		utilities::destroy_obj_ptr(this->_on_click_event);
		

		utilities::zero_obj(_cover);

		this->_colors.default_color	= BUTTON_DEFAULT_DEFAULT_COLOR;
		this->_colors.down_color	= BUTTON_DEFAULT_DOWN_COLOR;
		this->_colors.over_color	= BUTTON_DEFAULT_OVER_COLOR;

		this->_text_color			= TEXTLINE_DEFAULT_COLOR;
	}

	bool_t button::on_render(){
		return this->window::on_render();
	}

	void_t button::draw(hdc_t& hdc,  gdiplus_graphics_t* graphicsptr){
		if(!this->is_shown())
			return;
		
		this->window::draw_background(hdc, graphicsptr);
		this->draw_cover(hdc,  graphicsptr);
		this->window::draw_children(hdc,  graphicsptr);

		this->_is_to_render = false;
	}

	void_t button::set_text(wstring_t text){
		if (text.empty()){
			if (this->_textline != nullptr){
				this->remove_child(this->_textline);
				this->_textline = nullptr;
			}
		}

		else{
			if (this->_textline == nullptr){
				this->_textline = this->create_element<ikgl::text>();
				this->_textline->set_position(0, 0);
				this->_textline->set_size(this->get_size());
				this->_textline->set_horizontal_alignment(text_halign_t::TEXT_HALIGN_CENTER);
				this->_textline->set_vertical_alignment(text_valign_t::TEXT_VALIGN_CENTER);
				this->_textline->show();
				this->_textline->set_color(this->_text_color);
				this->_textline->disable_pick();
			}

			this->_textline->set_text(text);
		}

		this->call_render();
	}

	void_t button::set_text(string_t text){
		this->set_text(utilities::to_wstring(text));
	}

	void_t button::set_text(const_char_t* text, dword_t dwLen){
		string_t temp(text, dwLen);
		this->set_text(temp);
	}

	wstring_t button::get_textw(){
		if (this->_textline==nullptr)
			return L"";
		return this->_textline->get_textw();
	}

	string_t button::get_text(){
		if (this->_textline==nullptr)
			return "";
		return this->_textline->get_text();
	}

	void_t button::get_text(char_ptr_t szBuff, dword_t dwLen){
		if (this->_textline == nullptr){
			if (dwLen != 0)
				szBuff[0] = 0;
		}

		else
			this->_textline->get_text(szBuff,dwLen);
	}

	bool_t button::set_default_cover(const wstring_t path) {
		utilities::destroy_obj_ptr(this->_cover.default_image);

		gdiplus_image_t*& imageptr = this->_cover.default_image;

		imageptr = gdiplus_image_t::FromFile(path.c_str() , FALSE);
		bool_t success = imageptr!=nullptr && imageptr->GetLastStatus() == Gdiplus::Ok;

		if (success) {
			this->button::set_size(imageptr->GetWidth(), imageptr->GetHeight());
		}

		return success;
	}

	bool_t button::set_default_cover(const string_t path) {
		wstring_t wpath(path.begin(), path.end());
		return this->set_default_cover(wpath);
	}

	bool_t button::set_default_cover(const void_t* pData, dword_t len) {
		utilities::destroy_obj_ptr(this->_cover.default_image);
		this->_cover.default_image = utilities::get_image_from_buffer(pData, len);

		if (this->_cover.default_image) {
			this->set_size(this->_cover.default_image->GetWidth(), this->_cover.default_image->GetHeight());
		}

		return this->_cover.default_image != nullptr && this->_cover.default_image->GetLastStatus() == Gdiplus::Ok;
	}

	bool_t button::set_down_cover(const wstring_t path) {
		utilities::destroy_obj_ptr(this->_cover.down_image);
		gdiplus_image_t*& imageptr = this->_cover.down_image;

		imageptr = gdiplus_image_t::FromFile(path.c_str() , FALSE);
		return imageptr!=nullptr && imageptr->GetLastStatus() == Gdiplus::Ok;

	}

	bool_t button::set_down_cover(const string_t path) {
		wstring_t wpath(path.begin(), path.end());
		return this->set_down_cover(wpath);
	}

	bool_t button::set_down_cover(const void_t* pData, dword_t len) {
		utilities::destroy_obj_ptr(this->_cover.down_image);
		this->_cover.down_image = utilities::get_image_from_buffer(pData, len);
		return this->_cover.down_image != nullptr && this->_cover.down_image->GetLastStatus() == Gdiplus::Ok;
	}

	bool_t button::set_over_cover(const wstring_t path) {
		utilities::destroy_obj_ptr(this->_cover.over_image);
		gdiplus_image_t*& imageptr = this->_cover.over_image;

		imageptr = gdiplus_image_t::FromFile(path.c_str() , FALSE);
		return imageptr!=nullptr && imageptr->GetLastStatus() == Gdiplus::Ok;
	}

	bool_t button::set_over_cover(const string_t path) {
		wstring_t wpath(path.begin(), path.end());
		return this->set_over_cover(wpath);
	}

	bool_t button::set_over_cover(const void_t* pData, dword_t len) {
		utilities::destroy_obj_ptr(this->_cover.over_image);
		this->_cover.over_image = utilities::get_image_from_buffer(pData, len);
		return this->_cover.over_image != nullptr && this->_cover.over_image->GetLastStatus() == Gdiplus::Ok;
	}

	void_t button::set_tooltip(const string_t & text,const font_t& font,  const color_rgba_t& colorText, const color_rgba_t& colorBackground) {
		wstring_t wtext(text.begin(), text.end());
		this->set_tooltip(wtext, font, colorText, colorBackground);
	}

	void_t button::set_tooltip(const wstring_t & text, const font_t& font,  const color_rgba_t& text_color, const color_rgba_t& bg_color) {
		if (this->_tooltip) {
			this->remove_child(_tooltip);
		}

		this->_tooltip = this->create_element<window>();
		this->_tooltip->disable_pick();
		this->_tooltip->hide();
		this->_tooltip->set_background(&background_info_t(bg_color));

		if (this->_ws_name.empty()==false) {
			this->_tooltip->set_window_name(this->_ws_name + L"__tooltip__");
		}

		ikgl::text* text_tip = _tooltip->create_element<ikgl::text>();
		text_tip->set_text(text, true);
		text_tip->set_color(text_color);
		text_tip->set_font(&font);

		widget_size_t textsize = text_tip->get_size();
		this->_tooltip->set_size(textsize.width + 10, textsize.height +5);

		text_tip->move_horizontal_center();
		text_tip->move_vertical_center();

		text_tip->set_vertical_alignment(text_valign_t::TEXT_VALIGN_CENTER);
		text_tip->set_horizontal_alignment(text_halign_t::TEXT_HALIGN_CENTER);

		if (this->_ws_name.empty() == false) {
			text_tip->set_window_name(this->_ws_name + L"__tooltip__text__");
		}
	}

	void_t button::set_tooltip(const wstring_t& text, const font_t& font, const color_rgba_t& text_color, const color_rgba_t& bg_color, const widget_pos_t& pos) {
		if (this->_tooltip) {
			this->remove_child(_tooltip);
		}

		this->_tooltip = this->create_element<window>();
		this->_tooltip->disable_pick();
		this->_tooltip->hide();
		this->_tooltip->set_background(&background_info_t(bg_color));
		this->_tooltip->set_position(pos);

		ikgl::text* text_tip = _tooltip->create_element<ikgl::text>();
		text_tip->set_color(text_color);
		text_tip->set_font(&font);
		text_tip->set_text(text, true);

		widget_size_t textsize = text_tip->get_size();
		this->_tooltip->set_size(textsize.width + 10, textsize.height + 5);

		text_tip->move_horizontal_center();
		text_tip->move_vertical_center();

		text_tip->set_vertical_alignment(text_valign_t::TEXT_VALIGN_CENTER);
		text_tip->set_horizontal_alignment(text_halign_t::TEXT_HALIGN_CENTER);
	}

	bool_t button::is_covered(){
		return this->_cover.default_image!=nullptr;
	}

	void_t button::set_colors(button_color_info_t* pinfo){
		memcpy(&this->_colors, pinfo, sizeof(this->_colors));
		this->call_render();
	}

	void_t button::get_colors(button_color_info_t* pinfo){
		memcpy(pinfo, &this->_colors, sizeof(this->_colors));
	}

	void_t button::set_text_color(mc_colorref_t color){
		this->_text_color = color;

		if (this->_textline)	{
			this->_textline->set_color(color);
			this->call_render();
		}
	}

	void_t button::set_on_click_event(widget_event_t event){
		if(this->_on_click_event)
			delete(this->_on_click_event);

		this->_on_click_event = new widget_event_t(event);
	}

	bool_t button::is_over(){
		return this->_button_status == button_status_t::BUTTON_STATUS_OVER;
	}

	bool_t button::is_up(){
		return this->_button_status == button_status_t::BUTTON_STATUS_DEFAULT;
	}

	bool_t button::is_down(){
		return this->_button_status == button_status_t::BUTTON_STATUS_DOWN;
	}

	void_t button::enable_border(){
		this->_is_use_border = true;
		this->window::call_render();
	}

	void_t button::disable_border(){
		this->_is_use_border = false;
		this->window::call_render();
	}

	void_t button::enable_round(){
		this->_is_use_round = true;
		this->window::call_render();
	}

	void_t button::disable_round(){
		this->_is_use_round = false;
		this->window::call_render();
	}

	void_t button::on_mouseleft_up(){
		//* small check to prevent
		//* recursive event calling
		if (!this->_is_run_click_event) {
			this->_is_run_click_event = true;
			if (this->_on_click_event)
				if ((*this->_on_click_event)(this->get_id(), __ikgl_to_window_pointer(this)) != widget_event_return_t::KEEP_CALLBACK)
					utilities::destroy_obj_ptr(this->_on_click_event);

			this->_is_run_click_event = false;
		}

		this->_button_status = this->get_gui() && this->is_in(this->get_gui()->get_mouse_position()) ? button_status_t::BUTTON_STATUS_OVER : button_status_t::BUTTON_STATUS_DEFAULT;
		this->window::on_mouseleft_up();
		this->call_render();
	}

	void_t button::on_mouseleft_down(){
		this->window::on_mouseleft_down();
		this->_button_status = button_status_t::BUTTON_STATUS_DOWN;
		this->call_render();
	}

	void_t button::on_mouseover_in(){
		this->_button_status = button_status_t::BUTTON_STATUS_OVER;
		this->call_render();
		this->window::on_mouseover_in();

		if(this->_gui)
			this->_gui->set_button_cursor();
		
		if (this->_tooltip) {
			this->_tooltip->set_alpha(0.0f);
			this->_tooltip->fade_to(1.0f, 0.4f);
			this->_tooltip->show();
		}
	}

	void_t button::on_mouseover_out(){
		this->_button_status = button_status_t::BUTTON_STATUS_DEFAULT;
		this->call_render();
		this->window::on_mouseover_out();

		if(this->_gui)
			this->_gui->set_normal_cursor();

		if (this->_tooltip) {
			widget_event_t hideFunc = [this] __ikgl_wevent_lambda_type_func {
				this->_tooltip->hide();
				return widget_event_return_t::KEEP_CALLBACK;
			};

			this->_tooltip->fade_to(0.0f, 0.4f, &hideFunc);
		}
	}

	void_t button::on_mousemove() {
		this->window::on_mousemove();
	}

	void_t button::set_size(widget_size_t* size){
		if (size)
			this->button::set_size(size->width,size->height);
	}

	void_t button::set_size(widget_size_t& size){
		this->button::set_size(size.width,size.height);
	}

	void_t button::set_size(coord_t w, coord_t h){
		this->window::set_size(w,h);
		
		if (this->_textline) {
			this->_textline->set_size(w, h);
		}

		this->call_render();
	}

	void_t button::draw_cover(hdc_t& hdc, gdiplus_graphics_t* graphicsptr) {

		gdiplus_image_t* to_draw = nullptr;
		const float_t alpha = this->get_global_alpha();
		const float_t degree = this->get_global_rotation();
		gdiplus_rect_t rect = this->get_global_rect().to_gdiplus_rect();

		if (degree != 0.0f) {
			utilities::normalize_rect(rect, degree);
			external::rotate_trasform(graphicsptr, degree);
		}

		switch (this->_button_status) {
		case button_status_t::BUTTON_STATUS_DOWN:
			to_draw = this->_cover.down_image;
			break;
		case button_status_t::BUTTON_STATUS_OVER:
			to_draw = this->_cover.over_image;
			break;

		default:
			to_draw = this->_cover.default_image;
			break;
		}

		if (to_draw) {

			if (alpha == 1.f) {
				external::draw_image(graphicsptr, to_draw, rect);
			}

			else {
				static gdiplus_image_attributes_t img_attr;
				gdiplus_color_matrix_t matrix = {
					 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
					 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
					 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
					 0.0f, 0.0f, 0.0f, alpha,0.0f,
					 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
				};

				img_attr.SetColorMatrix(&matrix);
				external::draw_image(graphicsptr,
					to_draw,
					rect,
					0, 0,
					(int_t)to_draw->GetWidth(), (int_t)to_draw->GetHeight(),
					Gdiplus::UnitPixel,
					&img_attr
				);
			}
		}

		else {
			color_rgba_t color = 0;

			switch (_button_status) {
			case button_status_t::BUTTON_STATUS_DOWN:
				color = this->_colors.down_color;
				break;
			case button_status_t::BUTTON_STATUS_OVER:
				color = this->_colors.over_color;
				break;

			default:
				color = this->_colors.default_color;
				break;
			}

			color = color_rgba_t(color, alpha);

			if (this->_is_use_round) {
				if (!this->_is_use_border)
					draw_round_rect(graphicsptr, rect, color);

				else
					draw_round_rect_bordered(graphicsptr, rect, color, color_rgba_t(this->_colors.border_color, alpha), this->_colors.border_width);
			}

			else {
				if (!this->_is_use_border)
					draw_rect(graphicsptr, rect, color);
				else
					draw_rect_bordered(graphicsptr, rect, color, color_rgba_t(this->_colors.border_color, alpha), this->_colors.border_width);
			}
		}



		if (degree != 0.0f) {
			external::rotate_trasform(graphicsptr, -degree);
		}
	}

	ikgl::text* button::get_text_instance()
	{
		return this->_textline;
	}

	void_t button::hide_children() {
		
		for (auto ptr : this->_children_list) {
			if(ptr == this->_textline){
				continue;
			}

			if (ptr == this->_tooltip) {
				continue;
			}

			ptr->hide();
		}

	}

	void_t button::remove_children() {
		
		window* ptr=nullptr;

		for (auto iter = this->_children_list.begin(); iter != this->_children_list.end();) {
			auto ptr = *iter;

			if (this->_textline) {
				if (ptr->get_id() == this->_textline->get_id()) {
					iter++;
				}
			}

			if (this->_tooltip) {
				if (ptr->get_id() == this->_tooltip->get_id()) {
					iter++;
				}
			}

			delete(*iter);
			iter = this->_children_list.erase(iter);
		}
	}
}


