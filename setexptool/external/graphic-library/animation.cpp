/*
Copyright (c) 2019 IkarusDeveloper

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "stdafx.h"
#include "ikarus_gl.h"

namespace ikgl {

	animation::animation(gui* gui) :
	window(gui)
	{
		this->__clear_animation();
	}
	
	
	
	animation::~animation() {
		this->clear();
	}


	
	
	void_t animation::clear() {
		this->__clear_animation();
		this->window::clear();
	}



	void_t animation::draw(hdc_t& hdc,  gdiplus_graphics_t* graphicsptr) {
		this->window::draw_background(hdc, graphicsptr);

		if (this->_images.empty() == false) {
			auto* image = this->__get_frame();

			const float_t alpha  = this->get_global_alpha();
			const float_t degree = this->get_global_rotation();
			widget_pos_t gpos    = this->get_global_position();
			
			//* to implement rotation
			if (degree != 0.0f) {
				external::rotate_trasform(graphicsptr,degree);
				utilities::normalize_position(gpos, degree);
			}

			//* if possible use draw0 to improve efficient
			if (alpha == 1.f && this->is_static_draw()) {
				gdiplus_status_t result;
				if ((result = external::draw_image(graphicsptr,
					image,
					gdiplus_real_t(gpos.x), gdiplus_real_t(gpos.y),
					gdiplus_real_t(this->_image_size.width), gdiplus_real_t(this->_image_size.height)
				)) != Gdiplus::Ok) {
					__ikgl_error("is not drawing animation (code %s ) \n", utilities::get_gdiplus_status_str_name(result));
				}

				//* normalizing the rotation after the paint
				if (degree != 0.0f) {
					external::rotate_trasform(graphicsptr, -degree);
				}
			}


			//* for nonstatic draw use draw1
			else {
				//* making the image attributes
				gdiplus_image_attributes_t img_attr;

				const float_t a = __ikgl_minmax(0.0f, this->_apply_color.a, 1.0f);
				const float_t r = __ikgl_minmax(0.0f, this->_apply_color.r*a, 1.0f);
				const float_t g = __ikgl_minmax(0.0f, this->_apply_color.g*a, 1.0f);
				const float_t b = __ikgl_minmax(0.0f, this->_apply_color.b*a, 1.0f);

				const float_t al = alpha;

				gdiplus_color_matrix_t matrix = {
					 1.0f,    0.0f,    0.0f,    0.0f,    0.0f,
					 0.0f,    1.0f,    0.0f,    0.0f,    0.0f,
					 0.0f,    0.0f,    1.0f,    0.0f,    0.0f,
					 0.0f,    0.0f,    0.0f,    alpha,   0.0f,
					 r,	     g,       b,       0.0f,       1.0f,
				};

				img_attr.SetColorMatrix(&matrix);

				//* calculating the rect to repeat the image
				float_t left_rect  = -(this->_rect_scale.left_scale - 1.f);
				float_t right_rect =   this->_rect_scale.right_scale;

				const float_t image_width = float_t(this->_image_size.width);
				const float_t image_height = float_t(this->_image_size.height);

				const float_t x_scale = image_width / float_t(image->GetWidth());
				const float_t y_scale = image_height / float_t(image->GetHeight());

				external::scale_trasform(
					graphicsptr,
					x_scale,
					y_scale
				);

				coord_t x = gpos.x;

				//* starting a loop from left to right (multiple drawing to repeate the image)
				while (left_rect < right_rect) {
					coord_t y = gpos.y;

					float_t rect_horizontal = 0.0f;

					//* calculating the rect need to draw
					if (left_rect < 0.0f) {
						rect_horizontal = left_rect - (float_t(int_t(left_rect)));
						if (rect_horizontal == 0.0f) {
							rect_horizontal = left_rect >= 0.0f ? 1.f : -1.f;
						}
					}

					else {
						if (right_rect - left_rect < 1.0f)
							rect_horizontal = right_rect - left_rect;
						else
							rect_horizontal = 1.0f;
					}

					//* calculating vertical rect
					float_t top_rect = -(this->_rect_scale.top_scale - 1.0f);
					float_t bottom_rect = this->_rect_scale.bottom_scale;

					//* starting a loop from top to bottom (multiple draw image)
					while (top_rect < bottom_rect) {
						float_t rect_vertical = 0.0f;

						//* calculating the rect to draw (vertical)
						if (top_rect < 0.0f) {
							rect_vertical = top_rect - (float_t(int_t(top_rect)));
							if (rect_vertical == 0.0f) {
								rect_vertical = top_rect >= 0.0f ? 1.f : -1.f;
							}
						}

						else {
							if (bottom_rect - top_rect < 1.0f)
								rect_vertical = bottom_rect - top_rect;
							else
								rect_vertical = 1.0f;
						}


						//* settings rects
						gdiplus_rectf_t src_rect;
						gdiplus_rectf_t dest_rect;

						const float_t image_original_width = float_t(image->GetWidth());
						const float_t image_original_height = float_t(image->GetHeight());

						//* source rect
						src_rect.X = rect_horizontal > 0.0f ? 0 : image_original_width * (1.0f + rect_horizontal);
						src_rect.Y = rect_vertical > 0.0f ? 0 : image_original_height * (1.0f + rect_vertical);

						src_rect.Width = gdiplus_real_t(float_t(image_original_width) * __ikgl_max(rect_horizontal, -rect_horizontal));
						src_rect.Height = gdiplus_real_t(float_t(image_original_height) * __ikgl_max(rect_vertical, -rect_vertical));

						//* dest rect
						dest_rect.X = gdiplus_real_t(x);
						dest_rect.Y = gdiplus_real_t(y);

						dest_rect.Width = gdiplus_real_t(image_original_width * __ikgl_max(rect_horizontal, -rect_horizontal));
						dest_rect.Height = gdiplus_real_t(image_original_height * __ikgl_max(rect_vertical, -rect_vertical));


						//to fix the scale transform about coordinate
						dest_rect.X /= x_scale;
						dest_rect.Y /= y_scale;

						//* drawing the rect of image calculated
						if (external::draw_image(graphicsptr, image, dest_rect, src_rect.X, src_rect.Y, src_rect.Width, src_rect.Height, Gdiplus::UnitPixel, &img_attr) != Gdiplus::Ok) {
							__ikgl_error("is not drawing image correctly (is not static draw) ");
						}

						//* upgrading values 
						y += coord_t(dest_rect.Height);
						top_rect += __ikgl_max(rect_vertical, -rect_vertical);
					}

					//*upgrading values
					x += coord_t(image_width * __ikgl_max(rect_horizontal, -rect_horizontal));
					left_rect += __ikgl_max(rect_horizontal, -rect_horizontal);
				}

				//* resetting trasform
				external::reset_trasform(graphicsptr);
			}
		}

		this->window::draw_children(hdc, graphicsptr);
		this->_is_to_render = false;
	}



	bool_t animation::on_render() {
		bool is_window_on_render = this->window::on_render();

		//* small check to improve the efficient
		//* when the animation doesn't need the on_render methods to be perform
		if (!this->is_shown()) {
			return false;
		}
		else if (this->_images.empty() && this->_children_list.empty()) {
			return false;
		}

		//* check about current frame and next change
		if (this->_images.empty() == false) {
			auto time = utilities::get_time_double();
			if (this->_next_frame_change <= time) {
				const int_t extraframe = int_t((time - this->_next_frame_change) / this->_delay);

				this->_cur_frame += 1;
				this->_cur_frame += extraframe;
				this->_cur_frame %= this->_images.size();

				this->_next_frame_change += this->_delay * double_t(extraframe) + 1.0;
				this->call_render();
			}
		}

		if (is_window_on_render || this->is_to_render()) {
			return true;
		}
		else {
			return false;
		}
	}




	//this methods invlidate the rect setting because it need to set the size of the image to the value to set
	void_t animation::set_size(const coord_t width, const coord_t height){
		this->_image_size = { width, height };
		this->window::set_size(this->_rect_scale.get_size(this->_image_size));

		this->call_render();
	}


	void_t animation::set_size(const widget_size_t& size) {
		this->animation::set_size(size.width, size.height);
	}



	bool_t animation::append_image(const string_t& path) {
		wstring_t wpath(path.begin(), path.end());
		return this->append_image(wpath);
	}



	bool_t animation::append_image(const wstring_t& path) {
		std::unique_ptr<gdiplus_image_t> image (gdiplus_image_t::FromFile(path.c_str() , FALSE));
		bool_t success   = image!=nullptr && image->GetLastStatus() == Gdiplus::Ok;

		if (success) {
			this->_image_size = {image->GetWidth(), image->GetHeight()};
			this->window::set_size(image->GetWidth(), image->GetHeight());

			this->_images.push_back(image.release());
		}

		
		return success;
	}



	bool_t animation::append_image(const void_t* data, const dword_t len){
		std::unique_ptr<gdiplus_image_t> image(utilities::get_image_from_buffer(data,len));

		//assign the size to the image
		if (image && image->GetLastStatus() == Gdiplus::Ok) {
			this->animation::set_size(image->GetWidth(), image->GetHeight());
			this->_images.push_back(image.release());
			return true;
		}
		
		else {
			return false;
		}
	}



	bool_t animation::append_image(hbitmap_t hbitmap, gdiplus_pixel_format_t pixel_format) {
		bitmap_t source_info = { 0 };
		if( !external::get_object( hbitmap, sizeof( source_info ), &source_info ) )
			return Gdiplus::GenericError;

		gdiplus_status_t s;

		std::unique_ptr< gdiplus_bitmap_t > target( new gdiplus_bitmap_t( source_info.bmWidth, source_info.bmHeight, pixel_format ) );
		if( !target.get() )
			return false;

		if( ( s = target->GetLastStatus() ) != Gdiplus::Ok )
			return false;

		gdiplus_bitmap_data_t target_info;
		gdiplus_rect_t rect( 0, 0, source_info.bmWidth, source_info.bmHeight );

		s = target->LockBits( &rect, Gdiplus::ImageLockModeWrite, pixel_format, &target_info );
		if( s != Gdiplus::Ok )
			return false;

		if( target_info.Stride != source_info.bmWidthBytes )
			return false; // pixel_format is wrong!

		memcpy( target_info.Scan0, source_info.bmBits, size_t(source_info.bmWidthBytes) * size_t(source_info.bmHeight) );

		s = target->UnlockBits( &target_info );
		if( s != Gdiplus::Ok )
			return false;


		this->animation::set_size(source_info.bmWidth, source_info.bmHeight);
		this->_images.push_back(target.release());
		return true;
	}



	//this is to repeat the image (or to truncate it for value < 1.0f)
	void_t animation::set_rect_scale(const float_t rLeft, const float_t rTop, const float_t rRight, const float_t rBottom){
		rect_scale_t r;
		r.left_scale	= rLeft;
		r.top_scale		= rTop;
		r.right_scale	= rRight;
		r.bottom_scale	= rBottom;

		this->set_rect_scale(r);
	}




	void_t animation::set_rect_scale(const rect_scale_t& rScale){
		this->_rect_scale = rScale;
		this->window::set_size(this->_rect_scale.get_size(this->_image_size));
		this->call_render();
	}





	rect_scale_t animation::get_rect_scale(){
		return this->_rect_scale;
	}




	void_t animation::get_rect_scale(rect_scale_t& rScale){
		rScale = this->_rect_scale;
	}




	void_t animation::get_rect_scale(float_t& rLeft, float_t& rTop, float_t& rRight, float_t& rBottom){
		rLeft	= this->_rect_scale.left_scale;
		rTop	= this->_rect_scale.top_scale;
		rRight	= this->_rect_scale.right_scale;
		rBottom = this->_rect_scale.bottom_scale;
	}


	bool_t animation::is_static_draw() {
		if (_images.empty()) {
			return true;
		}

		else {
			gdiplus_image_t& img = *this->_images[0];
			return img.GetWidth() == this->_image_size.width && img.GetHeight() == this->_image_size.height &&
				this->_rect_scale.top_scale  == 1.0f && this->_rect_scale.bottom_scale == 1.0f &&
				this->_rect_scale.left_scale == 1.0f && this->_rect_scale.right_scale == 1.0f &&
				this->_apply_color.a == 0.0f && this->_apply_color.r == 0.0f && this->_apply_color.g == 0.0f &&
				this->_apply_color.b == 0.0f;
		}
	}


	void_t animation::apply_color(const color_rgba_t& color) {
		this->_apply_color = color;
		this->call_render();
	}




	void_t animation::set_delay(double_t seconds) {
		this->_delay = seconds;
	}



	double_t animation::get_delay() {
		return this->_delay;
	}


	void_t animation::__clear_animation() {
		this->_rect_scale = {1.0f,1.0f,1.0f,1.0f};
		utilities::zero_obj(this->_image_size);
		utilities::zero_obj(this->_apply_color);

		this->_delay = ANIMATION_DEFAULT_DELAY;
		this->_next_frame_change = utilities::get_time_double() + ANIMATION_DEFAULT_DELAY;
		this->_cur_frame = 0;
	}


	gdiplus_image_t* animation::__get_frame() {
		if (this->_images.empty() == false) {
			const double_t now = utilities::get_time_double();

			if (this->_next_frame_change <= now) {
				auto index = this->_cur_frame + 1;
				index += int_t((now- this->_next_frame_change)/ this->_delay);
				index %= this->_images.size();

				return this->_images[index];
			}

			else {
				return this->_images[this->_cur_frame% this->_images.size()];
			}
		}

		else {
			return nullptr;
		}
	}
}