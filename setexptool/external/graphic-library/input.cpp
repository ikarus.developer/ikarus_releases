#include "stdafx.h"
#include "ikarus_gl.h"

#define to_secret_text(text) std::replace_if(text.begin(),text.end(),[] (const wchar_t& ch)->bool_t{return true;},L'*');

namespace ikgl {
	input::input(gui* guiptr) :
	text(guiptr),
	_cursor_position(0),
	_input_limit(INPUT_LIMIT_DEFAULT),
	_is_show_cursor(true),
	_is_secret_enabled(false)
	{
	}


	input::~input() {
		text::clear();
	}

	void_t input::draw(hdc_t & hdc, gdiplus_graphics_t * graphicsptr) {
		this->window::draw_background(hdc, graphicsptr);

		if (!this->is_focused() && this->_ws_text.empty()) {
			if(this->_default_text.empty() == false)
				this->__draw_default_text(hdc, graphicsptr);
		}


		else {
			if (this->_is_secret_enabled == false)
				this->text::draw_text(hdc, graphicsptr);
			
			else 
				this->__draw_secret_text(hdc, graphicsptr);
			

			if (this->is_focused() && this->_is_show_cursor)
				this->__draw_cursor(hdc, graphicsptr);
		}

		this->window::draw_children(hdc, graphicsptr);
		this->_is_to_render = false;
	}

	void_t input::__draw_cursor(hdc_t & hdc, gdiplus_graphics_t * graphicsptr) {
		const auto emsize = this->_font_info.size;
		
		//* making the global rect
		auto global_rect = this->get_global_rect().to_gdiplus_rect();
		global_rect.Y    += (int_t) (float_t(emsize)*0.3333f);

		global_rect.Width  = 1;
		global_rect.Height = (int_t) emsize;

		//* getting the size of the text before the cursor
		if (this->_cursor_position != 0) {
			wstring_t subtext = this->_ws_text.substr(0, this->_cursor_position);
			if (this->_is_secret_enabled) {
				to_secret_text(subtext);
			}

			auto size = utilities::measure_string_strict(emsize, graphicsptr, subtext.c_str(), (int_t) subtext.length(), this->_font, this->get_global_position());
			global_rect.X += size.width;
		}
		
		//* fixing alignment
		this->__normalize_by_alignment(global_rect);

		//* drawing the cursor
		draw_rect(graphicsptr, global_rect, this->_color);
	}

	void_t input::__draw_secret_text(hdc_t & hdc, gdiplus_graphics_t * graphicsptr) {
		//* creating a solid brush using the color set
		//* and the global alpha
		gdiplus_solid_brush_t brush (
			gdiplus_color_t (
				color_rgba_t(
					this->_color, this->get_global_alpha()
				).to_gdiplus_argb()
			)
		);


		const auto degree = this->get_global_rotation();
		auto pos          = this->get_global_position();
		
		//* used to draw without padding
		gdiplus_string_format_t format = gdiplus_string_format_t::GenericTypographic();
		format.SetTrimming(gdiplus_string_trimming_t::StringTrimmingNone);
		format.SetFormatFlags(format.GetFormatFlags()|gdiplus_string_format_flag_t::StringFormatFlagsMeasureTrailingSpaces);

		this->__normalize_by_alignment(pos);

		if (degree != 0.0f) {
			external::rotate_trasform(graphicsptr,degree);
			utilities::normalize_position(pos, degree);
		}

		//* basically draw the text
		wstring_t text = this->_ws_text;
		to_secret_text(text);
		graphicsptr->DrawString(text.c_str(), (int_t) text.length(), this->_font, pos.to_gdiplus_pointf(), &format, &brush);

		if (degree != 0.0f) {
			external::rotate_trasform(graphicsptr,-degree);
		}
	}

	void_t input::__draw_default_text(hdc_t& hdc, gdiplus_graphics_t* graphicsptr) {
		//* creating a solid brush using the color set
		//* and the global alpha
		gdiplus_solid_brush_t brush(
			gdiplus_color_t(
				color_rgba_t(
					this->_color, this->get_global_alpha() * 0.35f
				).to_gdiplus_argb()
			)
		);

		//* setting the format alignments
		gdiplus_string_format_t format = gdiplus_string_format_t::GenericTypographic();

		//* setting tab stops
		const auto tabcount = std::count(this->_default_text.begin(), this->_default_text.end(), (wchar_t)'\t');

		if (tabcount != 0) {
			std::vector<gdiplus_real_t> vectab;
			vectab.resize(tabcount, 15.f);
			format.SetTabStops(0, (INT)tabcount, vectab.data());
		}


		const auto degree = this->get_global_rotation();
		auto pos = this->get_global_position();
		this->__normalize_by_alignment(pos, this->_default_text);

		if (degree != 0.0f) {
			external::rotate_trasform(graphicsptr, degree);
			utilities::normalize_position(pos, degree);
		}

		//* basically draw the text
		graphicsptr->DrawString(this->_default_text.c_str(), (int_t)this->_default_text.length(), this->_font, pos.to_gdiplus_pointf(), &format, &brush);

		if (degree != 0.0f) {
			external::rotate_trasform(graphicsptr, -degree);
		}
	}

	void_t input::on_key_down(const key_message_info_t & message_info) {
		if (message_info.message == WM_CHAR) {
			this->__on_char_down((wchar_t)message_info.wparam);
		}

		else {
			this->__on_key_down(message_info.wparam);
		}

		this->window::on_key_down(message_info);
	}

	void_t input::on_mouseleft_down() {
		this->__adjust_cursor_position();
		this->window::on_mouseleft_down();
	}

	void_t input::enable_secret() {
		this->_is_secret_enabled = true;
	}

	void_t input::disable_secret() {
		this->_is_secret_enabled = false;
	}

	bool_t input::is_secret() {
		return this->_is_secret_enabled;
	}

	void_t input::set_cursor_pos(const int_t pos) {
		this->_cursor_position = __ikgl_minmax(0, pos, (int_t) this->_ws_text.length());
		this->call_render();
	}

	void_t input::set_input_limit(dword_t limit) {
		this->_input_limit = limit;
	}

	void_t input::set_default_text(const wstring_t& default_text) {
		this->_default_text = default_text;
		this->call_render();
	}

	void_t input::set_default_text(const string_t& default_text) {
		this->_default_text = { default_text.begin(), default_text.end() };
		this->call_render();
	}

	dword_t input::get_input_limit() {
		return this->_input_limit;
	}

	void_t input::set_focus() {
		this->window::set_focus();
		this->_is_show_cursor = true;

		this->add_timer(0.6, [this] __ikgl_wevent_lambda_type_func {
			this->__toogle_cursor();
			return this->is_focused() ? widget_event_return_t::KEEP_CALLBACK : widget_event_return_t::UNSET_CALLBACK;
		});

		this->__adjust_cursor_position();
	}

	void_t input::__on_char_down(wchar_t key) {
		//* input limit ll return if the key is != from carriage return
		if((dword_t)this->_ws_text.length() >= this->_input_limit && key != 13)
			return;


		//* check to avoid use of special charater
		switch (key) {
			case 0:		return; //null character
			case 1:		return; //SOH
			case 2:		return; //STX
			case 3:		return; //ETX
			case 4:		return; //EOT
			case 5:		return; //ENQ
			case 6:		return; //ACK
			case 7:		return; //BEL
			case 8:		return; //backspace
			case 10:	return; //NL
			case 11:	return; //VT
			case 12:	return; //NP
			case 13:	if (this->_cursor_position != 0) { this->_cursor_position = 0; this->call_render(); } return;
			case 14:	return; //SO
			case 15:	return; //SI
			case 16:	return; //DLE
			case 17:	return; //DC1
			case 18:	return; //DC2
			case 19:	return; //DC3
			case 20:	return; //DC4
			case 21:	return; //NAK
			case 22:	return; //SYN
			case 23:	return; //ETB
			case 24:	return; //CAN
			case 25:	return; //EM
			case 26:	return; //SUB
			case 27:	return; //ESC
			case 28:	return; //FS
			case 29:	return; //GS
			case 30:	return; //RS
			case 31:	return; //US

			
			default:	break;
		}

		auto prev_text = this->_ws_text.substr(0, this->_cursor_position);
		auto next_text = this->_ws_text.substr(this->_cursor_position);

		this->set_text(prev_text + key + next_text);
		this->_cursor_position++;
	}

	void_t input::__on_key_down(wparam_t wparam) {
		switch (wparam) {
			case VK_LEFT:
				__on_move_cursor(-1);
				break;

			case VK_RIGHT:
				__on_move_cursor(+1);
				break;

			case VK_DELETE:
				__on_delete();
				break;

			case VK_BACK:
				__on_backspace();
				break;

			case VK_CANCEL:
				__on_cancel();
				break;

			case VK_ESCAPE:
				__on_escape();
				break;
		}
	}

	void_t input::__on_move_cursor(int_t pos) {
		this->_cursor_position = __ikgl_minmax(0, this->_cursor_position + pos, (int_t)this->_ws_text.length());
	}

	void_t input::__on_delete() {
		if (this->_cursor_position == this->_ws_text.length()) 
			return;

		auto prev_text = this->_ws_text.substr(0, this->_cursor_position);
		auto next_text = this->_ws_text.substr(static_cast<size_t>(this->_cursor_position)+1);
		this->set_text(prev_text + next_text);
	}

	void_t input::__on_backspace() {
		if (this->_cursor_position == 0) 
			return;

		auto prev_text = this->_ws_text.substr(static_cast<size_t>(0), static_cast<size_t>(this->_cursor_position)-1);
		auto next_text = this->_ws_text.substr(static_cast<size_t>(this->_cursor_position));
		this->set_text(prev_text + next_text);
		this->_cursor_position--;
	}

	void_t input::__on_cancel() {
		this->kill_focus();
	}

	void_t input::__on_escape() {
		this->kill_focus();
	}

	void_t input::__toogle_cursor() {
		this->_is_show_cursor = !this->_is_show_cursor;
		this->call_render();
	}

	void_t input::__adjust_cursor_position() {
		auto __get_cur_pos_by_text = [this] (const wstring_t& text, gdiplus_graphics_t& graphics, widget_pos_t& local_pos) ->int_t{
			for (size_t i = 0; i < this->_ws_text.length(); i++) {
				if (utilities::measure_string_strict(this->_font_info.size, &graphics, text.c_str(), (int_t) i, this->_font, this->_pos).width > local_pos.x - 3) {
					return static_cast<int_t>(i-1);
				}
			}

			if (utilities::measure_string_strict(this->_font_info.size, &graphics, text.c_str(), (int_t) text.length(), this->_font, this->_pos).width > local_pos.x - 3) {
				return (int_t) this->_ws_text.length() - 1;
			} else {
				return (int_t) this->_ws_text.length();
			}
		};



		//* moving cursor to the right position
		if (this->_gui) {
			auto global_pos = this->get_global_position();
			this->__normalize_by_alignment(global_pos);

			auto local_pos = this->_gui->get_mouse_position();
			local_pos -= global_pos;

			gdiplus_graphics_t graphics(this->_gui->get_hdc());
			external::set_text_rendering_hint_antialias(&graphics);

			if (local_pos.x > 0) {
				
				if (this->_is_secret_enabled == false) {
					this->_cursor_position = __get_cur_pos_by_text(this->_ws_text, graphics, local_pos);
				}

				else {
					 wstring_t text = this->_ws_text;
					 to_secret_text(text);
					 this->_cursor_position = __get_cur_pos_by_text(text, graphics, local_pos);
				}
			}
		}
	}

	
}
