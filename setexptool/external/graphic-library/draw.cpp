/*
Copyright (c) 2019 IkarusDeveloper

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "stdafx.h"
#include "ikarus_gl.h"


namespace ikgl
{
	
	void_t draw_round_rect(gdiplus_graphics_t* grptr, const gdiplus_rect_t& rect, const color_rgba_t& color) {
		gdiplus_graphics_path_t path;
		gdiplus_int_t radius = __ikgl_min(rect.Width, rect.Height) / 50;
		if(radius == 0)
			radius = 1;

		path.AddLine(rect.X + radius, rect.Y, rect.X + rect.Width - (radius * 2), rect.Y);
		path.AddArc(rect.X + rect.Width - (radius * 2), rect.Y, radius * 2, radius * 2, 270, 90);
		path.AddLine(rect.X + rect.Width, rect.Y + radius, rect.X + rect.Width, rect.Y + rect.Height - (radius * 2));
		path.AddArc(rect.X + rect.Width - (radius * 2), rect.Y + rect.Height - (radius * 2), radius * 2, radius * 2, 0, 90);
		path.AddLine(rect.X + rect.Width - (radius * 2), rect.Y + rect.Height, rect.X + radius, rect.Y + rect.Height);
		path.AddArc(rect.X, rect.Y + rect.Height - (radius * 2), radius * 2, radius * 2, 90, 90);
		path.AddLine(rect.X, rect.Y + rect.Height - (radius * 2), rect.X, rect.Y + radius);
		path.AddArc(rect.X, rect.Y, radius * 2, radius * 2, 180, 90);
		path.CloseFigure();

		grptr->FillPath(&gdiplus_solid_brush_t(color.to_gdiplus_argb()) , &path);
	}




	void_t draw_round_rect_bordered(  gdiplus_graphics_t* grptr, const gdiplus_rect_t& rect, const color_rgba_t& color, const color_rgba_t& border_color, uint_t bordersize ) {
		gdiplus_graphics_path_t path;
		gdiplus_int_t radius = __ikgl_min(rect.Width, rect.Height) / 50;
		if(radius == 0)
			radius = 1;

		path.AddLine(rect.X + radius, rect.Y, rect.X + rect.Width - (radius * 2), rect.Y);
		path.AddArc(rect.X + rect.Width - (radius * 2), rect.Y, radius * 2, radius * 2, 270, 90);
		path.AddLine(rect.X + rect.Width, rect.Y + radius, rect.X + rect.Width, rect.Y + rect.Height - (radius * 2));
		path.AddArc(rect.X + rect.Width - (radius * 2), rect.Y + rect.Height - (radius * 2), radius * 2, radius * 2, 0, 90);
		path.AddLine(rect.X + rect.Width - (radius * 2), rect.Y + rect.Height, rect.X + radius, rect.Y + rect.Height);
		path.AddArc(rect.X, rect.Y + rect.Height - (radius * 2), radius * 2, radius * 2, 90, 90);
		path.AddLine(rect.X, rect.Y + rect.Height - (radius * 2), rect.X, rect.Y + radius);
		path.AddArc(rect.X, rect.Y, radius * 2, radius * 2, 180, 90);
		path.CloseFigure();

		//draw fill
		grptr->FillPath(&gdiplus_solid_brush_t(color.to_gdiplus_argb()) , &path);

		//draw border
		gdiplus_solid_brush_t borderBrush(border_color.to_gdiplus_argb());
		grptr->DrawPath(&gdiplus_pen_t(&borderBrush, gdiplus_real_t(bordersize)) , &path);
	}




	void_t draw_rect_bordered(  gdiplus_graphics_t* grptr, const gdiplus_rect_t& rect, const color_rgba_t& color, const color_rgba_t& border_color, uint_t bordersize ) {
		draw_rect(grptr, rect, color);

		gdiplus_solid_brush_t borderBrush(border_color.to_gdiplus_argb());
		grptr->DrawRectangle(&gdiplus_pen_t(&borderBrush, gdiplus_real_t(bordersize)), rect);
	}




	void_t draw_rect(  gdiplus_graphics_t* grptr, const gdiplus_rect_t& rect, const color_rgba_t& color ) {
		grptr->FillRectangle(&gdiplus_solid_brush_t(color.to_gdiplus_argb()), rect);
	}


}

