/*
Copyright (c) 2019 IkarusDeveloper

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "stdafx.h"
#include "ikarus_gl.h"
#include "gui.h"

#define __critical_scope() std::lock_guard<mutex_t> lock(this->_mutex)

namespace ikgl {

	std::function<void_t(const_char_t*, valist_t)> IKGL_global_debug = utilities::default_debug;
	std::function<void_t(const wchar_t*, valist_t)> IKGL_global_debugW = utilities::default_debugW;

	std::function<void_t(const_char_t*, valist_t)> IKGL_global_error = utilities::default_debug;
	std::function<void_t(const wchar_t*, valist_t)> IKGL_global_errorW = utilities::default_debugW;

	mutex_t IKGL_global_mutex_Debug;

	gdiplus_graphics_t* __new_graphics_instance(hdc_t hdcMem) {
		auto grp = new gdiplus_graphics_t(hdcMem);
		grp->SetTextRenderingHint(Gdiplus::TextRenderingHintAntiAlias);
		return grp;
	}

	void_t __delete_graphics_instance(gdiplus_graphics_t** ppointer) {
		if (*ppointer) {
			delete(*ppointer);
			*ppointer = nullptr;
		}
	}

	LRESULT CALLBACK window_procedure(hwnd_t hWnd, uint_t uiMsg, wparam_t wParam, lparam_t lParam) {
		gui * guiptr = (gui*) external::get_window_long_ptr(hWnd, GWLP_USERDATA);
				
		if (guiptr) {
			return guiptr->message_procedure(hWnd, uiMsg, wParam, lParam);
		}

		else {
			return external::def_win_procedure(hWnd, uiMsg, wParam, lParam);
		}
	}


	void_t make_default_create_info(hinstance_t hinstance, create_gui_infow_t* pwinfo) {
		create_gui_infow_t& winfo = *pwinfo;
		utilities::zero_obj(winfo);

		wndclassexw_t& wcex = winfo.wc;
		wcex = {};
		wcex.cbSize = sizeof(wndclassexw_t);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = NULL;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hinstance;
		wcex.hIcon = NULL;
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(NULL);
		wcex.lpszMenuName = NULL;
		wcex.lpszClassName = L"ikarusgltest";
		wcex.hIconSm = NULL;

		winfo.ex_style = 0;
		winfo.style = WS_POPUP | WS_VISIBLE;
		winfo.instance = hinstance;
		winfo.menu = NULL;
		winfo.parent = NULL;
		winfo.lp_param = 0;
		winfo.window_name = L"sdt gui test";
		winfo.height = 100;
		winfo.width = 100;
		winfo.x = 100;
		winfo.y = 100;
	}


	void_t make_default_create_info(hinstance_t hinstance, create_gui_infoa_t* painfo) {
		create_gui_infoa_t& ainfo = *painfo;
		utilities::zero_obj(ainfo);

		wndclassexa_t& wcex = ainfo.wc;
		wcex = {};
		wcex.cbSize = sizeof(wndclassexa_t);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = NULL;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hinstance;
		wcex.hIcon = NULL;
		wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = NULL;
		wcex.lpszClassName = "ikarusgltest";
		wcex.hIconSm = NULL;

		ainfo.ex_style = 0;
		ainfo.style = WS_POPUP;
		ainfo.instance = hinstance;
		ainfo.menu = NULL;
		ainfo.parent = NULL;
		ainfo.lp_param = 0;
		ainfo.window_name = "sdt gui test";
		ainfo.height = 100;
		ainfo.width = 100;
		ainfo.x = 100;
		ainfo.y = 100;
	}



	gui::gui() {
		this->_rendering_thread = nullptr;
		this->_updating_thread = nullptr;
		this->_is_mouse_outside_window = false;
		this->_is_always_render = false;
		this->_is_minimized = false;
		this->_is_over_button = false;
		this->_is_quit = false;
		this->_is_to_render = true;
		this->_is_start_mouse_tracking = false;
		this->_is_use_colorkey = false;
		this->_is_trace_fps = {};
		this->_hwnd = external::invalid_handle_cast<hwnd_t>();
		this->_hdc = external::invalid_handle_cast<hdc_t>();
		this->clear();
		this->_is_shut = false;

		//initializing Gdiplus
		gdiplus_startup_input_t startup_info;
		external::gdiplus_startup(&this->_gdiplus_token, &startup_info, NULL);

	}


	gui::~gui() {
		clear();

		//* destroying the window
		if(!external::is_invalid_handle_value(this->_hwnd))
			external::destroy_window(this->_hwnd);

		//* clean up gdiplus
		external::gdiplus_shutdown(this->_gdiplus_token);
	}

	void_t gui::clear() {
		//* prevent non-shutted clear request
		this->__shut();

		//* check about mt (rendering)
		if (this->_rendering_thread) {
			if (this->_rendering_thread->joinable())
				this->_rendering_thread->join();
			delete(this->_rendering_thread);
		}

		//* check about mt (updating)
		if (this->_updating_thread) {
			if (this->_updating_thread->joinable())
				this->_updating_thread->join();
			delete(this->_updating_thread);
		}

		//* resetting threads handles
		this->_rendering_thread = nullptr;
		this->_updating_thread  = nullptr;
		
		//* check about the release of the dc
		if(this->_hwnd  != external::invalid_handle_cast<hwnd_t>() && this->_hdc != external::invalid_handle_cast<hdc_t>() ){
			external::release_dc(this->_hwnd , this->_hdc);
			external::destroy_window(this->_hwnd);
		}

		//* clearing the containers
		this->_layer_map.clear();
		this->_window_name_map.clear();
		this->_message_event_map_window.clear();
		this->_message_event_map_general.clear();

		//* clearing the windows peeked and focused
		this->_peeked_window_left  = nullptr;
		this->_peeked_window_right = nullptr;
		this->_focused_window      = nullptr;

		//* clearing the handles of mc api
		this->_hwnd      = external::invalid_handle_cast<hwnd_t>();
		this->_hdc       = external::invalid_handle_cast<hdc_t>();
		this->_hinstance = external::invalid_handle_cast<hinstance_t>();

		//* loading the default cursor
		this->_hcursor = external::load_cursor( external::null_cast<hinstance_t>(), IDC_ARROW);

		//* clearing the float_t values
		this->_last_refresh = 0.0;
		this->_last_update  = 0.0;
		this->_alpha        = 1.0f;

		//* clearing the uint values
		this->_fps         = DEFAULT_FPS;
		this->_ups         = DEFAULT_UPS;
		this->_alpha_color = DEFAULT_ALPHA_COLOR;

		//* resetting the mouse position
		this->_mouse_pos = {};

		//* resetting the size
		utilities::zero_obj(this->_size);
		utilities::zero_obj(this->_min_size);
		utilities::zero_obj(this->_max_size);
	}


	bool_t gui::on_render() {
		//* checking the render call
		//* directly on the gui object
		if (this->_is_to_render) {
			return true;
		}

		//* checking the render call
		//* on the layers's children
		for (auto& layer : this->_layer_map) {
			if (layer.second.on_render()) {
				return true;
			}
		}


		//* default return value
		return false;
	}


	void_t gui::render(hdc_t& hdc) {
#ifdef __TEST_PERFORMANCE
		//* initializing local var for test performance
		//* start time and deque (static) to stock the media
		auto   start_time = utilities::get_time_double();
		static std::deque<double_t> time_deq;
		static std::deque<double_t> render_deq;
		static std::deque<double_t> swapp_deq;
#endif

#ifdef __TEST_PERFORMANCE
		double_t check_time_end = 0;
		double_t check_time_start = 0;
#endif

		//* initialization of context etc..
		hdc_t hdc_mem;
		hbitmap_t hbitmap, hbitmap_old;
		gdiplus_graphics_t* grp = nullptr;

		//* initializing the buffer to perform the rendering without flickering
		hbitmap_old = this->__initialize_rendering_buffer(hdc, hdc_mem, hbitmap);
		grp         = __new_graphics_instance(hdc_mem);

		if (!grp) {
			__ikgl_error("cannot get a new instance of gdiplus_graphics_t (dynamic allocation error) \n");
		}

		else {

	#ifdef __TEST_PERFORMANCE
			check_time_start = utilities::get_time_double();
	#endif
			//* redering into the buffer
			for (auto& it : this->_layer_map) {
				auto& layer = it.second;
				layer.render(hdc_mem, grp);
			}

	#ifdef __TEST_PERFORMANCE
			check_time_end = utilities::get_time_double();
	#endif
			//* deleting graphics instance and swapping the bitmap
			//* into the real used device context
			__delete_graphics_instance(&grp);
			this->__swap_rendering_buffer(hdc, hdc_mem, hbitmap);

			//* set render as made
			this->__set_render_made();

			//* cleaning the buffer
			this->__clear_rendering_buffer(hdc_mem, hbitmap, hbitmap_old);
		}

#ifdef __TEST_PERFORMANCE
		auto __deq_media = [](std::deque<double_t>& dq, double_t n) {
			dq.push_back(n);
			if (dq.size() == 501) {
				dq.pop_front();
			}
			auto media = 0.0;
			for (auto t : dq) {
				media += t;
			}
			media /= dq.size();
			return media;
		};


		auto end_time			= utilities::get_time_double();
		const auto total_time	= end_time - start_time;
		const auto render_time	= (check_time_end - check_time_start);
		const auto swapp_time	= total_time - render_time;
		const auto r			= render_time / total_time;
		const auto s			= swapp_time / total_time;

		ikgl::utilities::debug("\n\ntotal time : %f ms", total_time*1000);
		ikgl::utilities::debug("rendering time : %f ms ", render_time*1000);
		ikgl::utilities::debug("swapping time : %f ms", swapp_time*1000);
		if (this->is_trace_fps_enabled()) {
			auto data_fps = this->get_real_fps();
			ikgl::utilities::debug("real fps : %u ", data_fps);
		}
		ikgl::utilities::debug("max fps per media : %u ", uint_t(1.0/total_time));
		ikgl::utilities::debug("percentage : r %0.2f , s %0.2f ", r*100, s*100 );
		

		const auto media_total  = __deq_media(time_deq, total_time);
		const auto media_render = __deq_media(render_deq, render_time);
		const auto media_swap   = __deq_media(swapp_deq, swapp_time);

		
		ikgl::utilities::debug("\ntot media : %f ms %d", media_total *1000, time_deq.size());
		ikgl::utilities::debug("rnd media : %f ms ", media_render *1000);
		ikgl::utilities::debug("swp media : %f ms\n\n", media_swap *1000);
#endif
	}



	bool_t gui::is_to_render() {
		return this->_is_to_render;
	}


	void_t gui::call_render() {
		this->_is_to_render = true;
	}


	void_t gui::__set_render_made() {
		this->_is_to_render = false;
	}


	hbitmap_t gui::__initialize_rendering_buffer(hdc_t hdc_dest, hdc_t& hdc_mem, hbitmap_t& hbitmap) {
		const auto w = this->_size.width;
		const auto h = this->_size.height;

		hdc_mem = external::create_compatible_dc(hdc_dest);
		hbitmap = external::create_compatible_bitmap(hdc_dest, w, h);

		return external::select_object(hdc_mem, hbitmap);
	}



	void_t gui::__swap_rendering_buffer(hdc_t hdc_dest, hdc_t hdc_mem, hbitmap_t hBitmap) {
		
		if (this->is_layer_mode_enabled()) {
			if (!external::update_layered_window(this->_hwnd, hdc_dest, hdc_mem, this->_size.width, this->_size.height, this->_alpha_color, this->_is_use_colorkey, this->_alpha))
				__ikgl_error("__swap_rendering_buffer :: cannot use update layered window! %d error", external::get_last_error());
		}

		else {
			external::bit_blt(hdc_dest, hdc_mem, this->_size.width,  this->_size.height);
		}
	}



	void_t gui::__clear_rendering_buffer(hdc_t hdcMem, hbitmap_t hBitmap, hbitmap_t hBitmapOld) {
		external::select_object(hdcMem, hBitmapOld);
		external::delete_object(hBitmap);
		external::delete_dc(hdcMem);
	}



	bool_t gui::__mt_render_ex() {
		if (!this->__is_window_initialized())
			return false;

		if (!this->_rendering_thread)
			return false;

		if(this->_is_minimized)
			return true;

		{
			__critical_scope();

			if (this->is_layer_mode_enabled()) {
				hdc_t hdc = external::get_dc(external::null_cast<hwnd_t>());
				this->render(hdc);
				external::release_dc(external::null_cast<hwnd_t>(), hdc);
			}

			else {
				this->render(this->_hdc);
			}
		}

		return true;
	}

	bool_t gui::__mt_update_ex() {
		const auto time = utilities::get_time_double();
		{
			__critical_scope();
			for (auto& iter : this->_layer_map){
				auto& layer_ = iter.second;
				layer_.update(time);
			}
		}
		return true;
	}


#ifdef __WIN32__
	hresult_t gui::message_procedure(hwnd_t hWnd, uint_t message, wparam_t wParam, lparam_t lParam)
#else
	lresult_t gui::message_procedure(hwnd_t hwnd, uint_t message, wparam_t wparam, lparam_t lparam)
#endif
	{
		static thread_local bool s_is_proceduring = false;
		
		//* store the previous value from the static to a local variable
		//* to know at the end of the methods if this is 
		bool is_proceduring = s_is_proceduring;
		s_is_proceduring = true;

		auto check_message = [this](hwnd_t hwnd, uint_t message, wparam_t wparam, lparam_t lparam) {
			auto it = this->_message_event_map_general.find(message);
			if (it != this->_message_event_map_general.end()) {
				for (auto& msg : it->second)
					if (msg(hwnd, message, wparam, lparam) == message_return_t::PREVENT_DEFAULT)
						return true;
			}

			if (hwnd == this->_hwnd) {
				auto it = this->_message_event_map_window.find(message);
				if (it != this->_message_event_map_window.end())
					for (auto& msg : it->second)
 						if (msg(hwnd, message, wparam, lparam) == message_return_t::PREVENT_DEFAULT)
 							return true;
			}
			return false;
		};



		if (is_proceduring) {
			if (check_message(hwnd, message, wparam, lparam)) {
				return external::def_win_procedure(hwnd, message, wparam, lparam);
			}
		} else {
			__critical_scope();
			if (check_message(hwnd, message, wparam, lparam)) {
				return external::def_win_procedure(hwnd, message, wparam, lparam);
			}
		}



		
		switch (message) {



		case WM_PAINT: {
			this->call_render();
		}break;

		case WM_MOUSEMOVE: {
			point_t p;

			 

			if (external::get_cursor_pos(&p) && external::screen_to_client(hwnd, &p)) {
				widget_pos_t newpos = { p.x, p.y };

				if (is_proceduring) {
					this->__on_mousemove(newpos);
				}else{
					 __critical_scope();
					 this->__on_mousemove(newpos);
				}
			}
		} break;

		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
		case WM_LBUTTONUP:
		case WM_RBUTTONUP: {
			
			point_t p;
			if (external::get_cursor_pos(&p) && external::screen_to_client(hwnd, &p)) {
				
				if (is_proceduring) {
					this->__on_mousebutton(message, p);
				}else{
					__critical_scope();
					this->__on_mousebutton(message, p);
				}
			}
		}break;

		case WM_MOUSEWHEEL: {
			if (is_proceduring) {
				this->__on_mousewheel(wparam, lparam);
			}else {
				__critical_scope();
				this->__on_mousewheel(wparam, lparam);
			}
		}break;

		case WM_MOUSELEAVE: {
			
			if (is_proceduring) {
				this->__on_mouseleave();
			} else {
				__critical_scope();
				this->__on_mouseleave();
			}
		}break;


		case WM_CHAR:
		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
			if (is_proceduring) {
				this->__on_key_down({ message, wparam, lparam });
			}else {
				__critical_scope();
				this->__on_key_down({ message, wparam, lparam });
			}
			
			break;

		case WM_KEYUP:
		case WM_SYSKEYUP:
			if (is_proceduring) {
				this->__on_key_up({ message, wparam, lparam });
			}else {
				__critical_scope();
				this->__on_key_up({ message, wparam, lparam });
			}
			
			break;

		case WM_SYSCOMMAND: {
				switch (wparam) {
					//* atomics doesnt request the critical_scope
					case SC_MINIMIZE: {
 					this->__minimize_window();
					}
 
					case SC_RESTORE:{
 					this->__restore_window();
					}
				}
				break;
			}
		
		case WM_GETMINMAXINFO:{
			lpminmaxinfo_t lp = (lpminmaxinfo_t)lparam;
			
			if (this->_min_size.width != 0 && this->_min_size.height != 0) {
				lp->ptMinTrackSize.x = this->_min_size.width;
				lp->ptMinTrackSize.y = this->_min_size.height;
			}

			if (this->_max_size.width != 0 && this->_max_size.height != 0) {
				lp->ptMaxTrackSize.x = this->_max_size.width;
				lp->ptMaxTrackSize.y = this->_max_size.height;
			}
		}break;

		case WM_SIZE: {
			auto size_process = [this, &lparam, &wparam]() {
				//* resizing window
				auto mc_rect = this->get_mc_rect();
				const auto w = mc_rect.right - mc_rect.left;
				const auto h = mc_rect.bottom - mc_rect.top;

				
				this->_size = {w, h};


				switch (wparam) {
					case SIZE_MAXIMIZED:
 						this->__restore_window();
 						break;

					case SIZE_MINIMIZED:
 						this->__minimize_window();
 						break;

				}

				for (auto& it : this->_layer_map) {
 					auto& layer = it.second;
 					layer.on_resize_gui();
				}

				this->call_render();
			};

			if(!is_proceduring)	{
				__critical_scope();
				size_process();
			} else {
				size_process();
			}
		}break;


		//prevent default
		case WM_SETCURSOR: {
				
				auto set_cursor_process = [this]() {
					 if (this->_is_over_button) {
 						external::set_cursor(this->_hcursor);
 						return TRUE;
					 }

					return FALSE;
				};

				if (is_proceduring) {
					 if (set_cursor_process())
 						return TRUE;

					 else
 						break;
				} else {
					 __critical_scope();
					 if(set_cursor_process())
 						return TRUE;

					 else
 						break;
				}
			} break;
			

		case WM_ERASEBKGND:
			return 1;
		
		case WM_CLOSE:
			if(this->_is_quit == false)
				external::post_quit_message(0);
			break;

		case WM_DESTROY:
			if (this->_is_quit == false)
				external::post_quit_message(0);
			break;
		
		case WM_QUIT:
			if (is_proceduring) {
				this->_is_shut = true;
			} else {
				__critical_scope();
				this->_is_shut = true;
			}break;
		

		default:
			//__ikgl_error("unrecognized message %s \n",utilities::get_message_name(message));
			break;

		}

		
		if (!is_proceduring)
			s_is_proceduring = false;

		return external::def_win_procedure(hwnd, message, wparam, lparam);
	}



	void_t gui::__on_mousewheel(wparam_t wParam, lparam_t lParam) {
		//getting info from params
		word_t fwkey   = external::get_key_state_wparam(wParam);
		short_t zdelta = external::get_wheel_delta_wparam(wParam);

		//calculating times to scroll
		int_t times = zdelta / WHEEL_DELTA;
		ulong_t flag = 0;

		if (__ikgl_isset_bit(fwkey , MK_CONTROL)) {
			times *= 3;
			__ikgl_set_bit(flag , scroll_down_button_t::SCROLL_CTRL_DOWN);
		}

		if (__ikgl_isset_bit(fwkey , MK_SHIFT)) {
			times *= 6;
			__ikgl_set_bit(flag , scroll_down_button_t::SCROLL_SHIFT_DOWN);
		}

		if (__ikgl_isset_bit(fwkey , MK_ALT)) {
			__ikgl_set_bit(flag , scroll_down_button_t::SCROLL_ALT_DOWN);
		}

		//lambda to scroll on the window
		auto scroll_meth = [](int_t timesIndex, ulong_t flag, window* win) {
			if (win == nullptr)
				return;

			mousewheel_data_t data = {};

			data.type  = timesIndex > 0 ? scroll_type_t::SCROLL_TYPE_UP : scroll_type_t::SCROLL_TYPE_DOWN;
			data.times = __ikgl_max(timesIndex, -timesIndex);
			data.flag  = flag;

			win->on_mousewheel(data);
		};


		//checking if is peeked any window
		if (this->_peeked_window_left) {
			scroll_meth(times, flag, this->_peeked_window_left);
		}

		else if (this->_peeked_window_right) {
			scroll_meth(times, flag, this->_peeked_window_right);
		}

		//no picked window so check the over in 
		else {
			auto mouse_pos = this->get_mouse_position();
			window* win = this->get_picked(mouse_pos);

			if (win)
				scroll_meth(times, flag, win);
		}
	}

	void_t gui::__on_key_down(const key_message_info_t& message_info) {
		if (this->_focused_window) {
			this->_focused_window->on_key_down(message_info);
		}
	}



	void_t gui::__on_key_up(const key_message_info_t& message_info) {
		if (this->_focused_window) {
			this->_focused_window->on_key_up(message_info);
		}
	}




	void_t gui::__on_mousebutton(uint_t message, point_t p)
	{
		widget_pos_t pos = { p.x, p.y };
		window* win = this->get_picked(pos);


		if (win) {
			switch (message) {

			case WM_LBUTTONDOWN:
				this->_peeked_window_left = win;

				//* focused window setting
				if (this->_focused_window != win) {
					if (this->_focused_window)
						this->_focused_window->kill_focus();

					this->_focused_window = win;
					win->set_focus();
				}
				win->on_mouseleft_down();
				break;

			case WM_RBUTTONDOWN:
				win->on_mouseright_down();
				this->_peeked_window_right = win;
				break;

			case WM_LBUTTONUP: {
				bool_t is_mouse_out = false;

				if (this->_peeked_window_left) {
 					this->_peeked_window_left->on_mouseleft_up();
 					if (this->_peeked_window_left && !this->_peeked_window_left->is_in(pos)) {
 						this->_peeked_window_left->on_mouseover_out();
 						is_mouse_out = true;
 					}
				}

				this->_peeked_window_left = nullptr;

				if (is_mouse_out) {
 					window* picked = this->get_picked(pos);
 					if (picked) {
 						picked->on_mouseover_in();
 					}
				}
			}break;


			case WM_RBUTTONUP: {
				bool_t is_mouse_out = false;
				if (this->_peeked_window_right) {
					this->_peeked_window_right->on_mouseright_up();
					if (!this->_peeked_window_right->is_in(pos)) {
						this->_peeked_window_right->on_mouseover_out();
						is_mouse_out = true;
					}
				}

				this->_peeked_window_right = nullptr;

				if (is_mouse_out) {
					window* picked = this->get_picked(pos);
					if (picked) {
						picked->on_mouseover_in();
					}
				}
				}break;
			}
		}
	}


	void_t gui::__on_mousemove(const widget_pos_t& newpos) {

		widget_pos_t oldpos = this->_mouse_pos;
		this->_mouse_pos = newpos;

		if (this->_peeked_window_left)
			this->_peeked_window_left->on_mousemove();

		else if (this->_peeked_window_right)
			this->_peeked_window_right->on_mousemove();

		else {
			for (auto & it : _layer_map) {
				auto& layer_ = it.second;
				layer_.on_mousemove(oldpos, newpos);

				if (this->_is_mouse_outside_window) {
					static window* pick = nullptr;
					if ((pick = layer_.get_picked(this->_mouse_pos))) {
						pick->on_mouseover_in();
					}
				}
			}
		}


		
		if (this->_is_mouse_outside_window || !this->_is_start_mouse_tracking) {

			trackmousevent_t tme = {};
			tme.cbSize    = sizeof(trackmousevent_t);
			tme.dwFlags   = TME_LEAVE;
			tme.hwndTrack = _hwnd;
			this->_is_start_mouse_tracking = external::track_mouse_event(&tme);
		}

		this->_is_mouse_outside_window = false;
	}


	void_t gui::__on_mouseleave() {



		if (this->_peeked_window_left) {
			this->_peeked_window_left->on_mouseleft_up();
			this->_peeked_window_left = nullptr;
		}
		else if (this->_peeked_window_right) {
			this->_peeked_window_right->on_mouseright_up();
			this->_peeked_window_right = nullptr;
		}

		window* picked = nullptr;

		for (auto & it : this->_layer_map) {
			auto& ly = it.second;

			if ((picked = ly.get_picked(this->_mouse_pos)))
				picked->on_mouseover_out();
		}

		this->_is_mouse_outside_window = true;
	}


	void_t gui::set_button_cursor() {
		this->_hcursor = external::load_cursor(NULL, IDC_HAND);
		this->_is_over_button = true;
		external::post_messagew(this->_hwnd, WM_SETCURSOR, 0,0);
	}

	void_t gui::set_normal_cursor() {
		this->_hcursor = external::load_cursor(NULL, IDC_ARROW);
		this->_is_over_button = false;
		external::post_messagew(this->_hwnd, WM_SETCURSOR, 0, 0);
	}

	void_t gui::set_cursor(hcursor_t hCursor) {
		this->_hcursor = hCursor;
		external::post_messagew(this->_hwnd, WM_SETCURSOR, 0,0);
	}

	void_t gui::set_cursor_lock(hcursor_t hCursor) {
		__critical_scope();
		this->_hcursor = hCursor;
		external::post_messagew(this->_hwnd, WM_SETCURSOR, 0,0);
	}

	hcursor_t gui::get_cursor() {
		return this->_hcursor;
	}

	hcursor_t gui::get_cursor_lock() {
		__critical_scope();
		return this->get_cursor();
	}

	void_t gui::__shut(){
		__critical_scope();
		this->_is_shut = true;
	}

	void_t gui::__mt_render(){
		auto is_shut = [this] ()->bool_t{
			__critical_scope();
			return this->_is_shut;
		};

		auto is_render = [this]()->bool_t {
			__critical_scope();
			return this->on_render() || this->is_to_render() || this->is_always_render();
		};


		const double_t time_per_frame	= (1.0/(double_t)get_fps_lock());
		double_t next = utilities::get_time_double() + time_per_frame;
		
		this->_fps_data = {};

		while (!is_shut()){
			
			const double_t now = utilities::get_time_double();

			if (now >= next){
				if (is_render()){
					this->__mt_render_ex();
					__set_last_rendering_time(utilities::get_time_double());

					if (this->_is_trace_fps) {
						__critical_scope();
						this->__update_fps_counter();
					}
 				   
				}
				next += time_per_frame;
			}
		

			const double_t sleep_time	= next - utilities::get_time_double();
			std::this_thread::sleep_for(std::chrono::microseconds( 
				(longlong_t)( (sleep_time - 0.001)* 10000.0)
			));
		}
		
		__ikgl_debug("exit from render thread.\n");
	}

	void_t gui::__mt_update(){
		auto is_shut = [this] ()->bool_t{
			__critical_scope();
			return this->_is_shut;
		};

		const double_t time_per_frame	= (1.0/(double_t)this->get_ups_lock());
		double_t next = utilities::get_time_double() + time_per_frame;

		while (!is_shut()){

			const double_t now = utilities::get_time_double();

			if (now >= next){
				this->__mt_update_ex();
				this->__set_last_update_time(utilities::get_time_double());
				next += time_per_frame;
			}


			const double_t sleep_time	= next - utilities::get_time_double();
			std::this_thread::sleep_for(std::chrono::microseconds( 
				(longlong_t)((sleep_time - 0.001) * 10000.0)
			));
		}


		__ikgl_debug("exit from update thread.\n");

	}

	void_t gui::__update_fps_counter() {
		const double_t now = utilities::get_time_double();

		if (this->_fps_data.last_time < now - 1.f) {
			this->_fps_data.start_time = now;
			this->_fps_data.fps = 0;
		}

		this->_fps_data.fps++;
		this->_fps_data.last_time = now;
	}

	void_t gui::__minimize_window() {
		_is_minimized = true;
	}

	void_t gui::__restore_window() {
		this->_is_minimized = false;
		this->call_render();
	}

	void_t gui::__register_window_name(window* win, const wstring_t& new_name){
		auto old_name = win->get_window_name();
		if (old_name.empty()==false) {
			__unregister_window_name(old_name);
		}

		this->_window_name_map.emplace(new_name, win);
	}

	void_t gui::__unregister_window_name(const wstring_t& name){
		auto it = this->_window_name_map.find(name);

		if (it != this->_window_name_map.end()) {
			this->_window_name_map.erase(it);
		}
	}

	bool_t gui::__is_window_initialized(){
		return this->_hwnd != external::invalid_handle_cast<hwnd_t>();
	}

	void_t gui::register_message_event_general(uint_t type, message_event_t event){
		auto it = this->_message_event_map_general.find(type);

		if(it == this->_message_event_map_general.end())
			it = this->_message_event_map_general.insert({type, std::vector<message_event_t>()}).first;

		this->_message_event_map_general[type].push_back(event);
	}

	void_t gui::register_message_event_window(uint_t type, message_event_t event){
		auto it = this->_message_event_map_window.find(type);

		if(it == this->_message_event_map_window.end())
			it = this->_message_event_map_window.insert({type, std::vector<message_event_t>()}).first;

		this->_message_event_map_window[type].push_back(event);
	}

	void_t gui::register_message_event_window_table(message_event_info_t* event, uint_t count){
		for(uint_t i=0; i < count; i++){
			message_event_info_t& table = *(event++);
			this->register_message_event_window(table.message, table.event);
		}
	}

	void_t gui::register_message_event_general_table(message_event_info_t* event, uint_t count){
		for(uint_t i=0; i < count; i++)	{
			message_event_info_t& table = *(event++);
			this->register_message_event_general(table.message, table.event);
		}
	}

	layer* gui::add_layer(string_t layername){
		auto it = this->_layer_map.find(layername);
		if(it != this->_layer_map.end())
			return nullptr;

		else
			return &(this->_layer_map.insert({layername,layer(this)}).first->second);
	}


	layer* gui::get_layer(string_t layername){
		auto it = this->_layer_map.find(layername);
		if(it == this->_layer_map.end())
			return nullptr;

		return &(it->second);
	}


	void_t gui::quit() {
		this->_is_quit = true;
	}


	bool_t gui::remove_layer(string_t layername){
		auto it = this->_layer_map.find(layername);
		if(it == this->_layer_map.end())
			return false;

		this->_layer_map.erase(it);
		return true;
	}



	window* gui::get_picked(coord_t x , coord_t y){
		return this->get_picked({ x, y });
	}


	window* gui::get_picked(widget_pos_t pos){

		if(this->_peeked_window_left)
			return this->_peeked_window_left;

		if(this->_peeked_window_right)
			return this->_peeked_window_right;


		window* res=nullptr;
		
		for (auto& iter : this->_layer_map) {
			if ((res = iter.second.get_picked(pos)) != nullptr)
				break;
		}

		return res;
	}

	window * gui::get_picked_lock(coord_t x, coord_t y){
		__critical_scope();
		return this->get_picked(x,y);
	}

	window * gui::get_picked_lock(widget_pos_t pos){
		__critical_scope();
		return this->get_picked(pos);
	}

	window * gui::get_focused() {
		return this->_focused_window;
	}

	window * gui::get_focused_lock() {
		__critical_scope();
		return this->get_focused();;
	}

	window* gui::get_window_by_name(const wstring_t& name){
		auto it = this->_window_name_map.find(name);
		return it != this->_window_name_map.end() ? it->second : nullptr;
	}

	window* gui::get_window_by_name(const string_t& name)
	{
		return this->get_window_by_name(utilities::to_wstring(name));
	}

	
	bool_t gui::create(create_gui_infow_t* winfo) {
		winfo->wc.lpfnWndProc = window_procedure;
		external::register_class(&winfo->wc);
		this->_hinstance = winfo->instance;
		
		this->_hwnd = external::create_window_ex(
			winfo->ex_style,
			winfo->wc.lpszClassName, winfo->window_name,
			winfo->style,
			winfo->x, winfo->y,
			winfo->width, winfo->height,
			winfo->parent, winfo->menu,
			this->_hinstance, winfo->lp_param
		);

		if (!_hwnd){
			__ikgl_error("cannot create window , error code %d ", external::get_last_error());
			return false;
		}

		
		external::show_window(this->_hwnd, SW_SHOW);
		external::update_window(this->_hwnd);
		external::set_window_long_ptr(this->_hwnd, GWLP_USERDATA, (long_ptr_t)this);

		set_normal_cursor();

		this->_hdc = external::get_dc(this->_hwnd);
		this->_size = { winfo->width, winfo->height };

		return true;
	}

	/*
	bool_t gui::create(create_gui_infoa_t* ainfo){
		ainfo->wc.lpfnWndProc = window_procedure;
		RegisterClassExA(&ainfo->wc);
		this->_hinstance = ainfo->instance;

		this->_hwnd = external::create_window_ex(
			ainfo->ex_style,
			ainfo->wc.lpszClassName, ainfo->window_name,
			ainfo->style,
			ainfo->x, ainfo->y,
			ainfo->width, ainfo->height,
			ainfo->parent, ainfo->menu,
			this->_hinstance, ainfo->lp_param
		);

		if (!this->_hwnd)
			return false;

		external::show_window(this->_hwnd, SW_SHOW);
		external::update_window(this->_hwnd);
		external::set_window_long_ptr(this->_hwnd, GWLP_USERDATA, (long_ptr_t)this);

		

		set_normal_cursor();
		this->_hdc = external::get_dc(this->_hwnd);
		this->_size = { ainfo->width, ainfo->height };

		return true;
	}*/

	bool_t gui::maximize(){
		const auto desktop_size  = ikgl::utilities::get_desktop_size();
		const auto app_task_size = ikgl::utilities::get_app_bar_size();
		const auto app_task_rect = ikgl::utilities::get_app_bar_rect();
		auto size = desktop_size;

		if (app_task_size.width > app_task_size.height) {
			if (app_task_rect.top > desktop_size.width/2) {
				size.height -= app_task_size.height;
				if (external::move_window(this->_hwnd, 0, 0, size.width, size.height)) {
					this->_size = size;
					return true;
				} else {
					return false;
				}
			} else {
				size.height -= app_task_size.height;
				if( external::move_window(this->_hwnd, 0,app_task_size.height, size.width, size.height)) {
					this->_size = size;
					return true;
				}else {
					return false;
				}
			}
		} else {
			if (app_task_rect.left > desktop_size.width / 2) {
				size.width -= app_task_size.width;
				if( external::move_window(this->_hwnd, 0,0, size.width, size.height)) {
					this->_size = size;
					return true;
				} else {
					return false;
				}
			} else {
				size.width -= app_task_size.width;
				if( external::move_window(this->_hwnd, app_task_size.width, 0, size.width, size.height)) {
					this->_size = size;
					return true;
				}else {
					return false;
				}
			}
		}
	}

	bool_t gui::minimize() {
		return ikgl::external::show_window(this->_hwnd, SW_MINIMIZE);
	}



	bool_t gui::resize_window(const widget_size_t& size){
		auto rect = this->get_mc_rect();
		if (external::move_window(this->_hwnd, rect.left, rect.top, size.width, size.height)) {
			this->_size = size;
			return true;
		} else {
			return false;
		}
	}

	bool_t gui::move_window(const widget_pos_t& pos){
		return external::move_window(this->_hwnd, pos.x, pos.y, this->_size.width, this->_size.height);
	}

	bool_t gui::start_rendering_thread(){
		if(!this->__is_window_initialized())
			return false;

		if(this->_rendering_thread != nullptr)
			return false;

		__ikgl_debug("starting rendering thread\n");
		this->_rendering_thread = new thread_t(&gui::__mt_render , this);
		return true;
	}

	bool_t gui::start_updating_thread(){
		if(!this->__is_window_initialized())
			return false;

		if(this->_updating_thread != nullptr)
			return false;

		__ikgl_debug("starting updating thread\n");
		this->_updating_thread = new thread_t(&gui::__mt_update , this);
		return true;
	}

	double_t gui::get_last_rendering_time(){
		return this->_last_refresh;
	}

	double_t gui::get_last_rendering_time_lock(){
		__critical_scope();
		return this->get_last_rendering_time();
	}

	double_t gui::get_last_updating_time(){
		return this->_last_update;
	}

	double_t gui::get_last_updating_time_lock(){
		__critical_scope();
		return this->get_last_updating_time();
	}

	void_t gui::__set_last_rendering_time(double_t dTime){
		__critical_scope();
		this->_last_refresh = dTime;
	}

	void_t gui::__set_last_update_time(double_t dTime){
		__critical_scope();
		this->_last_update = dTime;
	}

	void_t gui::set_fps(uint_t fps){
		this->_fps = fps;
	}

	uint_t gui::get_fps() const{
		return this->_fps;
	}

	void_t gui::set_fps_lock(uint_t fps){
		__critical_scope();
		this->set_fps(fps);
	}

	uint_t gui::get_fps_lock(){
		__critical_scope();
		return this->get_fps();
	}


	void_t gui::set_ups(uint_t ups){
		this->_ups = ups;
	}


	uint_t gui::get_ups() const{
		return this->_ups;
	}

	void_t gui::set_ups_lock(uint_t ups){
		__critical_scope();
		this->set_ups(ups);
	}


	uint_t gui::get_ups_lock(){
		__critical_scope();
		return this->get_ups();
	}



	uint_t gui::get_real_fps() {
		if (this->_fps_data.fps == 0)
			return 0;
		if (this->_fps_data.start_time == this->_fps_data.last_time)
			return 1;
		 
		const double_t time = (this->_fps_data.last_time - this->_fps_data.start_time);
		return uint_t(double_t(this->_fps_data.fps)/time);
	}



	uint_t gui::get_real_fps_lock() {
		__critical_scope();
		return this->get_real_fps();
	}



	void_t gui::enable_trace_fps() {
		this->_is_trace_fps = true;
	}



	void_t gui::disable_trace_fps() {
		this->_is_trace_fps = false;
	}


	bool_t gui::is_trace_fps_enabled() {
		return this->_is_trace_fps;
	}


	bool_t gui::is_always_render() {
		return this->_is_always_render;
	}



	void_t gui::enable_always_render() {
		this->_is_always_render = true;
	}



	void_t gui::disable_always_render() {
		this->_is_always_render = false;
	}



	bool_t gui::message_loop(const dword_t ms){
		if(!this->__is_window_initialized())
			return false;
		
		__ikgl_debug("starting message_loop\n");
		msg_t msg;
		utilities::zero_obj(msg);

		while (true) {
			while (external::peek_message(&msg, external::null_cast<hwnd_t>(), PM_REMOVE)) {
				external::translate_message(&msg);
				external::dispatch_message(&msg);
				if (external::is_quit_message(msg))
					break;
			}

			if (external::is_quit_message(msg))
				break;

			if (this->_is_quit){
				external::post_quit_message(0);
			}else external::sleep(ms);
		} return true;
	}

	bool_t gui::message_loop( loop_message_event_t event){
		if(!this->__is_window_initialized())
			return false;

		__ikgl_debug("starting message_loop\n");

		msg_t msg;
		utilities::zero_obj(msg);

		const auto timestamp = utilities::get_time_double();
		
		while (true){
			while (external::peek_message(&msg, external::null_cast<hwnd_t>(), PM_REMOVE)){
				external::translate_message(&msg); 
				external::dispatch_message(&msg);

				if (external::is_quit_message(msg))
					break;
			}

			if (external::is_quit_message(msg))
				break;

			if (this->_is_quit) {
				external::post_quit_message(0);
			} else {
				double_t time_sleep = 0.0;{
					__critical_scope();
					time_sleep = event(utilities::get_time_double() - timestamp);
				}external::sleep(dword_t(time_sleep * 1000.0));
			}
			
		}

		return true;
	}

	hwnd_t gui::get_hwnd(){
		return this->_hwnd;
	}

	hinstance_t gui::get_hinstance(){
		return this->_hinstance;
	}

	hwnd_t gui::get_hwnd_lock(){
		__critical_scope();
		return this->get_hwnd();
	}

	hinstance_t gui::get_hinstance_lock(){
		__critical_scope();
		return this->get_hinstance();
	}

	const widget_pos_t & gui::get_mouse_position(){
		return this->_mouse_pos;
	}

	const widget_pos_t & gui::get_mouse_position_lock(){
		__critical_scope();
		return this->_mouse_pos;
	}

	mc_rect_t gui::get_mc_rect() {
		mc_rect_t rect = {};

		if (__is_window_initialized()) {
			external::get_window_rect(this->_hwnd, &rect);
		}

		return rect;
	}

	widget_rect_t gui::get_rect() {
		mc_rect_t rect = get_mc_rect();
		return { rect.left , rect.top, rect.right, rect.bottom};
	}

	widget_size_t gui::get_size(){
		return this->_size;
	}

	widget_pos_t gui::get_position() {
		mc_rect_t rect = this->get_mc_rect();
		return {rect.left, rect.top};
	}

	point_t gui::get_position_as_point() {
		mc_rect_t rect = this->get_mc_rect();
		return {rect.left, rect.top};
	}

	hdc_t gui::get_hdc() {
		return this->_hdc;
	}

	bool_t gui::is_mouse_outside() {
		return this->_is_mouse_outside_window;
	}

	bool_t gui::is_minimized() {
		return this->_is_minimized;
	}

	void_t gui::set_alpha(float_t alpha){
		this->_alpha = alpha;
		this->call_render();
	}

	float_t gui::get_alpha(){
		return this->_alpha;
	}

	void_t gui::set_alpha_color(mc_colorref_t color){
		this->_alpha_color = color;
		this->call_render();
	}

	mc_colorref_t gui::get_alpha_color(){
		return this->_alpha_color;
	}

	void_t gui::set_alpha_color_lock(mc_colorref_t color){
		__critical_scope();
		this->set_alpha_color(color);
	}

	mc_colorref_t gui::get_alpha_color_lock(){
		__critical_scope();
		return this->_alpha_color;
	}

	void_t gui::disable_layer_mode(){
		if (is_layer_mode_enabled()) {
			auto flag = external::get_window_long_ptr(this->_hwnd, GWL_EXSTYLE);
			__ikgl_unset_bit(flag, WS_EX_LAYERED);
			external::set_window_long_ptr(this->_hwnd, GWL_EXSTYLE, flag);
		}
	}

	void_t gui::enable_layer_mode(){
		if (!is_layer_mode_enabled()) {
			auto flag = external::get_window_long_ptr(this->_hwnd, GWL_EXSTYLE);
			__ikgl_set_bit(flag, WS_EX_LAYERED);
			external::set_window_long_ptr(this->_hwnd, GWL_EXSTYLE,  flag);
		}
	}

	void_t gui::disable_layer_mode_lock(){
		__critical_scope();
		this->disable_layer_mode();
	}

	void_t gui::enable_layer_mode_lock(){
		__critical_scope();
		this->enable_layer_mode();
	}

	bool_t gui::is_layer_mode_enabled(){
		return (__ikgl_isset_bit(external::get_window_long_ptr(this->_hwnd, GWL_EXSTYLE) , WS_EX_LAYERED));
	}

	bool_t gui::is_layer_mode_enabled_lock(){
		__critical_scope();
		return this->is_layer_mode_enabled();
	}

	void_t gui::enable_transparent_colorkey(){
		this->_is_use_colorkey = true;
		this->call_render();
	}

	void_t gui::disable_transparent_colorkey(){
		this->_is_use_colorkey = false;
		this->call_render();
	}


	void_t gui::enable_transparent_colorkey_lock() {
		this->_is_use_colorkey = true;

		__critical_scope();
		this->call_render();
	}

	void_t gui::disable_transparent_colorkey_lock() {
		this->_is_use_colorkey = false;

		__critical_scope();
		this->call_render();
	}

	void_t gui::set_window_min_resize(const widget_size_t& size){
		this->_min_size = size;
	}

	void_t gui::set_window_max_resize(const widget_size_t& size){
		this->_max_size = size;
	}

	void_t gui::set_window_min_resize_lock(const widget_size_t& size){
		__critical_scope();
		this->set_window_min_resize(size);
	}

	void_t gui::set_window_max_resize_lock(const widget_size_t& size){
		__critical_scope();
		this->set_window_max_resize(size);
	}

	const widget_size_t& gui::get_window_min_resize() const{
		return this->_min_size;
	}

	const widget_size_t& gui::get_window_max_resize() const{
		return this->_max_size;
	}

	const widget_size_t gui::get_window_min_resize_lock(){
		__critical_scope();
		return this->get_window_min_resize();
	}

	const widget_size_t gui::get_window_max_resize_lock(){
		__critical_scope();
		return this->get_window_max_resize();
	}
}

#undef __critical_scope