/*
Copyright (c) 2019 IkarusDeveloper

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "stdafx.h"
#include "ikarus_gl.h"


namespace ikgl{



	scrollbar::scrollbar(gui* guiptr): 
		_on_scroll_event(nullptr), 
		_button1(nullptr),
		_button2(nullptr),
		_middle_button(nullptr),
		window(guiptr)
	{
		this->__clear_scrollbar();
		this->__create_default_button();
		this->set_size(SCROLLBAR_DEFAULT_WIDTH, SCROLLBAR_DEFAULT_HEIGHT);
	}

	void_t scrollbar::__clear_scrollbar() {
		utilities::destroy_obj_ptr(this->_on_scroll_event);
		this->_status				 = UINT8 (SCROLLBAR_STATUS_DEFAULT);
		this->_value				 = scrollbar_value_t (0);
		this->_step					 = scrollbar_value_t (SCROLLBAR_DEFAULT_STEP);
		
		//* don't need to delete these 
		//* because are children
		this->_button1				= nullptr;
		this->_button2				= nullptr;
		this->_middle_button		= nullptr;
		this->_picked_position		= { 0,0 };
	}

	scrollbar::~scrollbar(){
		this->clear();
	}

	void_t scrollbar::clear(){
		this->__clear_scrollbar();
		this->window::clear();
	}

	bool_t scrollbar::on_render(){
		return this->window::on_render();
	}

	void_t scrollbar::draw( hdc_t& hdc,  gdiplus_graphics_t* graphicsptr){
		if (!this->is_shown()) {
			return;
		}

		this->window::draw(hdc, graphicsptr);
	}

	void_t scrollbar::__draw_background( hdc_t& hdc,  gdiplus_graphics_t* graphicsptr){

		auto rect = this->get_global_rect().to_gdiplus_rect();
		const float_t degree = this->get_global_rotation();
		if (degree != 0.0f) {
			external::rotate_trasform(graphicsptr,degree);
			utilities::normalize_rect(rect, degree);
		}
		if (this->_bg_info) {
			draw_rect(
				graphicsptr,
				rect,
				color_rgba_t(this->_bg_info->bg_color, this->get_global_alpha())
			);
		} else {
			draw_rect(
				graphicsptr,
				rect,
				color_rgba_t(SCROLLBAR_DEFAULT_BACKGROUND_COLOR, this->get_global_alpha())
			);
		}
		if (degree != 0.0f) {
			external::rotate_trasform(graphicsptr,-degree);
		}
	}

	void_t scrollbar::set_on_scroll(widget_event_t event){
		//* deleting old event set
		if(this->_on_scroll_event)
			delete(this->_on_scroll_event);
		//* setting new event
		this->_on_scroll_event = new widget_event_t(event);
	}

	void_t scrollbar::on_scroll(){
		//* triggering the event
		if (this->_on_scroll_event)
			if ((*this->_on_scroll_event)(this->get_id(), this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_scroll_event);
	}

	scrollbar::scrollbar_value_t scrollbar::get_value() const {
		return this->_value;
	}

	void_t scrollbar::set_value(scrollbar::scrollbar_value_t val){
		if (val != this->_value) {
			//* setting value
			this->_value = val;

			//* refreshing pos and draw
			//* but only if the value change
			this->__refresh_middle_button_position();
			this->call_render();
		}
	}

	void_t scrollbar::set_step(scrollbar::scrollbar_value_t step){
		this->_step = step;
	}

	scrollbar::scrollbar_value_t scrollbar::get_step() const {
		return this->_step;
	}

	void_t scrollbar::__create_default_button(){
		//* remove child will delete
		//* the object in the heap
		if(this->_button2)
			this->remove_child(this->_button2);
		if (this->_button1)
			this->remove_child(this->_button2);
		if(this->_middle_button)
			this->remove_child(this->_middle_button);

		//* create new buttons
		//* and set as child
		this->_button1			= this->create_element<button>();
		this->_button2			= this->create_element<button>();
		this->_middle_button	= this->create_element<button>();

		//* setting default sizes
		this->_button2->set_size(SCROLLBAR_DEFAULT_WIDTH,  SCROLLBAR_DEFAULT_STEP_BUTTON_HEIGHT);
		this->_button1->set_size(SCROLLBAR_DEFAULT_WIDTH,  SCROLLBAR_DEFAULT_STEP_BUTTON_HEIGHT);
		this->_middle_button->set_size(SCROLLBAR_DEFAULT_WIDTH , SCROLLBAR_DEFAULT_MIDDLEBUTTON_HEIGHT);

		//* setting default colors
		button_color_info_t color;
		color.default_color			= SCROLLBAR_DEFAULT_BUTTON_DEFAULT_COLOR;
		color.down_color			= SCROLLBAR_DEFAULT_BUTTON_DOWN_COLOR;
		color.over_color			= SCROLLBAR_DEFAULT_BUTTON_OVER_COLOR;

		this->_button2->set_colors(&color);
		this->_button1->set_colors(&color);
		this->_middle_button->set_colors(&color);

		//* set default position
		this->_button2->move_to_bottom();
		this->_button1->move_to_top();

		this->_button2->move_horizontal_center();
		this->_button1->move_horizontal_center();

		this->__refresh_middle_button_position();
		this->__assign_button_events();
	}





	void_t scrollbar::__assign_button_events(){
			//* set upleft event
		this->_button1->set_on_click_event(__ikgl_wevent_lambda({
			if (c->get_parent()->get_widget_type() == scrollbar::type()) {
				((scrollbar*)c->get_parent())->on_click_button1();
			}return widget_event_return_t::KEEP_CALLBACK;
		}));

			//* set down right button
		this->_button2->set_on_click_event(__ikgl_wevent_lambda({
			if (c->get_parent()->get_widget_type() == scrollbar::type()) {
				((scrollbar*)c->get_parent())->on_click_button2();
			}return widget_event_return_t::KEEP_CALLBACK;
		}));

		
		this->_middle_button->set_on_mouseleft_down(__ikgl_wevent_lambda({
			if (c->get_parent()->get_widget_type() == scrollbar::type()) {
				scrollbar* scroll_bar = ((scrollbar*)c->get_parent());
				scroll_bar->set_status(SCROLLBAR_STATUS_PICKED);
				if (scroll_bar->get_gui()) {
					 widget_pos_t pos = scroll_bar->get_gui()->get_mouse_position();
					 widget_pos_t btn_pos = c->get_global_position();

					 pos.x -= btn_pos.x;
					 pos.y -= btn_pos.y;

					 scroll_bar->set_picked_position(pos);
				}
			}return widget_event_return_t::KEEP_CALLBACK;
		}));



		this->_middle_button->set_on_mouseleft_up(__ikgl_wevent_lambda({
			if (c->get_parent()->get_widget_type() == scrollbar::type()) {
				((scrollbar*)c->get_parent())->set_status(SCROLLBAR_STATUS_DEFAULT);
			}return widget_event_return_t::KEEP_CALLBACK;
		}));



		this->_middle_button->set_on_mousemove(__ikgl_wevent_lambda({
			if (c->get_parent()->get_widget_type() == scrollbar::type()) {
				if (c->get_parent()->get_gui()) {
					 scrollbar* scroll_bar = (scrollbar*)c->get_parent();

					 if (scroll_bar && scroll_bar->get_status() == SCROLLBAR_STATUS_PICKED) {
 						const widget_pos_t& mouse_pos = scroll_bar->get_gui()->get_mouse_position();
 						scroll_bar->follow_mouse(mouse_pos);
					 }
				}
			}return widget_event_return_t::KEEP_CALLBACK;
		}));
	}




	void_t scrollbar::on_click_button1(){
		if (this->_value > 0.0f) {
			this->_value	= __ikgl_max(this->_value - this->_step , 0.0f);

			this->__refresh_middle_button_position();
			this->on_scroll();
			this->call_render();
		}
	}



	void_t scrollbar::on_click_button2(){
		if (this->_value < 1.0f) {
			this->_value	= __ikgl_min(this->_value + this->_step  , 1.0f);

			this->__refresh_middle_button_position();
			this->on_scroll();
			this->call_render();
		}
	}




	void_t scrollbar::__refresh_middle_button_position(){
		coord_t h = static_cast<coord_t>(
			this->_button1->get_height() + (coord_t)(
				this->_value * ((scrollbar_value_t) (this->get_height() - (this->_button1->get_height() + this->_button2->get_height() + this->_middle_button->get_height() + 6))) 
			)
		);

		this->_middle_button->set_position(this->_middle_button->get_x() , h+3);
	}



	scrollbar::scrollbar_status_t scrollbar::get_status() const {
		return this->_status;
	}



	void_t scrollbar::set_status(scrollbar_status_t status){
		this->_status = status;
		this->call_render();
	}




	void_t scrollbar::set_size(const widget_size_t& size){
		this->window::set_size(size);
		
		if(this->_middle_button)
			this->__refresh_middle_button_position();

		if(this->_button1)
			this->_button1->set_position(0,0);

		if(this->_button2)
			this->_button2->move_to_bottom();

		this->call_render();
	}



	void_t scrollbar::set_size(const widget_size_t* size){
		this->scrollbar::set_size(*size);
	}




	void_t scrollbar::set_size(const coord_t w, const coord_t h){
		widget_size_t size;
		size.width=w;
		size.height=h;
		
		this->scrollbar::set_size(size);
	}




	void_t scrollbar::set_height(coord_t h){
		this->set_size(get_width() , h);
		if(this->_button2)
			this->_button2->move_to_bottom();
	}



	void_t scrollbar::set_middle_button_height(coord_t h){
		if (this->_middle_button){
			this->_middle_button->set_size(this->_middle_button->get_width(), h);
			this->__refresh_middle_button_position();
		}
	}




	void_t scrollbar::set_picked_position(const widget_pos_t& p){
		this->_picked_position.x=p.x;
		this->_picked_position.y=p.y;
	}




	void_t scrollbar::follow_mouse(const widget_pos_t& p){
		coord_t tot_leng = ((coord_t) (this->get_height() - (this->_button1->get_height() + this->_button2->get_height() + this->_middle_button->get_height()))) - 6;
		coord_t pick_off = this->_picked_position.y;

		coord_t diff = __ikgl_max(0 , p.y - this->get_global_position().y);
		diff = __ikgl_max(0, diff - pick_off);
		


		this->_value = float_t(diff) / float_t(tot_leng);
		this->_value = __ikgl_minmax(0.0f , this->_value , 1.0f);

		this->__refresh_middle_button_position();
		this->call_render();
	}

	button * scrollbar::get_button1_ptr() const {
		return this->_button1;
	}

	button * scrollbar::get_button2_ptr() const {
		return this->_button2;
	}

	button * scrollbar::get_middle_button_ptr() const {
		return this->_middle_button;
	}


	void_t scrollbar::hide_children() {
		auto is_protect = [this](window* p) -> bool_t {
			if(p->get_id() == this->_button1->get_id())
				return true;
			if(p->get_id() == this->_button2->get_id())
				return true;
			if(p->get_id() == this->_middle_button->get_id())
				return true;
			return false;
		};

		for (auto ptr : this->_children_list) {
			if(!is_protect(ptr))
				ptr->hide();
		}
	}


	void_t scrollbar::remove_children() {
		auto is_protect = [this](window* p) -> bool_t {
			if(p->get_id() == this->_button1->get_id())
				return true;
			if(p->get_id() == this->_button2->get_id())
				return true;
			if(p->get_id() == this->_middle_button->get_id())
				return true;
			return false;
		};


		window* ptr = nullptr;
		for (auto iter = this->_children_list.begin(); iter != this->_children_list.end(); 
			ptr = iter !=this-> _children_list.end() ? *iter : nullptr) {

			if(is_protect(ptr))
				iter++;

			else {
				delete(ptr);
				iter = this->_children_list.erase(iter);
			}
		}
	}
}