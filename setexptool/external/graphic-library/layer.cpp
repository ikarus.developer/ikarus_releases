/*
Copyright (c) 2019 IkarusDeveloper

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "stdafx.h"
#include "ikarus_gl.h"
#include "layer.h"

namespace ikgl
{
	layer::layer(gui* guiptr) : _gui(guiptr){
		this->clear();
	}

	layer::~layer(){
		this->clear();
	}

	void_t layer::clear(){
		for(auto pchild : this->_children_list)
			delete(pchild);

		this->_children_list.clear();
	}

	bool_t layer::on_render(){
		for(auto& child: this->_children_list)
			if (child && child->on_render()) {
				return true;
			}

		return false;
	}

	void_t layer::render(hdc_t& hdc,  gdiplus_graphics_t* graphicsptr){
		for (auto& child : this->_children_list) {
			child->draw(hdc, graphicsptr);
		}
	}

	void_t layer::update(const double_t time){
		for(auto& child : this->_children_list)
			child->update(time);
	}

	void_t layer::add_child(window* child){
		this->_children_list.push_back(child);
	}

	void_t layer::remove_child(window* child){
		auto it = std::find(this->_children_list.begin() , this->_children_list.end() , child);
		if (it != this->_children_list.end()){
			delete(*it);
			this->_children_list.erase(it);
		}
	}

	window* layer::get_picked(const widget_pos_t& pos){
		if(this->_children_list.empty())
			return nullptr;

		auto it=this->_children_list.end();
		window* win=nullptr;

		while (it != this->_children_list.begin()) {
			auto child = *(--it);
			if(child->is_shown() && (win= child->get_picked(pos)))
				return win;
		}

		return nullptr;
	}

	window* layer::get_child(const int id) const {
		window* temp = nullptr;

		//* iterating the children and
		//* searching by id the child in the list
		for (auto childptr : this->_children_list) {
			if (childptr->get_id() == id) {
				return childptr;
			}

			else if (childptr->get_child(id, &temp)) {
				return temp;
			}
		}

		//* if not exists return nullptr
		return nullptr;
	}

	bool_t layer::get_child(const int id, window** winptr) const {
		//* if the pointer is not allocated memory
		//* return before to unreference it
		if (!winptr) {
			return false;
		}

		//* else set the value of the pointer to nullptr
		//* to prevent inconsistent results
		else {
			*winptr = nullptr;
		}

		//* iterating the children list
		//* and searching the child by id
		for (auto& child : this->_children_list) {
			if (child->get_id() == id) {
				*winptr = child;
				break;
			}

			else if (child->get_child(id, winptr)) {
				break;
			}
		}

		return *winptr != nullptr;
	}

	void_t layer::on_mousemove(const widget_pos_t& oldpos,const widget_pos_t& newpos){
		window *old_win = nullptr , *new_win = nullptr;

		old_win = this->get_picked(oldpos);
		new_win = this->get_picked(newpos);
	

		if (old_win != new_win){
			if (old_win) {
				old_win->on_mouseover_out();
			}

			if (new_win) {
				new_win->on_mouseover_in();
#ifdef _DEBUG
				auto name = new_win->get_window_name();
				__ikgl_debugW(L"POINTED_window: [%s]\n", name.empty() ? utilities::get_empty_name_by_type(new_win).c_str() : name.c_str());
#endif
			}
		}

		if (new_win) {
			new_win->on_mousemove();
		}
	}

	void_t layer::on_resize_gui() {
		for (auto& win : this->_children_list) {
			win->on_resize_gui();
		}
	}

	window::children_list_t& layer::get_children_ref(){
		return this->_children_list;
	}
}


