/*
Copyright (c) 2019 IkarusDeveloper

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "stdafx.h"
#include "ikarus_gl.h"

namespace ikgl
{
	//* this is used to set
	//* an initial font to the textline
	void_t make_default_font(font_t&  font){
		font.font_family = L"Arial";
		font.size	= 12;
		font.style	= Gdiplus::FontStyleRegular;
		font.unit	= Gdiplus::UnitPixel;
	}

	font_t make_font(wstring_t family, uint_t size, int_t style, gdiplus_unit_t unit) {
		font_t font;
		font.font_family = family;
		font.size = size;
		font.style = style;
		font.unit = unit;

		return font;
	}

	font_t make_font_easy(wstring_t family, uint_t size) {
		return make_font(family, size);
	}




	//* initializing of the pointer to null
	//* and Window with gui pointer
	text::text(gui* guiptr) :
	_on_change_text_event(nullptr),
	_font(nullptr),
	window(guiptr)
	{
		this->__clear_text();
		this->__create_default_font();
	}

	//* clear will call window clear
	//* and clear textline
	text::~text() {
		this->clear();
	}

	//* set a default font to initialize the textline
	//* used the most commonly used font 
	void_t text::__create_default_font() {
		this->_font = new gdiplus_font_t(&gdiplus_font_family_t(L"Arial") , 12);
		make_default_font(this->_font_info);
	}

	void_t text::clear(){
		this->window::clear();
		this->__clear_text();
	}

	void_t text::__clear_text() {
		//* this will destroy if not null 
		//* and set to null the pointers
		utilities::destroy_obj_ptr(this->_on_change_text_event);
		utilities::destroy_obj_ptr(this->_font);

		//* this will set a default value
		//* to the alignment (left, middle)
		this->_string_horizontal_align = ikgl::text_halign_t::TEXT_HALIGN_LEFT;
		this->_string_vertical_align   = ikgl::text_valign_t::TEXT_VALIGN_TOP;

		//* setting of other default parameters
		this->_ws_text = L"";
		this->_color   = TEXTLINE_DEFAULT_COLOR;
	}

	bool_t text::on_render(){
		return this->window::on_render();		
	}

	void_t text::draw(hdc_t& hdc,  gdiplus_graphics_t* graphicsptr){
		if (!this->is_shown()) {
			return;
		}

		
		//* drawing first the text
		//* and after the children
		this->window::draw_background(hdc, graphicsptr);		
		this->draw_text(hdc,  graphicsptr);
		this->window::draw_children(hdc, graphicsptr);
		this->_is_to_render = false;
	}

	void_t text::set_text(wstring_t text, bool resize) {
		this->_ws_text = text;

		//*autoresize
		if (resize) {
			this->set_size(this->get_text_size());
		}

		this->on_change_text();
		this->call_render();
	}

	void_t text::set_text(string_t  text, bool resize){
		this->set_text(utilities::to_wstring(text), resize);
	}

	void_t text::draw_text(hdc_t& hdc,  gdiplus_graphics_t* graphicsptr){
		if(this->_ws_text.empty())
			return;


		//* creating a solid brush using the color set
		//* and the global alpha
		gdiplus_solid_brush_t brush (
			gdiplus_color_t (
				color_rgba_t(
					this->_color, this->get_global_alpha()
				).to_gdiplus_argb()
			)
		);

		//* setting the format alignments
		gdiplus_string_format_t format = gdiplus_string_format_t::GenericTypographic();

		//* setting tab stops
		const auto tabcount = std::count(this->_ws_text.begin(), this->_ws_text.end(), (wchar_t)'\t');

		if (tabcount !=0) {
			std::vector<gdiplus_real_t> vectab;
			vectab.resize(tabcount, 15.f);
			format.SetTabStops(0, (INT) tabcount, vectab.data());
		}
		

		const auto degree = this->get_global_rotation();
		auto pos   = this->get_global_position();
		this->__normalize_by_alignment(pos);
		
		if (degree != 0.0f) {
			external::rotate_trasform(graphicsptr,degree);
			utilities::normalize_position(pos, degree);
		}

		//* basically draw the text
		graphicsptr->DrawString(this->_ws_text.c_str(), (int_t) this->_ws_text.length(), this->_font, pos.to_gdiplus_pointf(), &format, &brush);

		if (degree != 0.0f) {
			external::rotate_trasform(graphicsptr,-degree);
		}
	}

	wstring_t text::get_textw(){
		return this->_ws_text;
	}

	string_t text::get_text(){
		return utilities::to_string(this->_ws_text);
	}

	void_t text::get_text(char_ptr_t szBuff, dword_t dwMaxLen){
		wcstombs(szBuff, this->_ws_text.c_str(), dwMaxLen);
	}

	void_t text::set_font(const font_t* fontInfo){
		//* deleting the old set font
		//* or the default font
		if (this->_font) {
			delete(this->_font);
		}

		//* allocating the new font using the information
		//* into the font_t
		this->_font      = new gdiplus_font_t(&gdiplus_font_family_t(fontInfo->font_family.c_str()) , gdiplus_real_t(fontInfo->size), fontInfo->style, fontInfo->unit);
		this->_font_info = *fontInfo;

		//* need to redraw
		this->call_render();
	}

	void_t text::set_font(const font_t& fontInfo){
		//* deleting the old set font
		//* or the default font
		if (this->_font) {
			delete(this->_font);
		}

		//* allocating the new font using the information
		//* into the font_t
		this->_font = new gdiplus_font_t(&gdiplus_font_family_t(fontInfo.font_family.c_str()), gdiplus_real_t(fontInfo.size), fontInfo.style, fontInfo.unit);
		this->_font_info = fontInfo;

		//* need to redraw
		this->call_render();
	}

	void_t text::get_font(font_t& r_out){
		memcpy(&r_out, &this->_font_info, sizeof(font_t));
	}

	const font_t& text::get_font(){
		return this->_font_info;
	}

	const color_rgba_t& text::get_color() const{
		return this->_color;
	}

	void_t text::set_color(const color_rgba_t& color){
		this->_color = color;
		this->call_render();
	}

	void_t text::set_horizontal_alignment(text_halign_t halign) {
		this->_string_horizontal_align = halign;
		this->call_render();
	}

	void_t text::set_vertical_alignment(text_valign_t valign){
		this->_string_vertical_align = valign;
		this->call_render();
	}

	void_t text::set_on_change_text(widget_event_t event) {
		//* deleting old event if set
		if(this->_on_change_text_event)
			delete(this->_on_change_text_event);

		//* save information
		this->_on_change_text_event = new widget_event_t(event);
	}

	void_t text::on_change_text(){
		//* if set a trigger
		//* this will run it
		if (this->_on_change_text_event)
			if ((*this->_on_change_text_event)(this->get_id(), this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_change_text_event);
	}

	widget_size_t text::get_text_size() {
		
		if (this->_gui) {
			gdiplus_graphics_t gr(this->_gui->get_hdc());
			external::set_text_rendering_hint_antialias(&gr);

			return utilities::measure_string_strict(
				this->_font_info.size,
				&gr,
				this->_ws_text.c_str(),
				static_cast<int_t>(this->_ws_text.length()),
				this->_font,
				this->get_global_position()
			);
		}

		else {
			return {0,0};
		}		
	}

	void_t text::__normalize_by_alignment(gdiplus_rect_t & rect, const wstring_t& txt) {
		const wstring_t& _txt = txt.empty() ? this->_ws_text : txt;
		
		//* check if really needs
		if (this->_string_horizontal_align == text_halign_t::TEXT_HALIGN_LEFT && this->_string_vertical_align == text_valign_t::TEXT_VALIGN_TOP)
			return;

		//* getting one time the size to prevent multiple 
		//* call of get_text_size which is a weight func
		gdiplus_graphics_t gr(this->_gui->get_hdc());
		external::set_text_rendering_hint_antialias(&gr);
		auto size = utilities::measure_string_strict(
			this->_font_info.size,
			&gr,
			_txt.c_str(),
			static_cast<int_t>(_txt.length()),
			this->_font,
			this->get_global_position()
		);
		

		if (size.height == 0)
			size.height = this->_font_info.size +2;

		//* normalize horizontal alignment
		switch (this->_string_horizontal_align) {
			case text_halign_t::TEXT_HALIGN_LEFT: 
				break;
			case text_halign_t::TEXT_HALIGN_CENTER: {
					 const auto tsize = (size.width/2);
					 const auto wsize = (this->_size.width/2);
					 rect.X -= tsize-wsize;
				}break;


			case text_halign_t::TEXT_HALIGN_RIGHT: {
					 const auto tsize = (size.width);
					 const auto wsize = (this->_size.width);
					 rect.X += wsize-tsize;
				}break;

			default:break;
		}



		//* normalize vertical alignment
		switch (this->_string_vertical_align) {
			case text_valign_t::TEXT_VALIGN_TOP: 
				break;
			case text_valign_t::TEXT_VALIGN_CENTER: {
				 const auto tsize = (size.height/2);
				 const auto wsize = (this->_size.height/2);
				 rect.Y += wsize-tsize;
				} break;

			case text_valign_t::TEXT_VALIGN_BOTTOM: {
				 const auto tsize = (size.height);
				 const auto wsize = (this->_size.height);
				 rect.Y += wsize-tsize;
				}break;

			default:break;
		}

	}

	void_t text::__normalize_by_alignment(widget_pos_t & pos, const wstring_t& txt) {
		const wstring_t& _txt = txt.empty() ? this->_ws_text : txt;

		//* check if really needs
		if (this->_string_horizontal_align == text_halign_t::TEXT_HALIGN_LEFT && this->_string_vertical_align == text_valign_t::TEXT_VALIGN_TOP)
			return;

		//* getting one time the size to prevent multiple 
		//* call of get_text_size which is a weight func
		gdiplus_graphics_t gr(this->_gui->get_hdc());
		external::set_text_rendering_hint_antialias(&gr);
		auto size = utilities::measure_string_strict(
			this->_font_info.size,
			&gr,
			_txt.c_str(),
			static_cast<int_t>(_txt.length()),
			this->_font,
			this->get_global_position()
		);


		//* normalize horizontal alignment
		switch (this->_string_horizontal_align) {
			case text_halign_t::TEXT_HALIGN_LEFT: break;
			case text_halign_t::TEXT_HALIGN_CENTER: {
					 const auto tsize = (size.width/2);
					 const auto wsize = (this->_size.width/2);
					 pos.x -= tsize-wsize;
				}break;


			case text_halign_t::TEXT_HALIGN_RIGHT: {
					const auto tsize = (size.width);
					const auto wsize = (this->_size.width);
					pos.x += wsize-tsize;
				}break;

			default:break;
		}



		//* normalize vertical alignment
		switch (this->_string_vertical_align) {
			case text_valign_t::TEXT_VALIGN_TOP: break;
			case text_valign_t::TEXT_VALIGN_CENTER: {
					 const auto tsize = (size.height/2);
					 const auto wsize = (this->_size.height/2);
					 pos.y += wsize-tsize;
				} break;

			case text_valign_t::TEXT_VALIGN_BOTTOM: {
					 const auto tsize = (size.height);
					 const auto wsize = (this->_size.height);
					 pos.y += wsize-tsize;
				}break;

			default:break;
		}

	}
}

