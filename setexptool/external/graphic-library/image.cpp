/*
Copyright (c) 2019 IkarusDeveloper

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "stdafx.h"
#include "ikarus_gl.h"

namespace ikgl
{
	image::image(gui* guiptr) : 
	_gdi_image(nullptr),
	window(guiptr)
	{
		this->__clear_image();
	}


	image::~image(){
		this->clear();
	}


	void_t image::clear() {
		this->window::clear();
		this->__clear_image();
	}


	void_t image::__clear_image(){
		this->_rect_scale = {1.0f,1.0f,1.0f,1.0f};
		utilities::zero_obj(this->_image_size);
		utilities::zero_obj(this->_apply_color);
		
		utilities::destroy_obj_ptr(this->_gdi_image);
	}



	bool_t image::on_render(){
		return this->window::on_render();
	}



	bool_t image::is_static_draw() {
		if (!this->_gdi_image) {
			return true;
		}

		else {
			return		this->_gdi_image->GetWidth()	== this->_image_size.width	&&	this->_gdi_image->GetHeight()	== this->_image_size.height &&
						this->_rect_scale.top_scale		== 1.0f 					&& this->_rect_scale.bottom_scale	== 1.0f &&
						this->_rect_scale.left_scale	== 1.0f 					&& this->_rect_scale.right_scale	== 1.0f &&
						this->_apply_color.a			== 0.0f 					&& this->_apply_color.r				== 0.0f && 
						this->_apply_color.g			== 0.0f 					&& this->_apply_color.b				== 0.0f;
		}
	}



	void_t image::draw(hdc_t& hdc,  gdiplus_graphics_t* graphicsptr){
		if(!this->is_shown())
			return;
		
		this->window::draw_background(hdc, graphicsptr);

		const float_t alpha = this->get_global_alpha();
		const float_t degree= this->get_global_rotation();
		auto gpos = this->get_global_position();
		
		if (degree != 0.0f) {
			external::rotate_trasform(graphicsptr,degree);
			utilities::normalize_position(gpos, degree);
		}

		if (alpha==1.f && this->is_static_draw()) {
			
			gdiplus_status_t result;
			
			if ((result = external::draw_image(graphicsptr,
				this->_gdi_image, 
					 gdiplus_real_t(gpos.x), gdiplus_real_t(gpos.y),
					 gdiplus_real_t(this->_image_size.width), gdiplus_real_t(this->_image_size.height)
				)) != Gdiplus::Ok) {
				__ikgl_error("is not drawing image (code %s ) \n", utilities::get_gdiplus_status_str_name(result));
			}

			if (degree != 0.0f) {
				external::rotate_trasform(graphicsptr,-degree);
			}
		}

		else {
			
			gdiplus_image_attributes_t img_attr;
			auto* image = this->_gdi_image;

			const float_t a = __ikgl_minmax(0.0f, this->_apply_color.a, 1.0f);
			const float_t r = __ikgl_minmax(0.0f, this->_apply_color.r*a, 1.0f);
			const float_t g = __ikgl_minmax(0.0f, this->_apply_color.g*a, 1.0f);
			const float_t b = __ikgl_minmax(0.0f, this->_apply_color.b*a, 1.0f);

			const float_t al = alpha;

			gdiplus_color_matrix_t matrix = {
				1.0f,    0.0f,    0.0f,    0.0f,    0.0f,
				0.0f,    1.0f,    0.0f,    0.0f,    0.0f,
				0.0f,    0.0f,    1.0f,    0.0f,    0.0f,
				0.0f,    0.0f,    0.0f,    alpha,   0.0f,
				r,	     g,       b,       0.0f,       1.0f,
			};

			img_attr.SetColorMatrix(&matrix);

			float_t left_rect  = -(this->_rect_scale.left_scale - 1.f);
			float_t right_rect =   this->_rect_scale.right_scale;

			const float_t image_width  = float_t(this->_image_size.width);
			const float_t image_height = float_t(this->_image_size.height);

			const float_t x_scale = image_width / float_t(image->GetWidth());
			const float_t y_scale = image_height / float_t(image->GetHeight());

			external::scale_trasform(
				graphicsptr, 
				x_scale,
				y_scale
			);

			auto x = gpos.x;

			while (left_rect < right_rect) {
				auto y = gpos.y;

				float_t rect_horizontal = 0.0f;

				if (left_rect < 0.0f) {
					 rect_horizontal = left_rect - (float_t(int_t(left_rect)));
					 if (rect_horizontal == 0.0f) {
 						rect_horizontal = left_rect >= 0.0f ? 1.f : -1.f;
					 }		
				}

				else {
					rect_horizontal = __ikgl_min(right_rect - left_rect, 1.0f);
				}

				float_t top_rect    = -(this->_rect_scale.top_scale - 1.0f);
				float_t bottom_rect =   this->_rect_scale.bottom_scale;

				while (top_rect < bottom_rect) {
					 float_t rect_vertical = 0.0f;

					 if (top_rect < 0.0f) {
 						rect_vertical = top_rect - (float_t(int_t(top_rect)));
 						if (rect_vertical == 0.0f) {
 							rect_vertical = top_rect >= 0.0f ? 1.f : -1.f;
 						}
					 }

					 else {
 						rect_vertical = __ikgl_min(bottom_rect - top_rect, 1.0f);
					 }


					 //settings rects
					 gdiplus_rectf_t src_rect;
					 gdiplus_rectf_t dest_rect;

					 //* we can use the original size because is Scaled the transform of the graphics
					 const float_t image_original_width = float_t(image->GetWidth());
					 const float_t image_original_height= float_t(image->GetHeight());

					 //source rect
					 src_rect.X = rect_horizontal > 0.0f ? 0 : image_original_width  * (1.0f + rect_horizontal);
					 src_rect.Y = rect_vertical   > 0.0f ? 0 : image_original_height * (1.0f + rect_vertical);

					 src_rect.Width =  gdiplus_real_t(float_t(image_original_width)  * __ikgl_max(rect_horizontal, -rect_horizontal));
					 src_rect.Height = gdiplus_real_t(float_t(image_original_height) * __ikgl_max(rect_vertical,   -rect_vertical));

					 //dest rect
					 dest_rect.X = gdiplus_real_t(x);
					 dest_rect.Y = gdiplus_real_t(y);

					 dest_rect.Width  = gdiplus_real_t(image_original_width   * __ikgl_max(rect_horizontal, -rect_horizontal));
					 dest_rect.Height = gdiplus_real_t(image_original_height  * __ikgl_max(rect_vertical,   -rect_vertical));

					 //to fix the scale transform about coordinate
					 dest_rect.X /= x_scale;
					 dest_rect.Y /= y_scale;


					 if (external::draw_image(graphicsptr,image, dest_rect,src_rect.X, src_rect.Y, src_rect.Width, src_rect.Height, Gdiplus::UnitPixel, &img_attr) != Gdiplus::Ok) {
 						__ikgl_error("is not drawing image correctly (is not static draw) ");
					 }

					 //upgrading values
					 y += coord_t(dest_rect.Height);
					 top_rect += __ikgl_max(rect_vertical, -rect_vertical);
				}

				x += coord_t(image_width  * __ikgl_max(rect_horizontal, -rect_horizontal));
				left_rect += __ikgl_max(rect_horizontal, -rect_horizontal);
			}

			external::reset_trasform(graphicsptr);
		}

		this->window::draw_children(hdc, graphicsptr);
		this->_is_to_render = false;
	}



	//this methods invlidate the rect setting because it need to set the size of the image to the value to set
	void_t image::set_size(coord_t width, coord_t height){
		this->_image_size = { width, height };
		this->window::set_size(this->_rect_scale.get_size(this->_image_size));	
		this->call_render();
	}


	void_t image::set_size(const widget_size_t& size) {
		this->image::set_size(size.width, size.height);
	}



	bool_t image::load_image(string_t path) {
		wstring_t wpath(path.begin(), path.end());
		return this->load_image(wpath);
	}



	bool_t image::load_image(wstring_t path) {
		if (this->_gdi_image) {
			delete(this->_gdi_image);
		}

		std::unique_ptr<gdiplus_image_t> image(gdiplus_image_t::FromFile(path.c_str() , FALSE));
		bool_t success = image != nullptr && image->GetLastStatus() == Gdiplus::Ok;
		
		if (success) {
			this->image::set_size(image->GetWidth(), image->GetHeight());
			this->_gdi_image = image.release();
		}

		return success;
	}



	bool_t image::load_image(void_t* data, dword_t len){
		if (this->_gdi_image) {
			delete(this->_gdi_image);
		}

		std::unique_ptr<gdiplus_image_t> image(utilities::get_image_from_buffer(data,len));

		//assign the size to the image
		if (image && image->GetLastStatus() == Gdiplus::Ok) {
			this->image::set_size(image->GetWidth(), image->GetHeight());
			this->_gdi_image = image.release();
			return true;
		}

		//reporting error on debug configuration
		else {
			return false;
		}
	}



	bool_t image::load_image(hbitmap_t hbitmap, gdiplus_pixel_format_t pixel_format) {
		if (this->_gdi_image) {
			delete(this->_gdi_image);
		}

		BITMAP source_info = { 0 };
		if( !::GetObject( hbitmap, sizeof( source_info ), &source_info ) )
			return Gdiplus::GenericError;

		gdiplus_status_t s;

		std::unique_ptr< Gdiplus::Bitmap > target( new Gdiplus::Bitmap( source_info.bmWidth, source_info.bmHeight, pixel_format ) );
		if( !target.get() )
			return false;

		if( ( s = target->GetLastStatus() ) != Gdiplus::Ok )
			return false;

		Gdiplus::BitmapData target_info;
		gdiplus_rect_t rect( 0, 0, source_info.bmWidth, source_info.bmHeight );

		s = target->LockBits( &rect, Gdiplus::ImageLockModeWrite, pixel_format, &target_info );
		if( s != Gdiplus::Ok )
			return false;

		if( target_info.Stride != source_info.bmWidthBytes )
			return false; // pixel_format is wrong!

		memcpy( target_info.Scan0, source_info.bmBits, source_info.bmWidthBytes * source_info.bmHeight );

		s = target->UnlockBits( &target_info );
		if( s != Gdiplus::Ok )
			return false;

		
		this->image::set_size(source_info.bmWidth, source_info.bmHeight);
		this->_gdi_image = target.release();
		return true;
	}



	//this is to repeat the image (or to truncate it for value < 1.0f)
	void_t image::set_rect_scale(float_t rLeft, float_t rTop, float_t rRight, float_t rBottom){
		rect_scale_t r;
		r.left_scale	= rLeft;
		r.top_scale		= rTop;
		r.right_scale	= rRight;
		r.bottom_scale	= rBottom;

		this->set_rect_scale(r);
	}




	void_t image::set_rect_scale(rect_scale_t& rScale){
		this->_rect_scale = rScale;
		this->window::set_size(this->_rect_scale.get_size(this->_image_size));	
		this->call_render();
	}





	rect_scale_t image::get_rect_scale(){
		return this->_rect_scale;
	}




	void_t image::get_rect_scale(rect_scale_t& rScale){
		rScale = this->_rect_scale;
	}




	void_t image::get_rect_scale(float_t& left, float_t& top, float_t& right, float_t& bottom){
		left	= this->_rect_scale.left_scale;
		top 	= this->_rect_scale.top_scale;
		right	= this->_rect_scale.right_scale;
		bottom  = this->_rect_scale.bottom_scale;
	}


	void_t image::apply_color(const color_rgba_t& color) {
		this->_apply_color = color;
		this->call_render();
	}
	
	void_t image::__update_size_translating() {
		if (this->_size != this->_dest_size) {
			if (this->_translating_size_step.width != 0) {
				this->_size_on_translation.change_width(this->_translating_size_step.width);

				if (this->_translating_size_step.width > 0) {
					 if (this->_size_on_translation.width > this->_dest_size.width) {
 						this->_size_on_translation.width = float_t(this->_dest_size.width);
					 }
				}

				else {
					 if (this->_size_on_translation.width < this->_dest_size.width) {
 						this->_size_on_translation.width = float_t(this->_dest_size.width);
					 }
				}
			}

			if (this->_translating_size_step.height != 0) {
				this->_size_on_translation.height += this->_translating_size_step.height;

				if (this->_translating_size_step.height > 0) {
					 if (this->_size_on_translation.height > this->_dest_size.height) {
 						this->_size_on_translation.height = float_t(this->_dest_size.height);
					 }
				}

				else {
					 if (this->_size_on_translation.height < this->_dest_size.height) {
 						this->_size_on_translation.height = float_t(this->_dest_size.height);
					 }
				}
			}

			this->_size = this->_size_on_translation;
			this->_image_size = this->_size_on_translation;

			if (this->_on_translating_size_end && this->_size == this->_dest_size) {
				if ((*this->_on_translating_size_end)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
					utilities::destroy_obj_ptr(this->_on_translating_size_end);
			}

			this->call_render();
			//* on resize parent implementation
			for (auto& child : this->_children_list) {
				child->on_resize_parent();
			}
		}
	}

	widget_size_t image::get_real_image_size()
	{
		if (!this->_gdi_image)
			return this->_image_size;
		return {this->_gdi_image->GetWidth(), this->_gdi_image->GetHeight()};
	}
}

