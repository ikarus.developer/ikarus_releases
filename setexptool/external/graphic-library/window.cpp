/*
Copyright (c) 2019 IkarusDeveloper

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "stdafx.h"
#include "ikarus_gl.h"

namespace ikgl
{
	int_t alloc_id() {
		static atomic_int_t id=1;
		return id++;
	}

	window::window(gui* guiptr) :
		_on_mouseleft_up_event(nullptr),
		_on_mouseleft_down_event(nullptr),
		_on_mouseright_up_event(nullptr),
		_on_mouseright_down_event(nullptr),
		_on_mousemove_event(nullptr),
		_on_resize_gui_event(nullptr),
		_on_redraw_event(nullptr),
		_on_set_focus_event(nullptr),
		_on_kill_focus_event(nullptr),
		_on_mouseover_in_event(nullptr),
		_on_mouseover_out_event(nullptr),
		_on_render_event(nullptr),
		_on_resize_parent_event(nullptr),
		_on_move_parent_event(nullptr),
		_on_rotate_parent_event(nullptr),
		_on_resize_event(nullptr),
		_on_move_event(nullptr),
		_on_show_event(nullptr),
		_on_hide_event(nullptr),
		_on_update_event(nullptr),
		_on_fade_end(nullptr),
		_on_translating_pos_end(nullptr),
		_on_translating_size_end(nullptr),
		_on_mousewheel_event(nullptr),
		_on_key_down_event(nullptr),
		_on_key_up_event(nullptr),
		_on_custom_render(nullptr),
		_bg_info(nullptr),
		_parent(nullptr),
		_ws_name(L""),
		_gui(guiptr)
	{
		this->clear();
	}

	window::~window(){
		this->clear();
	}

	void_t window::clear(){
		if (this->_ws_name.empty() == false && this->_gui) {
			this->_gui->__unregister_window_name(this->_ws_name);
		}
		
		
		for (auto ptr : this->_children_list) 
			delete(ptr);
		
		this->_children_list.clear();
		this->_timer_events_vec.clear();

		utilities::zero_obj(this->_pos);
		utilities::zero_obj(this->_dest_pos);
		utilities::zero_obj(this->_translating_pos_step);
		utilities::zero_obj(this->_pos_on_translation);
		utilities::zero_obj(this->_size);
		utilities::zero_obj(this->_dest_size);
		utilities::zero_obj(this->_translating_size_step);
		utilities::zero_obj(this->_size_on_translation);
		utilities::zero_obj(this->_mousewheel_data);

		utilities::destroy_obj_ptr(this->_on_mouseleft_up_event);
		utilities::destroy_obj_ptr(this->_on_mouseleft_down_event);
		utilities::destroy_obj_ptr(this->_on_mouseright_up_event);
		utilities::destroy_obj_ptr(this->_on_mouseright_down_event);
		utilities::destroy_obj_ptr(this->_on_mousemove_event);
		utilities::destroy_obj_ptr(this->_on_resize_gui_event);
		utilities::destroy_obj_ptr(this->_on_redraw_event);
		utilities::destroy_obj_ptr(this->_on_set_focus_event);
		utilities::destroy_obj_ptr(this->_on_kill_focus_event);
		utilities::destroy_obj_ptr(this->_on_mouseover_in_event);
		utilities::destroy_obj_ptr(this->_on_mouseover_out_event);
		utilities::destroy_obj_ptr(this->_on_render_event);
		utilities::destroy_obj_ptr(this->_on_resize_parent_event);
		utilities::destroy_obj_ptr(this->_on_move_parent_event);
		utilities::destroy_obj_ptr(this->_on_rotate_parent_event);
		utilities::destroy_obj_ptr(this->_on_resize_event);
		utilities::destroy_obj_ptr(this->_on_move_event);
		utilities::destroy_obj_ptr(this->_on_show_event);
		utilities::destroy_obj_ptr(this->_on_hide_event);
		utilities::destroy_obj_ptr(this->_on_update_event);
		utilities::destroy_obj_ptr(this->_on_fade_end);
		utilities::destroy_obj_ptr(this->_on_translating_pos_end);
		utilities::destroy_obj_ptr(this->_on_translating_size_end);
		utilities::destroy_obj_ptr(this->_on_mousewheel_event);
		utilities::destroy_obj_ptr(this->_on_key_down_event);
		utilities::destroy_obj_ptr(this->_on_key_up_event);
		utilities::destroy_obj_ptr(this->_on_custom_render);
		utilities::destroy_obj_ptr(this->_bg_info);
			
		this->_is_to_render		= true;
		this->_is_pickable		= true;
		this->_is_show			= true;
		this->_is_focused		= false;

		this->_id				= IKGL_WIDGET_INVALID_ID;
		this->_layer_level		= LAYER_TYPE_INVALID;
		this->_widget_type		= widget_type_t::WIDGET_TYPE_WINDOW;

		this->_ws_name			= L"";

		this->_alpha			= 1.0f;
		this->_dest_alpha		= 1.0f;
		this->_step_alpha		= 0.0f;
		this->_degree			= 0.0f;
		this->_parent			= nullptr;
	}

	void_t window::set_position(const widget_pos_t& pos){
		if (this->_gui) {
			this->_gui->__check_new_picked([this, &pos]() {
				//assignment of the new position
				this->_pos = pos;
				this->_dest_pos = pos;
				this->_pos_on_translation = pos;
			});

			//correlated actions : use on_move to trigger event and call redraw
			this->on_move();
			this->call_render();
		}

		else {

			//assignment
			this->_pos = pos;
			this->_dest_pos = pos;
			this->_pos_on_translation = pos;

			//correlated actions : use on_move to trigger event and call redraw
			this->on_move();
			this->call_render();
		}

		//* on move parent implementation
		for (auto& child : this->_children_list) {
			child->on_move_parent();
		}
	}

	void_t window::set_position(const widget_pos_t* pos){
		this->window::set_position(*pos);
	}

	void_t window::set_position(coord_t x, coord_t y){
		this->window::set_position({x,y});
	}

	void_t window::set_size(const widget_size_t* size){
		this->window::set_size(*size);
	}

	void_t window::set_size(const widget_size_t& size){
		if (this->_gui) {
			
			this->_gui->__check_new_picked([this, &size]() {
				//assignment
				this->_size = size;
				this->_dest_size = size;
				this->_size_on_translation = size;
			});
			
			//correlated actions : use on_resize to trigger event and call redraw
			this->on_resize();
			this->call_render();
		}

		else {
			//assignment
			this->_size = size;
			this->_dest_size = size;
			this->_size_on_translation = size;

			//correlated actions : use on_resize to trigger event and call redraw
			this->on_resize();
			this->call_render();
		}

		//* on resize parent implementation
		for (auto& child : this->_children_list) {
			child->on_resize_parent();
		}
	}

	void_t window::set_size(coord_t w, coord_t h){
		this->window::set_size({w,h});
	}

	void_t window::set_parent(window* parent){
		//* if parent is not set to nullptr
		//* is necessary to put this pointer
		//* in the children list
		//* add_child will set the m_parent 
		if(parent)
			parent->add_child(this);

		//* otherwhise we have to set
		//* the m_parent to nullptr
		else
			this->_parent = nullptr;
	}

	window* window::get_parent() const {
		return this->_parent;
	}




	bool_t window::has_child(const window* pchild) const {
		return std::find(this->_children_list.begin(), this->_children_list.end(), pchild) != this->_children_list.end();
	}

	void_t window::add_child(window* pchild){
		//* checking if it is already inserte
		if (!this->has_child(pchild)){
			//* if not we have to set layer level
			//* and parent pointer
			pchild->__set_parent_pointer(this);
			pchild->set_layer_level(this->get_layer_level());

			//* insert in the list and call
			//* the redraw of te parent
			this->_children_list.push_back(pchild);
			this->call_render();
		}
	}

	void_t  window::remove_child(const window* pChild){
		//* searching in the list the child
		auto it = std::find(this->_children_list.begin(), this->_children_list.end(), pChild);

		//* if found
		if (it != this->_children_list.end()){
			//* removing from list and deleting
			delete(*it);
			this->_children_list.erase(it);
			
			//* call the rendering of the parent
			this->call_render();
		}
	}

	void_t window::__set_parent_pointer(window* parent){
		this->_parent = parent;
	}

	void_t window::set_top(){
		if (this->_parent)
			this->_parent->set_child_top(this);
		//* call_render is already call in set_child_top
	}

	void_t window::set_child_top(window* child){
		//* searching in the list the child
		auto it = std::find(this->_children_list.begin(), this->_children_list.end(), child);

		//* if found remove from original position
		if (it != this->_children_list.end()) {
			this->_children_list.erase(it);

			//* put the child to the back
			//* because the rendering loop
			//* iterating list by back to front
			this->_children_list.push_back(child);
			this->call_render();
		}
	}

	void_t window::set_on_mouseleft_up(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_mouseleft_up_event)
			delete(this->_on_mouseleft_up_event);

		//* saving the information
		this->_on_mouseleft_up_event = new widget_event_t(event);
	}

	void_t window::set_on_mouseleft_down(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_mouseleft_down_event)
			delete(this->_on_mouseleft_down_event);

		//* saving the information
		this->_on_mouseleft_down_event = new widget_event_t(event);
	}

	void_t window::set_on_mouseright_up(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_mouseright_up_event)
			delete(this->_on_mouseright_up_event);

		//* saving the information
		this->_on_mouseright_up_event = new widget_event_t(event);
	}

	void_t window::set_on_mouseright_down(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_mouseright_down_event)
			delete(this->_on_mouseright_down_event);

		//* saving the information
		this->_on_mouseright_down_event = new widget_event_t(event);
	}

	void_t window::set_on_mousemove(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_mousemove_event)
			delete(this->_on_mousemove_event);

		//* saving the information
		this->_on_mousemove_event = new widget_event_t(event);
	}

	void_t window::set_on_resize_gui(const widget_event_t event){
		if (this->_on_resize_gui_event)
			delete(this->_on_resize_gui_event);

		this->_on_resize_gui_event = new widget_event_t(event);
	}

	void_t window::set_on_redraw(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_redraw_event)
			delete(this->_on_redraw_event);

		//* saving the information
		this->_on_redraw_event = new widget_event_t(event);
	}

	void_t window::set_on_render(const widget_event_t event) {
		//* deleting old event set if exists
		if(this->_on_render_event)
			delete(this->_on_render_event);

		//* saving the information
		this->_on_render_event = new widget_event_t(event);
	}

	void_t window::set_on_mouseover_in(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_mouseover_in_event)
			delete(this->_on_mouseover_in_event);

		//* saving the information
		this->_on_mouseover_in_event  = new widget_event_t(event);
	}

	void_t window::set_on_mouseover_out(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_mouseover_out_event)
			delete(this->_on_mouseover_out_event);

		//* saving the information
		this->_on_mouseover_out_event  = new widget_event_t(event);
	}

	void_t window::set_on_show(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_show_event)
			delete(this->_on_show_event);

		//* saving the information
		this->_on_show_event = new widget_event_t(event);
	}

	void_t  window::set_on_hide(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_hide_event)
			delete(this->_on_hide_event);

		//* saving the information
		this->_on_hide_event = new widget_event_t(event);
	}

	void_t  window::set_on_resize(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_resize_event)
			delete(this->_on_resize_event);

		//* saving the information
		this->_on_resize_event = new widget_event_t(event);
	}

	void_t  window::set_on_move(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_move_event)
			delete(this->_on_move_event);

		//* saving the information
		this->_on_move_event = new widget_event_t(event);
	}

	void_t window::set_on_mousewheel(widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_mousewheel_event)
			delete(this->_on_mousewheel_event);

		//* saving the information
		this->_on_mousewheel_event = new widget_event_t(event);
	}

	void_t window::set_on_update(const widget_event_t event){
		//* deleting old event set if exists
		if(this->_on_update_event)
			delete(this->_on_update_event);

		//* saving the information
		this->_on_update_event = new widget_event_t(event);
	}

	void_t window::set_on_key_down(const widget_event_t event) {
		//* deleting old event set if exists
		if(this->_on_key_down_event)
			delete(this->_on_key_down_event);

		//* saving the information
		this->_on_key_down_event = new widget_event_t(event);
	}

	void_t window::set_on_key_up(const widget_event_t event) {
		//* deleting old event set if exists
		if(this->_on_key_up_event)
			delete(this->_on_key_up_event);

		//* saving the information
		this->_on_key_up_event = new widget_event_t(event);
	}

	void_t window::set_on_set_focus(const widget_event_t event) {
		//* deleting old event set if exists
		if(this->_on_set_focus_event)
			delete(this->_on_set_focus_event);

		//* saving the information
		this->_on_set_focus_event = new widget_event_t(event);
	}

	void_t window::set_on_kill_focus(const widget_event_t event) {
		//* deleting old event set if exists
		if(this->_on_kill_focus_event)
			delete(this->_on_kill_focus_event);

		//* saving the information
		this->_on_kill_focus_event = new widget_event_t(event);
	}

	void_t window::on_mouseleft_up(){
		//* if trigger is set, trigger it
		if (this->_on_mouseleft_up_event)
			if ((*this->_on_mouseleft_up_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_mouseleft_up_event);
	}

	void_t window::on_mouseleft_down(){
		//* if trigger is set, trigger it
		if (this->_on_mouseleft_down_event)
			if ((*this->_on_mouseleft_down_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_mouseleft_down_event);
	}

	void_t window::on_mouseright_up(){
		//* if trigger is set, trigger it
		if (this->_on_mouseright_up_event)
			if ((*this->_on_mouseright_up_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_mouseright_up_event);
	}

	void_t window::on_mouseright_down(){
		//* if trigger is set, trigger it
		if (this->_on_mouseright_down_event)
			if ((*this->_on_mouseright_down_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_mouseright_down_event);
	}

	void_t window::on_mousemove(){
		//* if trigger is set, trigger it
		if (this->_on_mousemove_event)
			if ((*this->_on_mousemove_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_mousemove_event);
	}

	void_t window::on_resize_gui(){
		//* if trigger is set, trigger it
		if (this->_on_resize_gui_event)
			if ((*this->_on_resize_gui_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_resize_gui_event);

		//*run same on children
		for (auto& child : this->_children_list) {
			child->on_resize_gui();
		}
	}

	void_t window::on_redraw(){
		//* if trigger is set, trigger it
		if (this->_on_redraw_event)
			if ((*this->_on_redraw_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_redraw_event);
	}

	void_t window::on_mouseover_in(){
		//* if trigger is set, trigger it
		if (this->_on_mouseover_in_event)
			if ((*this->_on_mouseover_in_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_mouseover_in_event);
	}

	void_t window::on_mouseover_out(){
		//* if trigger is set, trigger it
		if (this->_on_mouseover_out_event)
			if ((*this->_on_mouseover_out_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_mouseover_out_event);
	}

	void_t window::set_on_move_parent(const widget_event_t event){
		utilities::destroy_obj_ptr(this->_on_move_parent_event);
		this->_on_move_parent_event = new widget_event_t(event);
	}

	void_t window::set_on_resize_parent(const widget_event_t event){
		utilities::destroy_obj_ptr(this->_on_resize_parent_event);
		this->_on_resize_parent_event = new widget_event_t(event);
	}

	void_t window::set_on_rotate_parent(const widget_event_t event){
		utilities::destroy_obj_ptr(this->_on_rotate_parent_event);
		this->_on_rotate_parent_event = new widget_event_t(event);
	}
	
	void_t window::set_on_custom_render(const custom_render_t event){
		utilities::destroy_obj_ptr(this->_on_custom_render);
		this->_on_custom_render = new custom_render_t(event);
	}

	void_t window::on_move_parent(){
		if (this->_on_move_parent_event) {
			if ((*this->_on_move_parent_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK) {
				utilities::destroy_obj_ptr(this->_on_move_parent_event);
			}
		}
	}

	void_t window::on_resize_parent(){
		if (this->_on_resize_parent_event) {
			if ((*this->_on_resize_parent_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK) {
				utilities::destroy_obj_ptr(this->_on_resize_parent_event);
			}
		}
	}

	void_t window::on_rotate_parent(){
		if (this->_on_rotate_parent_event) {
			if ((*this->_on_rotate_parent_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK) {
				utilities::destroy_obj_ptr(this->_on_rotate_parent_event);
			}
		}
	}
	

	void_t window::on_custom_render(hdc_t& hdc, gdiplus_graphics_t* graphicsptr){
		if (this->_on_custom_render) {
			if ((*this->_on_custom_render)(hdc, graphicsptr, this) != widget_event_return_t::KEEP_CALLBACK) {
				utilities::destroy_obj_ptr(this->_on_custom_render);
			}
		}
	}

	void_t window::on_hide(){
		//* if trigger is set, trigger it
		if (this->_on_hide_event)
			if ((*this->_on_hide_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_hide_event);
	}

	void_t window::on_show(){
		//* if trigger is set, trigger it
		if (this->_on_show_event)
			if ((*this->_on_show_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_show_event);
	}

	void_t window::on_resize(){
		//* if trigger is set, trigger it
		if (this->_on_resize_event)
			if ((*this->_on_resize_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_resize_event);
	}

	void_t window::on_move(){
		//* if trigger is set, trigger it
		if (this->_on_move_event)
			if ((*this->_on_move_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_move_event);
	}

	void_t window::on_set_focus() {
		//* if trigger is set, trigger it
		if (this->_on_set_focus_event)
			if ((*this->_on_set_focus_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_set_focus_event);
	}

	void_t window::on_kill_focus() {
		//* if trigger is set, trigger it
		if (this->_on_kill_focus_event)
			if ((*this->_on_kill_focus_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_kill_focus_event);
	}

	void_t window::on_mousewheel(const mousewheel_data_t& data) {
		//* if trigger is set, trigger it
		if (this->_on_mousewheel_event) {
			//* setting mouse data first to trigger
			this->_mousewheel_data.times = data.times;
			this->_mousewheel_data.type  = data.type;

			//* trigger the event
			if ((*this->_on_mousewheel_event)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_mousewheel_event);
		}
	}

	void_t window::on_key_down(const key_message_info_t & message_info) {
		//* if trigger is set, trigger it
		if (this->_on_key_down_event) {
			//* setting key message data first to trigger
			this->_key_message_info = message_info;
			if ((*this->_on_key_down_event)(get_id(), this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_key_down_event);
		}
	}

	void_t window::on_key_up(const key_message_info_t & message_info) {
		if (this->_on_key_up_event) {
			//* setting key message data first to trigger
			this->_key_message_info = message_info;
			if ((*this->_on_key_up_event)(get_id(), this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_key_up_event);
		}
	}

	void_t window::set_id(int_t id){
		this->_id = id;
	}

	int_t window::get_id() const {
		return this->_id;
	}

	widget_pos_t window::get_position() const {
		return this->_pos;
	}

	void_t window::get_position(widget_pos_t& r_pos) const {
		r_pos = this->_pos;
	}

	void_t window::get_position(coord_t& x, coord_t& y) const {
		x = this->_pos.x;
		y = this->_pos.y;
	}

	widget_pos_t window::get_global_position() const {
		//* if is root (no parent) the position 
		//* set in m_pos is absolute
		if(!this->_parent)
			return this->_pos;

		//* else calculating the position
		//* recursively by the parents
		else {
			widget_pos_t global_position= this->_parent->get_global_position();
			const float_t parent_degree	= this->_parent->get_global_rotation();
			

			//*check about the rotation of the parent
			if (parent_degree != 0.0f) {
				widget_pos_t pos = this->_pos;
				utilities::normalize_position(pos, -parent_degree);
				global_position += pos;
			}

			else {
				global_position += this->_pos;
			}

			return global_position;
		}
	}

	void_t window::get_global_position(widget_pos_t& r_pos) const {
		r_pos = this->get_global_position();
	}

	void_t window::get_global_position(coord_t& x, coord_t& y) const {
		const widget_pos_t global_position = this->get_global_position();

		x = global_position.x;
		y = global_position.y;
	}

	widget_rect_t window::get_global_rect() const {
		return widget_rect_t(this->get_global_position(), this->_size);
	}

	void_t window::get_global_rect(widget_rect_t & t_rect) const {
		t_rect = { this->get_global_position(), this->_size };
	}

	void_t window::get_global_rect(coord_t & left, coord_t & top, coord_t & right, coord_t & bottom) const {
		widget_rect_t grect = this->get_global_rect();
		left	= grect.left;
		top		= grect.top;
		right	= grect.right;	
		bottom	= grect.bottom;
	}

	coord_t window::get_x() const {
		return this->_pos.x;
	}

	coord_t window::get_y() const {
		return this->_pos.y;
	}

	coord_t window::get_width() const {
		return this->_size.width;
	}

	coord_t window::get_height() const {
		return this->_size.height;
	}

	const widget_size_t& window::get_size() const {
		return this->_size;
	}

	void_t window::get_size(widget_size_t& r_size) const {
		r_size = this->_size;
	}

	void_t window::get_size(coord_t& w, coord_t& h) const {
		w = this->_size.width;
		h = this->_size.height;
	}

	void_t window::set_alpha(float_t alpha) {
		//* if is fading to an alpha dest value
		//* we have to set only the alpha to the value
		if (this->_alpha != this->_dest_alpha) {
			this->_alpha = alpha;
		}


		//* otherwise we have to set both to the value
		else {
			this->_alpha     = alpha;
			this->_dest_alpha = alpha;
		}

		this->call_render();
	}

	float_t window::get_alpha() const {
		return this->_alpha;
	}

	float_t window::get_global_alpha() const {
		//* if is root widget (no parent)
		//* the value of m_alpha is absolute
		if(!this->_parent)
			return this->_alpha;

		//* otherwise we have to calculatin
		//* the absolute value recursively
		//* getting the value of the parents
		else {
			return this->_alpha * this->_parent->get_global_alpha();
		}
	}

	void_t window::fade_to(float_t alpha, float_t seconds, widget_event_t* eventFade) {
		//* calculating necessary update count
		float_t updatecount = float_t(this->_gui ? this->_gui->get_fps() : DEFAULT_FPS) * seconds;

		//* to avoid zero division
		updatecount = __ikgl_max(1.0f , updatecount);

		//* calculating difference and step per update
		const float_t alpha_difference = (alpha - this->_alpha);
		const float_t step_per_update  =  alpha_difference / updatecount;

		//setting information
		this->_dest_alpha = alpha;
		this->_step_alpha = __ikgl_max(step_per_update, -step_per_update);

		if (eventFade) {
			//* deleting the old event fade if exists
			if(this->_on_fade_end)
				delete(this->_on_fade_end);

			//* saving the information
			this->_on_fade_end = new widget_event_t(*eventFade);
		}
	}

	void_t window::call_render(){
		this->_is_to_render = true;
	}

	void_t window::set_focus(){
		this->_is_focused = true;
		this->on_set_focus();
		this->call_render();
		
		//* gui focused fix
		if (this->_gui) {
			if (this->_gui->get_focused() != this)
				this->_gui->_focused_window = this;
		}

	}

	void_t window::kill_focus(){
		this->_is_focused = false;
		this->on_kill_focus();
		this->call_render();
	}

	bool_t window::is_focused() const {
		return this->_is_focused;
	}

	void_t window::draw( hdc_t& hdc ,  gdiplus_graphics_t* graphicsptr){
		if(!this->is_shown())
			return;
		
		this->draw_background(hdc, graphicsptr);
		this->draw_children(hdc, graphicsptr);

		//* set as redrawed and call redraw trigger event
		this->_is_to_render = false;
		this->on_redraw();

		if (this->_on_custom_render)
			this->on_custom_render(hdc, graphicsptr);
	}

	void_t window::draw_background(hdc_t& hdc, gdiplus_graphics_t* graphicsptr){
		if (this->_bg_info) {

			//* getting absolute values alpha&degree
			const auto alpha = this->get_global_alpha();
			const auto degree = this->get_global_rotation();

			//* initializing color and rectangle info
			auto color = color_rgba_t(this->_bg_info->bg_color, alpha);
			auto rect = this->get_global_rect().to_gdiplus_rect();

			//* if needed rotate the graphics and normalize the rect
			if (degree != 0.0f) {
				utilities::normalize_rect(rect, degree);
				external::rotate_trasform(graphicsptr, degree);
			}

			//* if need to draw a rectangle bordered
			if (this->_bg_info->bg_color.to_gdiplus_argb() != this->_bg_info->border_color.to_gdiplus_argb()) {
				draw_rect_bordered(graphicsptr, rect, color, color_rgba_t(this->_bg_info->border_color, alpha), 1);
			}

			//* otherwise use sample rect to improve efficient
			else {
				draw_rect(graphicsptr, rect, color);
			}

			//*  rotate back the graphics
			if (degree != 0.0f) {
				external::rotate_trasform(graphicsptr, -degree);
			}
		}
	}

	void_t window::draw_children(hdc_t& hdc, gdiplus_graphics_t* graphicsptr){
		//* draw the children over the parent
		for (auto& childptr : this->_children_list) {
			if (childptr) {
				childptr->draw(hdc, graphicsptr);
			}
		}
	}

	void_t window::update(const double_t time){
				
		//* check about timer event set on the window
		if (this->_timer_events_vec.empty() == false) {

			//* iterating the timers vector
			for (auto it = this->_timer_events_vec.begin(); it != this->_timer_events_vec.end();) {
				//* reference to the timer
				auto& timer_event = *it;

				//* if the time which was set is now or past
				if (timer_event.ex_time < time) {
					 //* if the trigger will return no repeat value 
					 //* remove it from the vector
					 if (timer_event.event(get_id(), this) != widget_event_return_t::KEEP_CALLBACK) {
 						it = this->_timer_events_vec.erase(it);
					 }

					 //* else set again a new execute time to 
					 //* run again the trigger
					 else {
 						timer_event.ex_time = time + timer_event.seconds;
 						it++;
					 }
				}

				//* if the time is not now or past
				//* skip the event
				else {
					it++;
				}
			}
		}

		//* if is set a update event
		//* run it
		if (this->_on_update_event) {
			if ((*this->_on_update_event)(get_id(), this) != ikgl::widget_event_return_t::KEEP_CALLBACK) {
				utilities::destroy_obj_ptr(this->_on_update_event);
			}
		}

		//* update the children
		for (auto& win : this->_children_list) {
			win->update(time);
		}
	}

	bool_t window::is_in(const widget_pos_t& pos) const {
		return this->is_in(pos.x, pos.y);
	}

	bool_t window::is_in(coord_t x, coord_t y) const {
		widget_rect_t grect		= this->get_global_rect();
		const float_t degree	= this->get_global_rotation();

		if (degree == 0.0f) {
			return	 (grect.left		<= x &&
					 (grect.right)		>= x &&
					 (grect.top)		<= y &&
					 (grect.bottom)		>= y);
		}

		else {
			widget_pos_t pos = {x,y};
			utilities::normalize_rect(grect, degree);
			utilities::normalize_position(pos, degree);

			return	 (grect.left		<= pos.x &&
					 (grect.right)		>= pos.x &&
					 (grect.top)		<= pos.y &&
					 (grect.bottom)		>= pos.y);
		}
	}

	bool_t window::on_render(){
		//* if is set the trigger run it
		if (this->_on_render_event) {
			if ((*this->_on_render_event)(get_id(), this) != widget_event_return_t::KEEP_CALLBACK)
				utilities::destroy_obj_ptr(this->_on_render_event);
		}


		__update_alpha_fading();
		__update_pos_translating();
		__update_size_translating();

		
		//* iterating the children to 
		//* check if some child need to
		//* call the rendering
		//* we cant break the for loop
		//* to perform on_render to every child
		//* because is necessary for the translations effects
		bool_t children_render = false;
		for (auto& p : this->_children_list) {
			if (p->on_render()){
				children_render =true;
			}
		}

		if (!this->is_shown())
			return false;

		//* if is not needed to render
		//* can return false
		if (this->_is_to_render || children_render) {
			return true;
		}
		
		//* if the other check are not
		//* return nothing
		//* is not needed to render
		else {
			return false;
		}
	}

	bool_t window::is_to_render() const {
		return this->_is_to_render;
	}

	void_t window::get_rect(widget_rect_t& rect) const {
		rect = { this->_pos,this->_size };
	}

	widget_rect_t window::get_rect() const {
		return { this->_pos, this->_size };
	}

	void_t window::get_rect(mc_rect_t& rect) const {
		get_rect().to_mc_rect(rect);
	}

	void_t window::move_x(const coord_t x){
		this->set_position(this->_pos.x + x, this->_pos.y);
	}

	void_t window::move_y(const coord_t y){
		this->set_position(this->_pos.x, this->_pos.y+y);
	}

	void_t window::move_left(const coord_t val){
		this->set_position(this->_pos.x - val, this->_pos.y);
	}

	void_t window::move_right(const coord_t val){
		this->set_position(this->_pos.x + val, this->_pos.y);
	}

	void_t window::move_top(const coord_t val){
		this->set_position(this->_pos.x, this->_pos.y - val);
	}

	void_t window::move_bottom(const coord_t val){
		this->set_position(this->_pos.x, this->_pos.y + val);
	}

	void_t window::move_to_left(){
		this->set_position(0, get_y());
	}

	void_t window::move_to_right(){
		if (this->_parent) {
			this->set_position(this->_parent->get_width() - this->get_width(), this->get_y());
		}

		else if(this->_gui) {
			widget_rect_t rect = this->_gui->get_rect();
			this->set_position((rect.right-rect.left) - this->get_width(), this->get_y());
		}
	}

	void_t window::move_to_top(){
		this->set_position(get_x(), 0);
	}

	void_t window::move_to_bottom(){
		if (this->_parent) {
			this->set_position(this->get_x(), this->_parent->get_height() - this->get_height());
		}

		else {
			widget_rect_t rect = this->_gui->get_rect();
			this->set_position(this->get_x(), (rect.bottom - rect.top) - this->get_height());
		}
	}

	void_t window::move_vertical_center(){
		if (this->_parent)
			this->set_position(this->get_x(), this->_parent->get_height() / 2 - this->get_height() / 2);

		else {
			widget_rect_t rect = this->_gui->get_rect();
			this->set_position(this->get_x(), (rect.bottom - rect.top) / 2 - this->get_height() / 2);
		}
	}

	void_t window::move_horizontal_center(){
		if (this->_parent) {
			this->set_position(this->_parent->get_width() / 2 - this->get_width() / 2, this->get_y());
		}

		else {
			widget_rect_t rect = this->_gui->get_rect();
			this->set_position((rect.right - rect.bottom) / 2 - this->get_width() / 2, this->get_y());
		}
	}

	void_t window::translate_to(const widget_pos_t& pos, const double_t seconds, widget_event_t* event_end){
		if (this->get_gui()) {
			this->_dest_pos = pos;
			auto const step_x = (float_t(this->_dest_pos.x) - float_t(this->_pos.x)) / (float_t(seconds) * float_t(this->get_gui()->get_fps()));
			auto const step_y = (float_t(this->_dest_pos.y) - float_t(this->_pos.y)) / (float_t(seconds) * float_t(this->get_gui()->get_fps()));
			this->_translating_pos_step = {	step_x, step_y };

			if (event_end) {
				utilities::destroy_obj_ptr(this->_on_translating_pos_end);
				this->_on_translating_pos_end = new widget_event_t(*event_end);
			}
		}
	}

	void_t window::stretch_to(const widget_size_t& size, const double_t seconds, widget_event_t* event_end ){
		if (this->get_gui()) {
			this->_dest_size = size;
			this->_translating_size_step = {
				float_t(this->_dest_size.width - this->_size.width)   / (float_t(seconds) * float_t(this->get_gui()->get_fps())),
				float_t(this->_dest_size.height - this->_size.height) / (float_t(seconds) * float_t(this->get_gui()->get_fps())),
			};

			if (event_end) {
				utilities::destroy_obj_ptr(this->_on_translating_size_end);
				this->_on_translating_size_end = new widget_event_t(*event_end);
			}
		}
	}

	void_t window::set_window_name(const wstring_t name){
		if (this->_gui) {
			this->_gui->__register_window_name(this, name);
		}

		this->_ws_name = name;
	}

	void_t window::set_window_name(const string_t name){
		this->set_window_name(utilities::to_wstring(name));
	}

	wstring_t window::get_window_name() const {
		return this->_ws_name;
	}

	string_t window::get_window_name_str() const{
		return utilities::to_string(this->_ws_name);
	}

	void_t window::show(){
		this->_is_show = true;
		this->on_show();
		this->call_render();
	}

	void_t window::hide(){
		this->_is_show = false;
		this->on_hide();
		this->call_render();
	}

	bool_t window::is_shown() const {
		return this->_is_show;
	}

	void_t window::set_layer_level(layer_level_t layer){
		this->_layer_level = layer;
	}

	layer_level_t  window::get_layer_level() const {
		return this->_layer_level;
	}

	void_t window::set_widget_type(widget_type_t type){
		this->_widget_type = type;
	}

	widget_type_t window::get_widget_type() const {
		return this->_widget_type;
	}

	bool_t window::is_widget_type(widget_type_t type) const {
		return this->_widget_type == type;
	}

	void_t window::set_background(const background_info_t* info){
		//* destroy the old information (if exists)
		//* and set ptr to nullptr
		utilities::destroy_obj_ptr(this->_bg_info);

		//* if info is not a nullptr
		//* set the new info 
		if (info) {
			this->_bg_info = new background_info_t(*info);

			if (!this->_bg_info) {
				__ikgl_error("bad allocation (set_background)");
			}
		}

		this->call_render();
	}

	void_t window::set_background(const background_info_t& info){
		//* destroy the old information (if exists)
		//* and set ptr to nullptr
		utilities::destroy_obj_ptr(this->_bg_info);

		//* if info is not a nullptr
		//* set the new info 
		this->_bg_info = new background_info_t(info);

		if (!this->_bg_info) {
			__ikgl_error("bad allocation (set_background)");
		}
		
		this->call_render();
	}

	window* window::get_child(int_t id) const {
		window* temp = nullptr;

		//* iterating the children and
		//* searching by id the child in the list
		for (auto childptr : this->_children_list) {
			if (childptr->get_id() == id) {
				return childptr;
			}

			else if (childptr->get_child(id, &temp)) {
				return temp;
			}
		}

		//* if not exists return nullptr
		return nullptr;
	}

	bool_t window::get_child(int_t id, window** winptr) const {
		//* if the pointer is not allocated memory
		//* return before to unreference it
		if (!winptr) {
			return false;
		}

		//* else set the value of the pointer to nullptr
		//* to prevent inconsistent results
		else {
			*winptr = nullptr;
		}

		//* iterating the children list
		//* and searching the child by id
		for (auto& child : this->_children_list) {
			if (child->get_id() == id) {
				*winptr = child;
				break;
			}

			else if (child->get_child(id, winptr)) {
				break;
			}
		}

		return *winptr != nullptr;
	}

	window* window::get_picked(const widget_pos_t& pos){
		//* check if the basics conditions are missing
		if (!this->is_shown() || !this->is_pickable())
			return nullptr;

		//* exams child first to exams the parent 
		//* to prevent the "not pick outside parent" 
		if (this->_children_list.empty() == false) {
			window* win = nullptr;
			
			//* iterating the children front to back 
			//* to search the first which is picked
			for (auto it = this->_children_list.end(); it != this->_children_list.begin(); ) {
				auto child = *(--it);
				if (child->is_shown() && (win = child->get_picked(pos))) {
					return win;
				}
			}
		}

		//* no children picked so check about parent
		//* if is or not picked & pickable
		if (!this->is_in(pos)) {
			return nullptr;
		}

		//* here mean is pickable and picked
		else {
			return this;
		}
	}

	window::children_list_t& window::get_children_ref(){
		return this->_children_list;
	}

	void_t window::set_gui(gui * guiptr){
		this->_gui = guiptr;
	}

	gui * window::get_gui() const {
		return this->_gui;
	}

	bool_t window::get_gui(gui * guiptr) const{
		return (guiptr = this->_gui)!=nullptr;
	}

	bool_t window::is_pickable() const{
		return this->_is_pickable;
	}

	void_t window::enable_pick() {
		this->_is_pickable = true;
	}

	void_t window::erase_child(window* child)
	{
		auto it = std::find(this->_children_list.begin(), this->_children_list.end(), child);
		if (it != this->_children_list.end())
			this->_children_list.erase(it);
	}

	void_t window::disable_pick() {
		this->_is_pickable = false;
	}

	const mousewheel_data_t & window::get_mousewheel_data() const{
		return this->_mousewheel_data;
	}

	const key_message_info_t & window::get_key_message_info() const {
		return this->_key_message_info;
	}

	void_t window::add_timer(const double_t seconds , widget_event_t eventTimer) {
		//* setting of the main information about timer event
		timer_event_t event;
		event.event = eventTimer;
		event.seconds = seconds;
		event.ex_time = utilities::get_time_double() + seconds;

		//* use of emplace improve the efficient
		this->_timer_events_vec.emplace_back(event);
	}

	float_t window::get_rotation() const{
		return this->_degree;
	}

	float_t window::get_global_rotation() const{
		//* if is root widget (no parent)
		//* degree is absolute value
		if(!this->_parent)
			return this->_degree;

		//* else is necessary to calculate
		//* the value recursively by parents
		else {
			return this->_degree + this->_parent->get_global_rotation();
		}
	}

	void_t window::set_rotation(float_t degree) {
		//* this check avoid to perform 
		//* supplementary calculation if
		//* the angle is 360.f (so != 0.0)
		if (degree == 360.f) {
			degree = 0.0f;
		}

		//* is not necessary to redraw
		//* the widget if it was already
		//* to this rotation degree
		if (degree != this->_degree) {
			//* if possible we perform a check
			//* to know if the rotation of the window
			//* will put the mouse out/in of the window

			if (this->_gui) {
				this->_gui->__check_new_picked([this, &degree]() {
					 //* perform the rotation
					 call_render();
					 this->_degree = degree;
				});
			}

			else {
				this->call_render();
				this->_degree = degree;
			}
		}
	}

	void_t window::rotate(float_t degree) {
		//* is not necessary to redayw
		//* the widget if it was already
		//* to this rotation degree
		if (degree != 0.0f) {
			this->set_rotation(this->_degree + degree);
		}
	}

	void_t window::hide_children() {
		if (this->_children_list.empty() == false) {
			//* hide all children
			for (auto ptr : this->_children_list) {
				ptr->hide();
			}

			//* call redraw ( only if the list is not empty)
			this->call_render();
		}
	}

	void_t window::show_children() {
		if (this->_children_list.empty() == false) {
			//* show all children
			for (auto ptr : this->_children_list)
				ptr->show();

			//* call redraw (only if the list is not empty)
			this->call_render();
		}
	}

	void_t window::remove_children() {
		if (this->_children_list.empty() == false) {
			//* remove all children
			for (auto ptr : this->_children_list) {
				delete(ptr);
			}

			//* clear and redraw (only if the list is not empty)
			this->_children_list.clear();
			this->call_render();
		}
	}

	void_t window::__update_alpha_fading() {
		//* check about fade action
		//* case of alpha is upping
		if (this->_alpha < this->_dest_alpha) {
			//* increase alpha to next step
			this->_alpha += this->_step_alpha;

			//* check to know if the effect is end
			if (this->_alpha >= this->_dest_alpha) {
				//* set alpha to dest alpha to 
				//* prevent next future check wrong
				this->_alpha = this->_dest_alpha;

				//* if is set a trigger
				if (this->_on_fade_end) {
					 //* call the trigger and delete it
					 (*this->_on_fade_end)(this->get_id(), this);
					 utilities::destroy_obj_ptr(this->_on_fade_end);
				}
			}

			//* the alpha value is changed
			//* so the window need to be redrawed
			this->call_render();
		}


		//* check about fade action
		//* case of alpha is downing
		else if (this->_alpha > this->_dest_alpha) {
			//* decrease alpha value
			this->_alpha -= this->_step_alpha;

			//* check if the effect fade is end
			if (this->_alpha <= this->_dest_alpha) {
				//* set alpha to dest alpha to
				//* prevent next future check wrong
				this->_alpha = this->_dest_alpha;

				//* if is set a trigger
				if (this->_on_fade_end) {
					//* call the trigger and destroy it
					(*this->_on_fade_end)(this->get_id(), this);
					utilities::destroy_obj_ptr(this->_on_fade_end);
				}
			}

			//* alpha value is changed so window need
			//* to be redrawed
			this->call_render();
		}
	}

	void_t window::__update_pos_translating(){
		if (this->_pos != this->_dest_pos) {
			if (this->_translating_pos_step.x != 0) {
				this->_pos_on_translation.x += this->_translating_pos_step.x;

				if (this->_translating_pos_step.x > 0) {
					 if (this->_pos_on_translation.x > this->_dest_pos.x) {
 						this->_pos_on_translation.x = float_t(this->_dest_pos.x);
					 }
				}

				else {
					 if (this->_pos_on_translation.x < this->_dest_pos.x) {
 						this->_pos_on_translation.x = float_t(this->_dest_pos.x);
					 }
				}
			}

			if (this->_translating_pos_step.y != 0) {
				this->_pos_on_translation.y += this->_translating_pos_step.y;

				if (this->_translating_pos_step.y > 0) {
					 if (this->_pos_on_translation.y > this->_dest_pos.y) {
 						this->_pos_on_translation.y = float_t(this->_dest_pos.y);
					 }
				}

				else {
					 if (this->_pos_on_translation.y < this->_dest_pos.y) {
 						this->_pos_on_translation.y = float_t(this->_dest_pos.y);
					 }
				}
			}

			this->_pos = this->_pos_on_translation;

			if (this->_on_translating_pos_end && this->_pos == this->_dest_pos) {
				if((*this->_on_translating_pos_end)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
 utilities::destroy_obj_ptr(this->_on_translating_pos_end);
			}

			this->call_render();
			//* on move parent implementation
			for (auto& child : this->_children_list) {
				child->on_move_parent();
			}
		}
	}

	void_t window::__update_size_translating(){
		if (this->_size != this->_dest_size) {
			if (this->_translating_size_step.width != 0) {
				this->_size_on_translation.change_width(this->_translating_size_step.width);

				if (this->_translating_size_step.width > 0) {
					 if (this->_size_on_translation.width > this->_dest_size.width) {
 						this->_size_on_translation.width = float_t(this->_dest_size.width);
					 }
				}

				else {
					 if (this->_size_on_translation.width < this->_dest_size.width) {
 						this->_size_on_translation.width = float_t(this->_dest_size.width);
					 }
				}
			}

			if (this->_translating_size_step.height != 0) {
				this->_size_on_translation.height += this->_translating_size_step.height;

				if (this->_translating_size_step.height > 0) {
					 if (this->_size_on_translation.height > this->_dest_size.height) {
 						this->_size_on_translation.height = float_t(this->_dest_size.height);
					 }
				}

				else {
					 if (this->_size_on_translation.height < this->_dest_size.height) {
 						this->_size_on_translation.height = float_t(this->_dest_size.height);
					 }
				}
			}

			this->_size = this->_size_on_translation;

			if (this->_on_translating_size_end && this->_size == this->_dest_size) {
				if((*this->_on_translating_size_end)(this->_id, this) != widget_event_return_t::KEEP_CALLBACK)
					utilities::destroy_obj_ptr(this->_on_translating_size_end);
			}

			this->call_render();

			//* on resize parent implementation
			for (auto& child : this->_children_list) {
				child->on_resize_parent();
			}
		}
	}
}
