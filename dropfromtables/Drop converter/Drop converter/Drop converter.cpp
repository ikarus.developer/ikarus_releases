// Drop converter.cpp : Definisce il punto di ingresso dell'applicazione.
//

#include "stdafx.h"
#include "Drop converter.h"

#define MAX_LOADSTRING 100

// Variabili globali:
HINSTANCE hInst;                                // istanza corrente
WCHAR szTitle[MAX_LOADSTRING];                  // Testo della barra del titolo
WCHAR szWindowClass[MAX_LOADSTRING];            // nome della classe della finestra principale

// Dichiarazioni con prototipo di funzioni incluse in questo modulo di codice:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

#define CRITICAL_SCOPE() std::lock_guard<std::mutex> temp(g_mutex)

/*GLOBALs*/
HWND		g_mainhWind;
std::mutex  g_mutex;

sdtgui::GUI g_gui;

sdtgui::WINDOW*			g_pMainWindow			= nullptr;
sdtgui::BUTTON*			g_pSelectFileButton		= nullptr;
sdtgui::BUTTON*			g_pConvertButton		= nullptr;
sdtgui::BUTTON*			g_pCloseButton			= nullptr;

sdtgui::TEXTLINE*		g_pFileName				= nullptr;
sdtgui::TEXTLINE*		g_pTextOut				= nullptr;

const long				g_windowWidth			= 500;
const long				g_windowHeight			= 282;


std::wstring			g_selectedFileName		= L"";
std::thread*			g_pThread				= nullptr;
bool					g_isShut				= false;
std::atomic<bool>		g_isSelectingFile		= false;

//declaration
void GetDesktopResolution(long& w, long&h);

void Process();
void CreateGUI();
void ExitApp();
void SelectFile();
void ConvertFile();
void MT_ConvertFile();




int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_DROPCONVERTER, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_DROPCONVERTER));
	MSG msg;


	CreateGUI();


	while (true)
	{
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			if(msg.message == WM_QUIT)
				break;
		}

		if(msg.message == WM_QUIT)
			break;

		Process();
	}

	ExitApp();

    return (int) msg.wParam;
}



ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_DROPCONVERTER));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = NULL;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Archivia l'handle di istanza nella variabile globale
   long screenW=0, screenH=0;
   GetDesktopResolution(screenW,screenH);


#ifdef _DEBUG
   AllocConsole();

   freopen("CONOUT$",	"a", stdout);
   freopen("CONIN$",	"r", stdin);
   freopen("CONIN$",	"r", stderr);
#endif


   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_POPUP,
     (screenW-g_windowWidth)/2 , (screenH-g_windowHeight)/2 , g_windowWidth, g_windowHeight, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd) 
      return FALSE;
 
   g_mainhWind = hWnd;



   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);




   return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if(hWnd == g_mainhWind && !g_isSelectingFile)
	{
		CRITICAL_SCOPE();
		g_gui.messageProcedure(hWnd, message, wParam, lParam);
	}
	

    switch (message)
    {
   

    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}



void Process()
{
	Sleep(40);

	{
		CRITICAL_SCOPE();

		if (g_pMainWindow && g_pMainWindow->onRender())
		{
			HDC hdc = GetDC(g_mainhWind);

			g_pMainWindow->draw(hdc);

			ReleaseDC(g_mainhWind, hdc);
		}
	}
	
}



void CreateGUI()
{
	g_gui.clear();
	sdtgui::LAYER* pLayer = g_gui.addLayer("UI");

	pLayer->addChild((g_pMainWindow = new sdtgui::WINDOW()));
	g_pMainWindow->setSize(g_windowWidth, g_windowHeight);
	g_pMainWindow->setPosition(0, 0);
	g_pMainWindow->show();


	//bg
	HBITMAP hImage = LoadBitmap((HINSTANCE)GetWindowLongPtr(g_mainhWind, GWLP_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP1));

	sdtgui::IMAGE* pImageBG = new sdtgui::IMAGE();
	pImageBG->setParent(g_pMainWindow);
	pImageBG->setPosition(0,0);
	pImageBG->setSize(g_windowWidth, g_windowHeight);
	pImageBG->setImage(hImage);
	pImageBG->show();

	DeleteObject(hImage);

	//create buttons
	g_pSelectFileButton = new sdtgui::BUTTON();
	g_pSelectFileButton->setParent(g_pMainWindow);
	g_pSelectFileButton->setPosition(129, 203);

	sdtgui::TButtonCoverInfo cover;
	cover.colorIgnore = RGB(255,0,255);
	cover.defaultCover	= LoadBitmap((HINSTANCE)GetWindowLongPtr(g_mainhWind, GWLP_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP8));
	cover.downCover		= LoadBitmap((HINSTANCE)GetWindowLongPtr(g_mainhWind, GWLP_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP9));
	cover.overCover		= LoadBitmap((HINSTANCE)GetWindowLongPtr(g_mainhWind, GWLP_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP10));
	g_pSelectFileButton->setCover(&cover);

	DeleteObject(cover.defaultCover);
	DeleteObject(cover.downCover);
	DeleteObject(cover.overCover);

	g_pSelectFileButton->show();


	sdtgui::TWidgetEvent selectFileEvent = SDT_WIDGET_LAMBDA({
		SelectFile();
		return true;
	});
	g_pSelectFileButton->setOnClickEvent(selectFileEvent);





	g_pConvertButton = new sdtgui::BUTTON();
	g_pConvertButton->setParent(g_pMainWindow);
	g_pConvertButton->setPosition(287, 203);

	sdtgui::TButtonCoverInfo coverBtn;
	coverBtn.colorIgnore	= RGB(255,0,255);
	coverBtn.defaultCover	= LoadBitmap((HINSTANCE)GetWindowLongPtr(g_mainhWind, GWLP_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP5));
	coverBtn.downCover		= LoadBitmap((HINSTANCE)GetWindowLongPtr(g_mainhWind, GWLP_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP6));
	coverBtn.overCover		= LoadBitmap((HINSTANCE)GetWindowLongPtr(g_mainhWind, GWLP_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP7));

	g_pConvertButton->setCover(&coverBtn);

	DeleteObject(coverBtn.defaultCover);
	DeleteObject(coverBtn.downCover);
	DeleteObject(coverBtn.overCover);

	g_pConvertButton->show();


	sdtgui::TWidgetEvent convertFileEvent = SDT_WIDGET_LAMBDA({
		ConvertFile();
		return true;
	});
	g_pConvertButton->setOnClickEvent(convertFileEvent);



	g_pCloseButton = new sdtgui::BUTTON();
	g_pCloseButton->setParent(g_pMainWindow);
	g_pCloseButton->setPosition(0, 0);

	coverBtn.colorIgnore	= RGB(255,0,255);
	coverBtn.defaultCover	= LoadBitmap((HINSTANCE)GetWindowLongPtr(g_mainhWind, GWLP_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP2));
	coverBtn.downCover		= LoadBitmap((HINSTANCE)GetWindowLongPtr(g_mainhWind, GWLP_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP3));
	coverBtn.overCover		= LoadBitmap((HINSTANCE)GetWindowLongPtr(g_mainhWind, GWLP_HINSTANCE), MAKEINTRESOURCE(IDB_BITMAP4));

	g_pCloseButton->setCover(&coverBtn);

	DeleteObject(coverBtn.defaultCover);
	DeleteObject(coverBtn.downCover);
	DeleteObject(coverBtn.overCover);

	g_pCloseButton->show();
	g_pCloseButton->setPosition(g_windowWidth- g_pCloseButton->getWidth(), 0);


	sdtgui::TWidgetEvent CloseEvent = SDT_WIDGET_LAMBDA({
		PostQuitMessage(0);
		return true;
	});
	g_pCloseButton->setOnClickEvent(CloseEvent);


	sdtgui::TFontInfo fontInfo;
	sdtgui::makeDefaultFont(fontInfo);
	fontInfo.cHeight	= 27;
	fontInfo.cWidth		= 10;
	

	g_pFileName = new sdtgui::TEXTLINE();
	g_pFileName->setParent(g_pMainWindow);
	g_pFileName->setPosition(172 , 81);
	g_pFileName->setSize(279, 40);
	g_pFileName->applyHorizontalCenterAling(true);
	g_pFileName->setColor(RGB(170,170,170));
	g_pFileName->setText("Select a File...");
	g_pFileName->setFont(&fontInfo);
	g_pFileName->show();



	g_pTextOut = new sdtgui::TEXTLINE();
	g_pTextOut->setParent(g_pMainWindow);
	g_pTextOut->setPosition(172 , 137);
	g_pTextOut->setSize(275, 40);
	g_pTextOut->applyHorizontalCenterAling(true);
	g_pTextOut->setColor(RGB(170,170,170));
	g_pTextOut->setText("Ready to work");
	g_pTextOut->setFont(&fontInfo);
	g_pTextOut->show();
}



void GetDesktopResolution(long& w , long& h)
{
	RECT rct;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect(hDesktop, &rct);
	w = rct.right;
	h = rct.bottom;
}




void ExitApp()
{
	{
		CRITICAL_SCOPE();
		g_gui.clear();
		g_isShut = true;
	}

	if (g_pThread)
	{
		if(g_pThread->joinable())
			g_pThread->join();
		delete(g_pThread);
		g_pThread = nullptr;
	}
	


}


void SelectFile()
{
	g_isSelectingFile = true;

	TCHAR filename[ MAX_PATH ];

	OPENFILENAME ofn;
	ZeroMemory( &filename, sizeof( filename ) );
	ZeroMemory( &ofn,      sizeof( ofn ) );

	ofn.lStructSize  = sizeof( ofn );
	ofn.hwndOwner    = g_mainhWind;
	ofn.lpstrFilter  = L"Text Files\0*.txt\0Any File\0*.*\0";
	ofn.lpstrFile    = filename;
	ofn.nMaxFile     = MAX_PATH;
	ofn.lpstrTitle   = L"Select a file to convert!";
	ofn.Flags        = OFN_DONTADDTORECENT | OFN_FILEMUSTEXIST | OFN_NOCHANGEDIR;

	if (GetOpenFileName( &ofn ))
		g_selectedFileName = filename;

	if(!g_selectedFileName.empty())
		g_pFileName->setText(utils::path::GetFileName(g_selectedFileName));

	g_isSelectingFile = false;
}


void ConvertFile()
{
	if (g_selectedFileName.empty())
	{
		g_pTextOut->setText("must select a file.");
		return;
	}

	if ( g_pThread )
		return;


	g_pTextOut->setText("converting...");
	g_pThread = new std::thread( MT_ConvertFile );
}


void MT_ConvertFile()
{
	std::wstring filename = utils::path::GetFileName(g_selectedFileName);
	if (filename.empty())
	{
		CRITICAL_SCOPE();
		g_pTextOut->setText("invalid file!");
		return;
	}

	WCONSOLE(L"[%s]",filename.c_str());

	int type = utils::GetFileType(filename);
	if (type == FILE_TYPE_ERROR)
	{
		CRITICAL_SCOPE();
		g_pTextOut->setText("invalid file!");
		return;
	}

	std::wstring resultName = filename;
	msl::string_replace_in_place<std::wstring>(resultName, L".txt" , L".sql");

	switch (type)
	{
		case FILE_TYPE_COMMON_DROP:
		{
			convert::CommonDropFile converted(g_selectedFileName);
			if (!converted.IsGood())
			{
				CRITICAL_SCOPE();
				g_pTextOut->setText(converted.GetError());
				break;
			}

			if(!converted.WriteOut(resultName))
			{
				CRITICAL_SCOPE();
				g_pTextOut->setText(converted.GetError());
				break;
			}

			{
				CRITICAL_SCOPE();
				g_pTextOut->setText("done.");
			}
			break;
		}
		
/*
		case FILE_TYPE_DROP_ITEM:
		{
			convert::DropItemFile converted(g_selectedFileName);

			if (!converted.IsGood())
			{
				CRITICAL_SCOPE();
				g_pTextOut->setText(converted.GetError());
				break;
			}

			converted.WriteOut();
			break;
		}
		*/

		case FILE_TYPE_MOB_ITEM:
		{
			convert::MobDropItemFile converted(g_selectedFileName);

			if (!converted.IsGood())
			{
				CRITICAL_SCOPE();
				g_pTextOut->setText(converted.GetError());
				break;
			}

			if(!converted.WriteOut(resultName))
			{
				CRITICAL_SCOPE();
				g_pTextOut->setText(converted.GetError());
				break;
			}

			{
				CRITICAL_SCOPE();
				g_pTextOut->setText("done.");
			}
			break;
		}
		
		default:
		{
			CRITICAL_SCOPE();
			g_pTextOut->setText("nothing done.");
			break;
		}
	}
}




