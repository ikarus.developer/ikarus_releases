#pragma once

#include "resource.h"

enum eConstMagic
{
	DROP_SCALE_TABLE = 1000,
};



enum eFiletype
{
	FILE_TYPE_ERROR=0,
	FILE_TYPE_MOB_ITEM,
	FILE_TYPE_COMMON_DROP,
	FILE_TYPE_DROP_ITEM,
};

enum eCommonDropConst
{
	MOB_RANK_MAX_NUM	= 6,
	MOB_RANK_S_KNIGHT	= 3,
	ITEM_NAME_MAX_LEN	= 24,
};


template<class T> void str_to_number(std::wstring& IN stnum , T& num)
{
	num = static_cast<T>(wcstoll(stnum.c_str(),NULL,10));
}


typedef struct SDropItem
{
	BYTE	bRank;
	DWORD	dwItemVnum;
	int		iLvStart;
	int		iLvEnd;
	float	fPercent;
} TDropItem;

struct DropItemKill
{
	struct Info
	{
		DWORD	dwVnum;
		int		iCount;
		int		iPartProb;
	};

	DWORD	m_mob;
	int		m_kill;
	std::vector<Info> m_items;

	bool assignInfo(std::vector<std::wstring>& vec)
	{
		if (vec.size() < 2)
			return false;

		if (vec[0] == L"mob")
			str_to_number<DWORD>(vec[1], m_mob);

		else if (vec[0] == L"type")
			return true;

		else if(vec[0] == L"kill_drop")
			str_to_number(vec[1], m_kill);

		else if (vec[0].find_first_not_of(L"0123456789") == std::wstring::npos && vec.size() > 3)
		{
			Info info;
			str_to_number(vec[1], info.dwVnum);
			str_to_number(vec[2], info.iCount);
			str_to_number(vec[3], info.iPartProb);

			m_items.emplace_back(info);
		}

		else
			return false;

		return true;
	}
};

struct DropItemDrop
{
	struct Info
	{
		DWORD	dwVnum;
		int		iCount;
		float	fProb;
	};

	DWORD m_mob;
	std::vector<Info> m_items;

	bool assignInfo(std::vector<std::wstring>& vec)
	{
		if (vec.size() < 2)
			return false;

		if (vec[0] == L"mob")
			str_to_number<DWORD>(vec[1], m_mob);

		else if (vec[0] == L"type")
			return true;

		else if (vec[0].find_first_not_of(L"0123456789") == std::wstring::npos && vec.size() > 3)
		{
			Info info;
			str_to_number(vec[1], info.dwVnum);
			str_to_number(vec[2], info.iCount);
			info.fProb = wcstof(vec[3].c_str(), NULL);

			CONSOLE("found item in drop info : item vnum (%d)  , count (%d) ,  prob (%f)", info.dwVnum, info.iCount, info.fProb);
			m_items.emplace_back(info);
		}

		else
			return false;

		return true;
	}

};

struct DropItemLevel
{
	struct Info
	{
		DWORD	dwVnum;
		int		iCount;
		float	fProb;
	};

	DWORD	m_mob;
	int		m_level;
	std::vector<Info> m_items;

	bool assignInfo(std::vector<std::wstring>& vec)
	{
		if (vec.size() < 2)
			return false;

		if (vec[0] == L"mob")
			str_to_number(vec[1], m_mob);

		else if (vec[0] == L"type")
			return true;

		else if(vec[0] == L"level_limit")
			str_to_number(vec[1] , m_level);

		else if (vec[0].find_first_not_of(L"0123456789") == std::wstring::npos && vec.size() > 3)
		{
			Info info;
			str_to_number(vec[1], info.dwVnum);
			str_to_number(vec[2], info.iCount);
			info.fProb = wcstof(vec[3].c_str(), NULL);

			m_items.emplace_back(info);
		}

		else
			return false;

		return true;
	}

};





namespace utils 
{
	namespace path
	{
		inline std::wstring GetFileName(std::wstring stPath)
		{
			size_t cur = stPath.find_last_of(L"/\\");
			if(cur == std::wstring::npos)
				return stPath;
			if(++cur == stPath.length())
				return L"";
			
			return stPath.substr(cur,std::wstring::npos);
		}
	}



	inline int GetFileType(std::wstring file)
	{
		if(file == L"mob_drop_item.txt")
			return FILE_TYPE_MOB_ITEM;
		if(file == L"common_drop_item.txt")
			return FILE_TYPE_COMMON_DROP;
		if(file == L"drop_item_group.txt")
			return FILE_TYPE_MOB_ITEM;
		return FILE_TYPE_ERROR;
	}


	template<class T>
	std::vector<T> split_string(T IN in , T IN tok)
	{
		std::vector<T> res;
		res.clear();
		size_t pos=0;

		while ((pos=in.find_first_of(tok)) != T::npos)
		{
			res.emplace_back(in.substr(0,pos));

			if(pos+1 < in.length())
				in = in.substr(pos+1);
			else
				in.clear();
		}

		if(!in.empty())
			res.push_back(in);

		return res;
	}

}
 





namespace convert
{


	std::wstring CommonDropInfoToQuery(TDropItem& info)
	{
		//`rank`, `item_vnum` , `level_start`, `level_end`, `prob`
		int drop = static_cast<int>(info.fPercent * (float)DROP_SCALE_TABLE);

		wchar_t wszQuery[2048]=L"\0";
		wsprintf(wszQuery, L"INSERT INTO `player`.`drop_common` (`rank`, `item_vnum` , `level_start`, `level_end`, `prob`) VALUES( %u, %u, %d, %d, %d);\n",
			info.bRank, info.dwItemVnum, info.iLvStart, info.iLvEnd, drop!=0?drop:1 
		);

		return std::wstring(wszQuery);
	}


	template <class T> 
	void MobDropItemDropToQueries(DropItemDrop& info, T& vec)
	{
		
		for (auto& item : info.m_items)
		{
			int iDrop = (int)(item.fProb * (float) DROP_SCALE_TABLE);
			iDrop /= 100;

			wchar_t wszQuery[2048]=L"\0";
			wsprintf(wszQuery, L"INSERT INTO `player`.`drop_default` (`mob_vnum`,`item_vnum`,`count`,`prob`) VALUES(%u , %u, %d, %d);\n",
				info.m_mob, item.dwVnum, item.iCount, iDrop
			);

			vec.push_back(wszQuery);
		}
	}


	template <class T>
	void MobDropKillToQueries(DropItemKill& info, T& vec)
	{
		static int groupID = 100;
		wchar_t wszQuery[2048]=L"\0";

		for (auto& item : info.m_items)
		{
			wsprintf(wszQuery, L"INSERT INTO `player`.`drop_mob_item_kill` ( `group_id`, `item_vnum`, `count`, `part_prob` )  VALUES( %d, %u , %d, %d )\n",
				groupID, item.dwVnum, item.iCount, item.iPartProb
			);

			vec.push_back(wszQuery);
		}

		wsprintf(wszQuery, L"INSERT INTO `player`.`drop_mob_group_kill` ( `group_id`, `mob_vnum`, `kill_per_drop` )  VALUES( %d, %u , %d)\n",
			groupID++, info.m_mob, info.m_kill
		);
		vec.push_back(wszQuery);
	}

	template <class T>
	void MobDropLevelToQueries(DropItemLevel& info, T& vec)
	{
		static int groupID = 100;
		wchar_t wszQuery[2048]=L"\0";
		

		for (auto& item : info.m_items)
		{
			int iDrop = (int)(item.fProb * (float)(DROP_SCALE_TABLE));
			iDrop/=100;
			wsprintf(wszQuery, L"INSERT INTO `player`.`drop_mob_item_level` ( `group_id`, `item_vnum`, `count`, `prob` )  VALUES( %d, %u , %d, %d )\n",
				groupID, item.dwVnum, item.iCount, iDrop
			);

			vec.push_back(wszQuery);
		}

		wsprintf(wszQuery, L"INSERT INTO `player`.`drop_mob_group_level` ( `group_id`, `mob_vnum`, `level_start`, `level_end` )  VALUES( %d, %u , %d, %d)\n",
			groupID++, info.m_mob, info.m_level, 1000
		);
		vec.push_back(wszQuery);
	}

	class Converter
	{
	public:
		typedef std::list<std::wstring> LISTLINES;
		std::string error;
		LISTLINES m_outLines;
		

		Converter(){
			m_lines.clear();
			error="";
			m_outLines.clear();
		}
		~Converter(){
			m_lines.clear();
			error="";
			m_outLines.clear();
		}

		bool ReadLines(std::wstring filename)
		{
			std::wifstream in(filename);
			if(!in.good())
				return false;

			std::wstring wline;

			while ((std::getline(in,wline)))
				m_lines.push_back(wline);

			return true;
		}

		size_t LinesCount() const
		{
			return m_lines.size();
		}

		LISTLINES& GetLinesRef()
		{
			return m_lines;
		}

		bool IsGood()
		{
			return error.empty()==true;
		}


		std::string GetError()
		{
			return error;
		}

		bool WriteOut(std::wstring name)
		{
			std::wofstream out(name);
			if (!out.is_open())
			{
				error = "cannot write out file";
				WCONSOLE(L"cannot write out file [%s]",name.c_str());
				return false;
			}
			
			WCONSOLE(L"writing output file %s",name.c_str());
#ifdef _DEBUG
			for(auto& line : m_outLines)
				WCONSOLE(line.c_str());
#endif

			for(auto& line : m_outLines)
				out.write(line.c_str(), line.length());
			return true;
		}

	private:
		LISTLINES m_lines;
	};


	class CommonDropFile : public Converter
	{
	public:


		
		CommonDropFile(std::wstring file)
		{ 
			if(!ReadLines(file))
				error="cannot read the selected file.";
			Exams();
		}


		~CommonDropFile() {m_outLines.clear();}

		//table : mobrank, ilevelstart, ilevelend, item, prob
		void Exams()
		{
			std::wstring line=L"";
			Converter::LISTLINES& lines = GetLinesRef();

			WCONSOLE(L"line count : %d.",lines.size());

			int lineindex =1;

			for(auto it = lines.begin(); it!= lines.end(); it++, lineindex++)
			{
				line = *it;

				if (line.empty() || line.find_first_not_of(L" \t\n\r")==std::wstring::npos)
					continue;

				size_t cursor=0;


				for (int i = 0; i <= MOB_RANK_S_KNIGHT; ++i)
				{
					TDropItem item;
					memset(&item, 0 , sizeof(item));
					
					if (line.empty() || line.find_first_not_of(L" \t\n\r")==std::wstring::npos)
						continue;

					
					int j= 0; 
					for (; j < 6; ++j)
					{
						cursor = line.find(L"\t");
						if(cursor == std::wstring::npos && j!=5)
							break;

						std::wstring val = line.substr(0,cursor);
						line = cursor+1 != line.length() && cursor!=std::wstring::npos ? line.substr(cursor+1) : L"";

						WCONSOLE(L"%d - %d %s",i, j,val.c_str());

						switch (j)
						{
							case 1: str_to_number<int>(val ,item.iLvStart);	break;
							case 2: str_to_number<int>(val ,item.iLvEnd);	break;
							case 3: item.fPercent = wcstof(val.c_str(), NULL);	break;
							case 4: str_to_number<DWORD>(val, item.dwItemVnum);	break;
							default: break;
						}
					}

					item.bRank = i;

					if (item.dwItemVnum == 0 && (item.iLvEnd != 0 || item.iLvStart != 0 || item.fPercent != 0.0f))
					{
						char szError[100];
						snprintf(szError, sizeof(szError), "err line %d (phase %d)", lineindex, j);
						error = szError;
						WCONSOLE(L"[%s]", line.c_str());
						return;
					}

					else if(item.dwItemVnum!=0)
						m_outLines.push_back(CommonDropInfoToQuery(item));
				}
			}
		}
	};


	class MobDropItemFile : public Converter
	{
	public:
		enum eNodeType 
		{
			NODE_TYPE_NONE=0,
			NODE_TYPE_KILL,
			NODE_TYPE_LEVEL,
			NODE_TYPE_DROP,
		};		

		std::vector<DropItemKill> m_nodesKill;
		std::vector<DropItemDrop> m_nodesDrop;
		std::vector<DropItemLevel> m_nodesLevel;
		
		MobDropItemFile(std::wstring file)
		{
			if(!ReadLines(file))
				error="cannot read the selected file.";
			Exams();
		}

		void Exams()
		{
			CONSOLE("start exams");

			Converter::LISTLINES& lines = GetLinesRef();
			int lineindex =0;

			Converter::LISTLINES nodeLines;
			bool foundStartNode = false;


			for (auto line : lines)
			{
				lineindex++;

				if (line.find('{') != std::wstring::npos)
				{
					nodeLines.clear();
					foundStartNode=true;
					continue;
				}

				if (line.find('}') != std::wstring::npos)
				{
					if (!__addNode(nodeLines))
					{
						char szError[100];
						snprintf(szError, sizeof(szError), "err line %d wrong nodes syntax", lineindex);
						error = szError;
						return;
					}
					foundStartNode=false;
					continue;
				}

				if(foundStartNode)
					nodeLines.push_back(line);
			}


			if (foundStartNode)
				error="raise end file while reading node lines.";

			if (__getTotalNodesCount()==0)
				error="empty nodes list.";

			MakeOutlines();
			CONSOLE("done Mob Exams");
		}

		static BYTE __GetTypeFromStringType(std::wstring type)
		{
			if(type == L"drop")
				return NODE_TYPE_DROP;
			if(type == L"kill")
				return NODE_TYPE_KILL;
			if(type == L"limit")
				return NODE_TYPE_LEVEL;

			return NODE_TYPE_NONE;
		}


		size_t __getTotalNodesCount()
		{
			return m_nodesDrop.size() + m_nodesKill.size() + m_nodesLevel.size();
		}



		bool __addNode(Converter::LISTLINES& lines)
		{
			std::vector<std::vector<std::wstring>> splitRes;



			for(auto& line : lines)
				splitRes.emplace_back(utils::split_string<std::wstring>(line, L" \t"));

			for(auto& res : splitRes)
				for(auto& st : res)
					std::transform(st.begin(), st.end(), st.begin(), ::towlower);

			for(auto& res : splitRes)
				res.erase(std::remove_if(res.begin(), res.end(), 
					[](const std::wstring& val) -> bool 
					{ 
						return val.empty(); 
					}
				), res.end());

			splitRes.erase(std::remove_if(splitRes.begin(), splitRes.end(), 
				[](const std::vector<std::wstring>& val) -> bool 
				{	
					CONSOLE("size is %d is empty? %s  ",val.size(), val.empty()?"TRUE":"FALSE");
					return val.empty();
				}
			), splitRes.end());

			


			BYTE type=NODE_TYPE_NONE;
			for(auto& res : splitRes)
				if(res.size()>1 && res[0] == L"type")
					type = __GetTypeFromStringType(res[1]);

			if (type == NODE_TYPE_NONE)
			{
				WCONSOLE(L"cannot found type ");
				return false;
			}

			else
			{
				WCONSOLE(L"found type %d",type);
			}


			switch (type)
			{
			case NODE_TYPE_DROP:
				{
					DropItemDrop dropInfo;
					for(auto& res: splitRes)
						if (!dropInfo.assignInfo(res))
						{
							WCONSOLE(L"cannot assign info:");
							for(auto& info : res)
								WCONSOLE(L"%s",info.c_str());

							return false;
						}

					CONSOLE("added node type drop :mob_vnum(%d)  itemcount(%d)",dropInfo.m_mob, dropInfo.m_items.size());
					m_nodesDrop.emplace_back(dropInfo);
				}
				break;


			case NODE_TYPE_KILL:
				{
					DropItemKill dropInfo;
					for(auto& res: splitRes)
						if (!dropInfo.assignInfo(res))
						{
							WCONSOLE(L"cannot assign info:");
							for(auto& info : res)
								WCONSOLE(L"%s",info.c_str());

							return false;
						}

					m_nodesKill.emplace_back(dropInfo);
				}
				break;

			case NODE_TYPE_LEVEL:
				{
					DropItemLevel dropInfo;
					for(auto& res: splitRes)
						if (!dropInfo.assignInfo(res))
						{
							WCONSOLE(L"cannot assign info: %s ",res.empty()?L"EMPTYLIST":L"");
							for(auto& info : res)
								WCONSOLE(L"[%s]",info.c_str());

							return false;
						}

					m_nodesLevel.emplace_back(dropInfo);
				}
				break;


			default:
				return false;
			}

			return true;
		}	




		void MakeOutlines()
		{
			for(auto& dropInfo : m_nodesDrop)
				MobDropItemDropToQueries(dropInfo, m_outLines);

			for(auto& killInfo : m_nodesKill)
				MobDropKillToQueries(killInfo, m_outLines);

			for(auto& levelInfo: m_nodesLevel)
				MobDropLevelToQueries(levelInfo, m_outLines);
		}

	};

}





