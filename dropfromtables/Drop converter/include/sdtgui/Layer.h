
#ifndef __INCLUDE_HEADER_LAYER_SDTGUI__
#define __INCLUDE_HEADER_LAYER_SDTGUI__


namespace sdtgui
{
	class LAYER
	{
	public:
		LAYER();
		~LAYER();

		void		clear();
		bool		onRender();
		void		render(HDC& hdc);

		void		addChild(WINDOW* child);
		void		removeChild(WINDOW* child);

		WINDOW*		getPicked(TPos& pos);


		void		onMouseMove(TPos& oldpos, TPos& newpos);


	private:
		WINDOW::CHILDREN m_childrenList;
	};
}

#endif//__INCLUDE_HEADER_LAYER_SDTGUI__