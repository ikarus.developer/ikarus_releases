COMMON DROP:
	usually the common_drop_item.txt can work using the name of the item or the vnum
	to avoid to integrate in the converter the reading of item_proto , the tool could give "err in line x" when the vnum of the item is 0
	basically, if the vnum is 0 maybe there is the name of the item instead of the vnum, and so you must replace it with the vnum.