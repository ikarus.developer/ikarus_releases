	HEADER_GD_REQUEST_CHARGE_CASH	= 137,

	HEADER_GD_DELETE_AWARDID	= 138,	// delete gift notify icon

	HEADER_GD_UPDATE_CHANNELSTATUS	= 139,
	HEADER_GD_REQUEST_CHANNELSTATUS	= 140,	
#ifdef ENABLE_DROP_FROM_TABLE
	HEADER_GD_MOB_DROP_TABLE_REQUEST_RELOAD				= 141, //must change the value with right sequencly value
#endif
	HEADER_GD_SETUP			= 0xff, //255
	
	
	
	
	
	//[............]
	
	
	HEADER_DG_RESULT_CHARGE_CASH	= 179,
	HEADER_DG_ITEMAWARD_INFORMER	= 180,	//gift notify
	HEADER_DG_RESPOND_CHANNELSTATUS		= 181,
	

#ifdef ENABLE_DROP_FROM_TABLE
	HEADER_DG_MOB_DROP_TABLE_LOAD = 182,//must change the value with right sequencly value
#endif


	HEADER_DG_MAP_LOCATIONS		= 0xfe, //254
	HEADER_DG_P2P			= 0xff, //255
	
	
	
	//[................]
	
	
	
	
typedef struct tBlockException
{
	BYTE	cmd;	// 1 == add, 2 == delete
	char	login[LOGIN_MAX_LEN + 1];
}TPacketBlockException;

typedef struct tChangeGuildMaster
{
	DWORD dwGuildID;
	DWORD idFrom;
	DWORD idTo;
} TPacketChangeGuildMaster;

#ifdef ENABLE_DROP_FROM_TABLE
//common drop
typedef struct SCommonDropItemTable
{
	BYTE		bRank;
	DWORD		dwItemVnum;
	int			iLevelStart;
	int			iLevelEnd;
	int			iDropPct;
} TCommonDropItemTable;

//default drop
typedef struct SDropItemTable
{
	DWORD		dwMobVnum;
	DWORD		dwItemVnum;
	int			iCount;
	int			iProb;
} TDropItemTable;


//mob drop item group by kill
typedef struct SMobDropItemGroupKill
{
	DWORD		dwGroupID;
	DWORD		dwMobVnum;
	int			iKill;
}TMobDropItemGroupKill;


typedef struct SMobDropItemKill
{
	DWORD		dwGroupID;
	DWORD		dwItemVnum;

	int			iCount;
	int			iPartPct;
} TMobDropItemKill;



//mob drop item group by level
typedef struct SMobDropItemGroupLevel
{
	DWORD		dwGroupID;
	DWORD		dwMobVnum;

	int			iLevelStart;
	int			iLevelEnd;
} TMobDropItemGroupLevel;


typedef struct SMobDropItemLevel
{
	DWORD		dwGroupID;
	DWORD		dwItemVnum;
	int			iCount;
	int			iDropPct;

} TMobDropItemLevel;


typedef struct
{
	//common drop
	DWORD	dwCommonDropCount;
	DWORD	dwCommonDropTableSize;

	//default drop
	DWORD	dwDropItemCount;
	DWORD	dwDropItemTableSize;

	//mob drop item kill
	DWORD	dwMobDropItemKillGroupCount;
	DWORD	dwMobDropItemKillGroupTableSize;

	DWORD	dwMobDropItemKillCount;
	DWORD	dwMobDropItemKillTableSize;

	//mob drop item level
	DWORD	dwMobDropItemLevelGroupCount;
	DWORD	dwMobDropItemLevelGroupTableSize;

	DWORD	dwMobDropItemLevelCount;
	DWORD	dwMobDropItemLevelTableSize;
} TPacketDGDropTables;




#endif

