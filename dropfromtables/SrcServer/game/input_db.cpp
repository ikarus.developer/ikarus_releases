
//after the other includes

#ifdef ENABLE_DROP_FROM_TABLE
#include "mob_drop_manager.h"
#endif





// [............]

int CInputDB::Analyze(LPDESC d, BYTE bHeader, const char * c_pData)
{
	switch (bHeader)
	{
	case HEADER_DG_BOOT:
		Boot(c_pData);
		break;

#ifdef ENABLE_DROP_FROM_TABLE
	case HEADER_DG_MOB_DROP_TABLE_LOAD:
		if (!MOB_DROP_MANAGER::instance().RegisterDropInfo((TPacketDGDropTables*)c_pData, c_pData + sizeof(TPacketDGDropTables)))
		{
			sys_err("CANNOT REGISTER DROP INFO!");
			return -1;
		}
		break;

#endif

	case HEADER_DG_LOGIN_SUCCESS:
	
	
	