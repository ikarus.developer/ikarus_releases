  private:
	bool		InitializeTables();
	bool		InitializeShopTable();

#ifdef ENABLE_DROP_FROM_TABLE
	bool		InitializeItemDropTable();
#endif



// [.............]

void		WeddingRequest(TPacketWeddingRequest * p);
	void		WeddingReady(TPacketWeddingReady * p);
	void		WeddingEnd(TPacketWeddingEnd * p);

#ifdef ENABLE_DROP_FROM_TABLE
	void		__SendDropTables(CPeer* peerTarget=NULL);
#endif


//[...........]

 private:
	int						m_looping;
	socket_t				m_fdAccept;	// Á¢¼Ó ¹Þ´Â ¼ÒÄÏ
	TPeerList				m_peerList;

	CPeer *					m_pkAuthPeer;

#ifdef ENABLE_DROP_FROM_TABLE
	std::vector<TCommonDropItemTable>		m_vecCommonDropItemTable;
	std::vector<TDropItemTable>				m_vecDropItemTable;
	

	std::vector<TMobDropItemGroupKill>		m_vecMobDropItemGroupKill;
	std::vector<TMobDropItemKill>			m_vecMobDropItemKill;


	std::vector<TMobDropItemGroupLevel>		m_vecMobDropItemGroupLevel;
	std::vector<TMobDropItemLevel>			m_vecMobDropItemLevel;
#endif
	