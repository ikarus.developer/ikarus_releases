	if (!InitializeShopTable())
	{
		sys_err("InitializeShopTable FAILED");
		return false;
	}

#ifdef ENABLE_DROP_FROM_TABLE
	if (!InitializeItemDropTable())
	{
		sys_err("InitializeItemDropTable FAILED");
		return false;
	}
#endif

	if (!InitializeSkillTable())
	{
		sys_err("InitializeSkillTable FAILED");
		return false;
	}
	
	
	// [..............]
	
	
	
	
	// TO THE END OF THE FILEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
	
	
#ifdef ENABLE_DROP_FROM_TABLE
bool CClientManager::InitializeItemDropTable()
{
	//clearing phase (to clear list in case of reloading via command)
	{
		if (!m_vecCommonDropItemTable.empty())
		{
			m_vecCommonDropItemTable.clear();
			sys_log(0, "CLEANED m_vecCommonDropItemTable (Reloading table)..");
		}

		if (!m_vecDropItemTable.empty())
		{
			m_vecDropItemTable.clear();
			sys_log(0, "CLEANED m_vecDropItemTable (Reloading table)..");
		}

		if (!m_vecMobDropItemGroupKill.empty())
		{
			m_vecMobDropItemGroupKill.clear();
			sys_log(0, "CLEANED m_vecMobDropItemGroupKill (Reloading table)..");
		}

		if (!m_vecMobDropItemKill.empty())
		{
			m_vecMobDropItemKill.clear();
			sys_log(0, "CLEANED m_vecMobDropItemKill (Reloading table)..");
		}

		if (!m_vecMobDropItemGroupLevel.empty())
		{
			m_vecMobDropItemGroupLevel.clear();
			sys_log(0, "CLEANED m_vecMobDropItemGroupLevel (Reloading table)..");
		}

		if (!m_vecMobDropItemLevel.empty())
		{
			m_vecMobDropItemLevel.clear();
			sys_log(0, "CLEANED m_vecMobDropItemLevel (Reloading table)..");
		}
	}



	//loading common drop item
	{
		char szQuery[] = "SELECT `rank`, `item_vnum` , `level_start`, `level_end`, `prob` FROM `drop_common`;";
		std::unique_ptr<SQLMsg> pMsg(CDBManager::instance().DirectQuery(szQuery));

		if (!pMsg.get() || !pMsg->Get())
		{
			sys_err("cannot run query [%s] FAILED ", szQuery);
			return false;
		}


		MYSQL_ROW				row;
		TCommonDropItemTable	commonDropTable;
		SQLResult*				pResult	= pMsg->Get();

		//reserve the count of rows to avoid the reallocation
		m_vecCommonDropItemTable.reserve((size_t) pResult->uiNumRows);

		while ((row = mysql_fetch_row(pResult->pSQLResult)))
		{
			int			col = 0;

			//`rank`, `item_vnum` , `level_start`, `level_end`, `prob`
			str_to_number(commonDropTable.bRank ,		row[col++]);
			str_to_number(commonDropTable.dwItemVnum,	row[col++]);
			str_to_number(commonDropTable.iLevelStart,	row[col++]);
			str_to_number(commonDropTable.iLevelEnd,	row[col++]);
			str_to_number(commonDropTable.iDropPct,		row[col++]);

			//to fix drop pct scale
			const DWORD scale = MAX(1000000/DROP_TABLE_SCALE,1);
			commonDropTable.iDropPct *= scale;

			m_vecCommonDropItemTable.push_back(commonDropTable);
		}
		
		sys_log(0, "LOADED %d COMMON DROP ITEM TABLES [%u] ", m_vecCommonDropItemTable.size());
	}


	//loading default drop table
	{
		char szQuery[] = "SELECT `mob_vnum` , `item_vnum` , `count` , `prob` FROM `drop_default`;";
		std::unique_ptr<SQLMsg> pMsg(CDBManager::instance().DirectQuery(szQuery));

		if (!pMsg.get() || !pMsg->Get())
		{
			sys_err("cannot run query [%s] FAILED ", szQuery);
			return false;
		}


		MYSQL_ROW				row;
		TDropItemTable			dropItemTable;
		SQLResult*				pResult	= pMsg->Get();

		//reserve the count of rows to avoid the reallocation
		m_vecDropItemTable.reserve((size_t) pResult->uiNumRows);

		while ((row = mysql_fetch_row(pResult->pSQLResult)))
		{
			int			col = 0;

			// `mob_vnum` , `item_vnum` , `count` , `prob`
			str_to_number(dropItemTable.dwMobVnum, row[col++]);
			str_to_number(dropItemTable.dwItemVnum, row[col++]);
			str_to_number(dropItemTable.iCount, row[col++]);
			str_to_number(dropItemTable.iProb, row[col++]);

			//to fix drop pct scale
			const DWORD scale = MAX(1000000/DROP_TABLE_SCALE,1);
			dropItemTable.iProb *= scale;

			m_vecDropItemTable.push_back(dropItemTable);
		}

		sys_log(0, "LOADED %d Default Drop Table ", m_vecDropItemTable.size());
	}


	//loading mob drop item group kill
	{
		char szQuery[] = "SELECT `group_id` , `mob_vnum` , `kill_per_drop` FROM `drop_mob_group_kill`;";
		std::unique_ptr<SQLMsg> pMsg(CDBManager::instance().DirectQuery(szQuery));

		if (!pMsg.get() || !pMsg->Get())
		{
			sys_err("cannot run query [%s] FAILED ", szQuery);
			return false;
		}


		MYSQL_ROW				row;
		TMobDropItemGroupKill	mobDropItemGroup;
		SQLResult*				pResult	= pMsg->Get();

		//reserve the count of rows to avoid the reallocation
		m_vecMobDropItemGroupKill.reserve((size_t) pResult->uiNumRows);

		while ((row = mysql_fetch_row(pResult->pSQLResult)))
		{
			int			col = 0;

			// `group_id` , `mob_vnum` , `kill_per_drop`
			str_to_number(mobDropItemGroup.dwGroupID,	row[col++]);
			str_to_number(mobDropItemGroup.dwMobVnum,	row[col++]);
			str_to_number(mobDropItemGroup.iKill,		row[col++]);

			m_vecMobDropItemGroupKill.push_back(mobDropItemGroup);
		}

		sys_log(0, "LOADED %d MobItemGroupKill Table ", m_vecMobDropItemGroupKill.size());
	}


	//loading mob drop item kill
	{
		char szQuery[] = "SELECT `group_id`, `item_vnum` , `count`, `part_prob` FROM `drop_mob_item_kill` ;";
		std::unique_ptr<SQLMsg> pMsg(CDBManager::instance().DirectQuery(szQuery));

		if (!pMsg.get() || !pMsg->Get())
		{
			sys_err("cannot run query [%s] FAILED ", szQuery);
			return false;
		}


		MYSQL_ROW				row;
		TMobDropItemKill		mobDropItemKill;
		SQLResult*				pResult	= pMsg->Get();

		//reserve the count of rows to avoid the reallocation
		m_vecMobDropItemKill.reserve((size_t) pResult->uiNumRows);

		while ((row = mysql_fetch_row(pResult->pSQLResult)))
		{
			int			col = 0;

			// `group_id`, `item_vnum` , `count`, `part_prob`
			str_to_number(mobDropItemKill.dwGroupID,	row[col++]);
			str_to_number(mobDropItemKill.dwItemVnum,	row[col++]);
			str_to_number(mobDropItemKill.iCount,		row[col++]);
			str_to_number(mobDropItemKill.iPartPct,		row[col++]);

			m_vecMobDropItemKill.push_back(mobDropItemKill);
		}

		sys_log(0, "LOADED %d MobItemKill Table ", m_vecMobDropItemKill.size());
	}

	//loading mob drop item group level
	{
		char szQuery[] = "SELECT `group_id` , `mob_vnum` , `level_start` , `level_end` FROM `drop_mob_group_level`;";
		std::unique_ptr<SQLMsg> pMsg(CDBManager::instance().DirectQuery(szQuery));

		if (!pMsg.get() || !pMsg->Get())
		{
			sys_err("cannot run query [%s] FAILED ", szQuery);
			return false;
		}


		MYSQL_ROW				row;
		TMobDropItemGroupLevel	mobDropItemGroup;
		SQLResult*				pResult	= pMsg->Get();

		//reserve the count of rows to avoid the reallocation
		m_vecMobDropItemGroupLevel.reserve((size_t) pResult->uiNumRows);

		while ((row = mysql_fetch_row(pResult->pSQLResult)))
		{
			int			col = 0;

			// `group_id` , `mob_vnum` , `level_start` , `level_end`
			str_to_number(mobDropItemGroup.dwGroupID,		row[col++]);
			str_to_number(mobDropItemGroup.dwMobVnum,		row[col++]);
			str_to_number(mobDropItemGroup.iLevelStart,		row[col++]);
			str_to_number(mobDropItemGroup.iLevelEnd,		row[col++]);

			m_vecMobDropItemGroupLevel.push_back(mobDropItemGroup);
		}

		sys_log(0, "LOADED %d MobItemGroupLevel Table ", m_vecMobDropItemGroupLevel.size());
	}


	//loading mob drop item kill
	{
		char szQuery[] = "SELECT `group_id`, `item_vnum` , `count`, `prob` FROM `drop_mob_item_level` ;";
		std::unique_ptr<SQLMsg> pMsg(CDBManager::instance().DirectQuery(szQuery));

		if (!pMsg.get() || !pMsg->Get())
		{
			sys_err("cannot run query [%s] FAILED ", szQuery);
			return false;
		}


		MYSQL_ROW				row;
		TMobDropItemLevel		mobDropItemLevel;
		SQLResult*				pResult	= pMsg->Get();

		//reserve the count of rows to avoid the reallocation
		m_vecMobDropItemLevel.reserve((size_t) pResult->uiNumRows);

		while ((row = mysql_fetch_row(pResult->pSQLResult)))
		{
			int			col = 0;

			// `group_id`, `item_vnum` , `count`, `part_prob`
			str_to_number(mobDropItemLevel.dwGroupID,	row[col++]);
			str_to_number(mobDropItemLevel.dwItemVnum,	row[col++]);
			str_to_number(mobDropItemLevel.iCount,		row[col++]);
			str_to_number(mobDropItemLevel.iDropPct,	row[col++]);

			//to fix drop item scale
			const DWORD scale = MAX(1000000/DROP_TABLE_SCALE,1);
			mobDropItemLevel.iDropPct *= scale;

			m_vecMobDropItemLevel.push_back(mobDropItemLevel);
		}

		sys_log(0, "LOADED %d MobItemLevel Table ", m_vecMobDropItemLevel.size());
	}


	if (!m_peerList.empty())
	{
		__SendDropTables();
	}


	return true;
}
#endif

