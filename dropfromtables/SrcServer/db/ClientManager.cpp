	//MONARCH
	peer->EncodeWORD(sizeof(TMonarchInfo));
	peer->EncodeWORD(1);
	peer->Encode(CMonarch::instance().GetMonarch(), sizeof(TMonarchInfo));

	CMonarch::VEC_MONARCHCANDIDACY & rVecMonarchCandidacy = CMonarch::instance().GetVecMonarchCandidacy();

	size_t num_monarch_candidacy = CMonarch::instance().MonarchCandidacySize();
	peer->EncodeWORD(sizeof(MonarchCandidacy));
	peer->EncodeWORD(num_monarch_candidacy);
	if (num_monarch_candidacy != 0) {
		peer->Encode(&rVecMonarchCandidacy[0], sizeof(MonarchCandidacy) * num_monarch_candidacy);
	}
	//END_MONARCE

	if (g_test_server)
		sys_log(0, "MONARCHCandidacy Size %d", CMonarch::instance().MonarchCandidacySize());

	peer->EncodeWORD(0xffff);
/*WARNING PASTE THIS TOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO */
#ifdef ENABLE_DROP_FROM_TABLE
	__SendDropTables(peer);
#endif
}



#ifdef ENABLE_DROP_FROM_TABLE
void CClientManager::__SendDropTables(CPeer* peerTarget)
{
	TPacketDGDropTables dropPacket;
	//common drop
	dropPacket.dwCommonDropCount				= (DWORD)	m_vecCommonDropItemTable.size();
	dropPacket.dwCommonDropTableSize			= (DWORD)	sizeof(TCommonDropItemTable);

	//default drop
	dropPacket.dwDropItemCount					= (DWORD)	m_vecDropItemTable.size();
	dropPacket.dwDropItemTableSize				= (DWORD)	sizeof(TDropItemTable);
	
	//mob drop item kill
	dropPacket.dwMobDropItemKillGroupCount		= (DWORD)	m_vecMobDropItemGroupKill.size();
	dropPacket.dwMobDropItemKillGroupTableSize	= (DWORD)	sizeof(TMobDropItemGroupKill);

	dropPacket.dwMobDropItemKillCount			= (DWORD)	m_vecMobDropItemKill.size();
	dropPacket.dwMobDropItemKillTableSize		= (DWORD)	sizeof(TMobDropItemKill);

	//mob drop item level
	dropPacket.dwMobDropItemLevelGroupCount		= (DWORD)	m_vecMobDropItemGroupLevel.size();
	dropPacket.dwMobDropItemLevelGroupTableSize	= (DWORD)	sizeof(TMobDropItemGroupLevel);

	dropPacket.dwMobDropItemLevelCount			= (DWORD)	m_vecMobDropItemLevel.size();
	dropPacket.dwMobDropItemLevelTableSize		= (DWORD)	sizeof(TMobDropItemLevel);


	if (!peerTarget)
	{
		itertype(m_peerList) it = m_peerList.begin();
		for (; it != m_peerList.end(); it++)
		{
			CPeer* peer = *it;

			if (!peer)
				continue;

			//encode header
			peer->EncodeHeader(HEADER_DG_MOB_DROP_TABLE_LOAD, 0,
				sizeof(TPacketDGDropTables) +

				sizeof(TCommonDropItemTable)	* m_vecCommonDropItemTable.size() +
				sizeof(TDropItemTable)			* m_vecDropItemTable.size() +

				sizeof(TMobDropItemGroupKill)	* m_vecMobDropItemGroupKill.size() +
				sizeof(TMobDropItemKill)		* m_vecMobDropItemKill.size() +

				sizeof(TMobDropItemGroupLevel)	* m_vecMobDropItemGroupLevel.size() +
				sizeof(TMobDropItemLevel)		* m_vecMobDropItemLevel.size()
			);


			//encode packet
			peer->Encode(&dropPacket , sizeof(dropPacket));

			//encode common drop
			if (!m_vecCommonDropItemTable.empty())
				peer->Encode(&m_vecCommonDropItemTable[0], m_vecCommonDropItemTable.size() * sizeof(TCommonDropItemTable));

			//encode default drop
			if (!m_vecDropItemTable.empty())
				peer->Encode(&m_vecDropItemTable[0], m_vecDropItemTable.size() * sizeof(TDropItemTable));


			//encode mob drop kill
			if (!m_vecMobDropItemGroupKill.empty())
				peer->Encode(&m_vecMobDropItemGroupKill[0], m_vecMobDropItemGroupKill.size() * sizeof(TMobDropItemGroupKill));

			if (!m_vecMobDropItemKill.empty())
				peer->Encode(&m_vecMobDropItemKill[0], m_vecMobDropItemKill.size() * sizeof(TMobDropItemKill));


			//encode mob drop level
			if (!m_vecMobDropItemGroupLevel.empty())
				peer->Encode(&m_vecMobDropItemGroupLevel[0], m_vecMobDropItemGroupLevel.size() * sizeof(TMobDropItemGroupLevel));

			if (!m_vecMobDropItemLevel.empty())
				peer->Encode(&m_vecMobDropItemLevel[0], m_vecMobDropItemLevel.size() * sizeof(TMobDropItemLevel));

		}
	}

	else
	{
		CPeer* peer = peerTarget;

		if (!peer)
			return;

		//encode header
		peer->EncodeHeader(HEADER_DG_MOB_DROP_TABLE_LOAD, 0,
			sizeof(TPacketDGDropTables) +

			sizeof(TCommonDropItemTable)	* m_vecCommonDropItemTable.size() +
			sizeof(TDropItemTable)			* m_vecDropItemTable.size() +

			sizeof(TMobDropItemGroupKill)	* m_vecMobDropItemGroupKill.size() +
			sizeof(TMobDropItemKill)		* m_vecMobDropItemKill.size() +

			sizeof(TMobDropItemGroupLevel)	* m_vecMobDropItemGroupLevel.size() +
			sizeof(TMobDropItemLevel)		* m_vecMobDropItemLevel.size()
		);

		//encode packet
		peer->Encode(&dropPacket , sizeof(dropPacket));

		//encode common drop
		if (!m_vecCommonDropItemTable.empty())
			peer->Encode(&m_vecCommonDropItemTable[0], m_vecCommonDropItemTable.size() * sizeof(TCommonDropItemTable));

		//encode default drop
		if (!m_vecDropItemTable.empty())
			peer->Encode(&m_vecDropItemTable[0], m_vecDropItemTable.size() * sizeof(TDropItemTable));


		//encode mob drop kill
		if (!m_vecMobDropItemGroupKill.empty())
			peer->Encode(&m_vecMobDropItemGroupKill[0], m_vecMobDropItemGroupKill.size() * sizeof(TMobDropItemGroupKill));

		if (!m_vecMobDropItemKill.empty())
			peer->Encode(&m_vecMobDropItemKill[0], m_vecMobDropItemKill.size() * sizeof(TMobDropItemKill));


		//encode mob drop level
		if (!m_vecMobDropItemGroupLevel.empty())
			peer->Encode(&m_vecMobDropItemGroupLevel[0], m_vecMobDropItemGroupLevel.size() * sizeof(TMobDropItemGroupLevel));

		if (!m_vecMobDropItemLevel.empty())
			peer->Encode(&m_vecMobDropItemLevel[0], m_vecMobDropItemLevel.size() * sizeof(TMobDropItemLevel));
	}

}
#endif













// [..........................]

		switch (header)
		{
			case HEADER_GD_BOOT:
				QUERY_BOOT(peer, (TPacketGDBoot *) data);
				break;

#ifdef ENABLE_DROP_FROM_TABLE
			case HEADER_GD_MOB_DROP_TABLE_REQUEST_RELOAD:
				if(!InitializeItemDropTable())
					sys_err("cannot reload drop information from table!");
				break;
#endif





