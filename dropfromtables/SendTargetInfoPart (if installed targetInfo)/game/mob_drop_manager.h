	bool RegisterDropInfo(TPacketDGDropTables* pPack, const char * pData);
	void ClearDropInfo();

	void MakeDropItems(LPCHARACTER pkChar , LPCHARACTER pkKiller , std::vector<LPITEM>& items);

	#ifdef __SEND_TARGET_INFO__
	void MakeDropInfoItems(LPCHARACTER pkChar , LPCHARACTER pkKiller , std::vector<LPITEM>& items);
	#endif

	/*
	PRIVATE METHODS
	*/