//search : 


void CActorInstance::__ProcessDataAttackSuccess(const NRaceData::TAttackData & c_rAttackData, CActorInstance & rVictim, const D3DXVECTOR3 & c_rv3Position, UINT uiSkill, BOOL isSendPacket)
{
	if (NRaceData::HIT_TYPE_NONE == c_rAttackData.iHittingType)
		return;

	InsertDelay(c_rAttackData.fStiffenTime);

	if (__CanPushDestActor(rVictim) && c_rAttackData.fExternalForce > 0.0f)
	{
		__PushCircle(rVictim);

		// VICTIM_COLLISION_TEST
		const D3DXVECTOR3& kVictimPos = rVictim.GetPosition();
		rVictim.m_PhysicsObject.IncreaseExternalForce(kVictimPos, c_rAttackData.fExternalForce); //*nForceRatio/100.0f);

		// VICTIM_COLLISION_TEST_END
	}

	// Invisible Time
	if (IS_PARTY_HUNTING_RACE(rVictim.GetRace()))
	{
		if (uiSkill) // ÆÄÆ¼ »ç³É ¸ó½ºÅÍ¶óµµ ½ºÅ³ÀÌ¸é ¹«Àû½Ã°£ Àû¿ë
			rVictim.m_fInvisibleTime = CTimer::Instance().GetCurrentSecond() + c_rAttackData.fInvisibleTime;

		if (m_isMain) // #0000794: [M2KR] Æú¸®¸ðÇÁ - ¹ë·±½Ì ¹®Á¦ Å¸ÀÎ °ø°Ý¿¡ ÀÇÇÑ ¹«Àû Å¸ÀÓÀº °í·ÁÇÏÁö ¾Ê°í ÀÚ½Å °ø°Ý¿¡ ÀÇÇÑ°Í¸¸ Ã¼Å©ÇÑ´Ù
			rVictim.m_fInvisibleTime = CTimer::Instance().GetCurrentSecond() + c_rAttackData.fInvisibleTime;
	}
	else // ÆÄÆ¼ »ç³É ¸ó½ºÅÍ°¡ ¾Æ´Ò °æ¿ì¸¸ Àû¿ë
	{
		rVictim.m_fInvisibleTime = CTimer::Instance().GetCurrentSecond() + c_rAttackData.fInvisibleTime;
	}













//make it like this: (take a look where __ENABLE_SHAMAN_ATTACK_FIX__ is used)

void CActorInstance::__ProcessDataAttackSuccess(const NRaceData::TAttackData & c_rAttackData, CActorInstance & rVictim, const D3DXVECTOR3 & c_rv3Position, UINT uiSkill, BOOL isSendPacket)
{
	if (NRaceData::HIT_TYPE_NONE == c_rAttackData.iHittingType)
		return;

	InsertDelay(c_rAttackData.fStiffenTime);

	if (__CanPushDestActor(rVictim) && c_rAttackData.fExternalForce > 0.0f)
	{
		__PushCircle(rVictim);

		// VICTIM_COLLISION_TEST
		const D3DXVECTOR3& kVictimPos = rVictim.GetPosition();
		rVictim.m_PhysicsObject.IncreaseExternalForce(kVictimPos, c_rAttackData.fExternalForce); //*nForceRatio/100.0f);

		// VICTIM_COLLISION_TEST_END
	}

#ifdef __ENABLE_SHAMAN_ATTACK_FIX__
	// Invisible Time
	if (IS_PARTY_HUNTING_RACE(rVictim.GetRace()))
	{
		if (uiSkill) // ÆÄÆ¼ »ç³É ¸ó½ºÅÍ¶óµµ ½ºÅ³ÀÌ¸é ¹«Àû½Ã°£ Àû¿ë
			rVictim.m_fInvisibleTime = CTimer::Instance().GetCurrentSecond() + (c_rAttackData.fInvisibleTime - __GetInvisibleTimeAdjust(uiSkill, c_rAttackData));

		if (m_isMain) // #0000794: [M2KR] Æú¸®¸ðÇÁ - ¹ë·±½Ì ¹®Á¦ Å¸ÀÎ °ø°Ý¿¡ ÀÇÇÑ ¹«Àû Å¸ÀÓÀº °í·ÁÇÏÁö ¾Ê°í ÀÚ½Å °ø°Ý¿¡ ÀÇÇÑ°Í¸¸ Ã¼Å©ÇÑ´Ù
			rVictim.m_fInvisibleTime = CTimer::Instance().GetCurrentSecond() + (c_rAttackData.fInvisibleTime - __GetInvisibleTimeAdjust(uiSkill, c_rAttackData));
	}
	else // ÆÄÆ¼ »ç³É ¸ó½ºÅÍ°¡ ¾Æ´Ò °æ¿ì¸¸ Àû¿ë
	{
		rVictim.m_fInvisibleTime = CTimer::Instance().GetCurrentSecond() + (c_rAttackData.fInvisibleTime - __GetInvisibleTimeAdjust(uiSkill, c_rAttackData));
	}
#else
	// Invisible Time
	if (IS_PARTY_HUNTING_RACE(rVictim.GetRace()))
	{
		if (uiSkill) // ÆÄÆ¼ »ç³É ¸ó½ºÅÍ¶óµµ ½ºÅ³ÀÌ¸é ¹«Àû½Ã°£ Àû¿ë
			rVictim.m_fInvisibleTime = CTimer::Instance().GetCurrentSecond() + c_rAttackData.fInvisibleTime;

		if (m_isMain) // #0000794: [M2KR] Æú¸®¸ðÇÁ - ¹ë·±½Ì ¹®Á¦ Å¸ÀÎ °ø°Ý¿¡ ÀÇÇÑ ¹«Àû Å¸ÀÓÀº °í·ÁÇÏÁö ¾Ê°í ÀÚ½Å °ø°Ý¿¡ ÀÇÇÑ°Í¸¸ Ã¼Å©ÇÑ´Ù
			rVictim.m_fInvisibleTime = CTimer::Instance().GetCurrentSecond() + c_rAttackData.fInvisibleTime;
	}
	else // ÆÄÆ¼ »ç³É ¸ó½ºÅÍ°¡ ¾Æ´Ò °æ¿ì¸¸ Àû¿ë
	{
		rVictim.m_fInvisibleTime = CTimer::Instance().GetCurrentSecond() + c_rAttackData.fInvisibleTime;
	}
#endif

	// Stiffen Time
	rVictim.InsertDelay(c_rAttackData.fStiffenTime);





















//at the end of the file paste:


#ifdef __ENABLE_SHAMAN_ATTACK_FIX__
float CActorInstance::__GetInvisibleTimeAdjust(const UINT uiSkill, const NRaceData::TAttackData& c_rAttackData) {

	static const int shamanw = 3, shamanm = 7;

	if ((GetRace() != shamanw && GetRace() != shamanm) ||
		uiSkill != 0 ||
		m_fAtkSpd < 1.3)
		return 0.0f;

	const auto scale = (m_fAtkSpd - 1.3) / 1.3;
	const auto inv = c_rAttackData.fInvisibleTime * 0.5;
	return inv * scale;
}
#endif
